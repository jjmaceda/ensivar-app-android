# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Applications/adt-bundle-mac-x86_64-20140321/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
#-keepattributes Annotation,SourceFile,LineNumberTable
#-dontwarn com.parse.**
#-keep class com.parse.** { *; }
#-keep class com.facebook.** { *; }
#-keepclassmembers class com.parse.** { *; }

# Keep source file names, line numbers, and Parse class/method names for easier debugging
#-keepattributes SourceFile,LineNumberTable
#-keepnames class com.parse.** { *; }

# Required for Parse
#-keepattributes *Annotation*
#-keepattributes Signature
#-dontwarn android.net.SSLCertificateSocketFactory
#-dontwarn android.app.Notification
#-dontwarn com.squareup.**
#-dontwarn okio.**
#-dontwarn com.facebook.**

#-keepattributes Signature
#
#-dontwarn com.facebook.**
#-dontwarn com.parse.**
#
#-keep class com.facebook.** { *; }
#-keep class com.parse.** { *; }

# Keep source file names, line numbers, and Parse class/method names for easier debugging
-keepattributes SourceFile,LineNumberTable
-keepnames class com.parse.** { *; }
-keep class com.parse.** { *; }

# Required for Parse
-keepattributes *Annotation*
-keepattributes Signature
-dontwarn com.squareup.**
-dontwarn okio.**
-dontwarn com.parse.**