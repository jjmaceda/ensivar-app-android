package net.clevermobileapps.ensivar;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.vision.text.Line;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import net.clevermobileapps.ensivar.adapters.CompanyAdapter;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.interfaces.RecycleViewRowVariant;
import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.models.Separator;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;
import net.clevermobileapps.ensivar.utils.DividerItemDecoration;
import net.clevermobileapps.ensivar.utils.RecyclerViewEmptySupport;

import java.util.ArrayList;
import java.util.List;

import br.com.customsearchable.SearchActivity;
import br.com.customsearchable.contract.CustomSearchableConstants;
import br.com.customsearchable.model.ResultItem;

public class AppSearchActivity extends AppCompatActivity {
    final static String TAG = "AppSearchActivity";
    private CompanyAdapter adapter;
    public static final String TAG_SEARCH = "tagSearch";
    private List tags;
    private ProgressBar pb;
    private LinearLayout no_result;
    private RecyclerViewEmptySupport mListView;
    private ArrayList<RecycleViewRowVariant> mList = new ArrayList<>();
    private ParseQuery<Company> companyParseQuery;
    private ParseQuery<Company> companyParseQuery1;

    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        EnsivarApplication application = (EnsivarApplication) getApplication();
        mTracker = application.getDefaultTracker();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        Button suggest_from_search_btn = (Button) findViewById(R.id.suggest_from_search_btn);
        if(suggest_from_search_btn != null) {
            suggest_from_search_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AppSearchActivity.this, WebActivity.class);
                    intent.putExtra("endPoint", WebActivity.SUGGEST_URL);
                    intent.putExtra("name", R.string.suggestion_title);
                    startActivity(intent);
                }
            });
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if(fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Fab Search")
                            .build());

                    Intent intent = new Intent(AppSearchActivity.this, SearchActivity.class);
                    startActivity(intent);
                }
            });
        }

        pb = (ProgressBar) findViewById(R.id.pb);

        no_result = (LinearLayout) findViewById(R.id.no_result);
        mListView = (RecyclerViewEmptySupport) findViewById(R.id.list);
        if(mListView != null){
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            mListView.setLayoutManager(mLayoutManager);
            mListView.addItemDecoration(new DividerItemDecoration(AppSearchActivity.this));
            mListView.setEmptyView(no_result);

        }


        String type = getIntent().getStringExtra("type");
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            //Log.d(TAG, "onCreate");
            doQuerySearch(query);
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // Receives suggested clicked item
            Bundle bundle = intent.getExtras();

            if (bundle != null) {
                ResultItem receivedItem = bundle.getParcelable(CustomSearchableConstants.CLICKED_RESULT_ITEM);

                // Do complex processing
                if (receivedItem != null) {
                    doQuerySearch(receivedItem.getHeader());
                }

            }
        } else if (type != null && type.equals(TAG_SEARCH)) {
            doTagSearch();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            //Log.d(TAG, "onNewIntent");
            doQuerySearch(query);
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // Receives suggested clicked item
            Bundle bundle = intent.getExtras();

            if (bundle != null) {
                ResultItem receivedItem = bundle.getParcelable(CustomSearchableConstants.CLICKED_RESULT_ITEM);

                // Do complex processing
                if (receivedItem != null) {
                    doQuerySearch(receivedItem.getHeader());
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.SEARCH_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onPause() {
        super.onPause();

        if(companyParseQuery != null){
            companyParseQuery.cancel();
        }
        if(companyParseQuery1 != null){
            companyParseQuery1.cancel();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            // Calls Custom Searchable Activity
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item); // important line
    }

    private void doQuerySearch(String query) {
        no_result.setVisibility(View.GONE);
        pb.setVisibility(View.VISIBLE);
        mList = new ArrayList<>();

        final String lastQuery = query.trim();

        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Search")
                .setLabel(lastQuery)
                .build());

        if(companyParseQuery == null){
            companyParseQuery = ParseQuery.getQuery("Company");
        }
        companyParseQuery.cancel();
        companyParseQuery.addAscendingOrder("name");
        companyParseQuery.whereMatches("name", lastQuery, "i");
        companyParseQuery.whereEqualTo("status", 1);
        companyParseQuery.setLimit(1000);
        companyParseQuery.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
        companyParseQuery.setMaxCacheAge(Constants.MAX_CACHE_TIME);
        companyParseQuery.findInBackground(new FindCallback<Company>() {
            @Override
            public void done(List<Company> objects, ParseException e) {
                if (e == null) {
                    for(Company company:objects){
                        mList.add(company);
                    }
                    setAdapter(false);
                }else{
                    pb.setVisibility(View.GONE);
                    mListView.setAdapter(null);
                }
            }
        });
    }

    private void doTagSearch() {
        no_result.setVisibility(View.GONE);
        pb.setVisibility(View.VISIBLE);
        mList = new ArrayList<>();

        tags = (List) getIntent().getStringArrayListExtra("tags");
        String tagName = getIntent().getStringExtra("tagName");

        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Tag Search")
                .setLabel(tagName)
                .build());

        if(companyParseQuery1 == null){
            companyParseQuery1 = ParseQuery.getQuery("Company");
        }
        companyParseQuery1.cancel();
        companyParseQuery1.addDescendingOrder("featured");
        companyParseQuery1.addAscendingOrder("name");
        companyParseQuery1.whereContainedIn("tags", tags);
        companyParseQuery1.whereEqualTo("status", 1);
        companyParseQuery1.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
        companyParseQuery1.setMaxCacheAge(Constants.MAX_CACHE_TIME);
        companyParseQuery1.findInBackground(new FindCallback<Company>() {
            @Override
            public void done(List<Company> companyList, ParseException e) {
                if (e == null) {
                    boolean hasFeaturedCompanies = false;
                    for(Company company:companyList){
                        if(company.getFeatured()){
                            mList.add(company);
                            hasFeaturedCompanies = true;
                        }
                    }

                    if(hasFeaturedCompanies){
                        mList.add(new Separator());
                    }

                    for(Company company:companyList){
                        if(!company.getFeatured()){
                            mList.add(company);
                        }
                    }

                    setAdapter(true);
                }else{
                    pb.setVisibility(View.GONE);
                    mListView.setAdapter(null);
                }
            }
        });
    }

    private void setAdapter(boolean isFeatured){
        adapter = new CompanyAdapter(mList, AppSearchActivity.this, isFeatured);
        if(mList.size() > 0){
            mListView.setAdapter(adapter);
            setAdapterCallbacks();
        }else{
            mListView.setAdapter(null);
        }
        pb.setVisibility(View.GONE);
    }

    private void setAdapterCallbacks(){
        adapter.companyCallbackListener(new Callback<Company>() {
            @Override
            public void success(Company company) {
                if(company.getSponsor()){ //if is sponsor company
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(Constants.GA_STATS_CATEGORY)
                            .setAction(Constants.GA_STATS_ACTION_SPONSOR_COMPANY)
                            .setLabel(company.getObjectId() + "|" + company.getName())
                            .build());
                }
                startActivity(Common.getInstance().createIntentForLocationByCompany(AppSearchActivity.this, company));
            }

            @Override
            public void error(String error) {

            }
        });
    }

}
