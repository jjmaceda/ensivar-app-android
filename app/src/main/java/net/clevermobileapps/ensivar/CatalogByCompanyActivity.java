package net.clevermobileapps.ensivar;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import net.clevermobileapps.ensivar.adapters.CatalogAdapter;
import net.clevermobileapps.ensivar.adapters.CompanyProductAdapter;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.models.Catalog;
import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.models.Profile;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;
import net.clevermobileapps.ensivar.utils.RecyclerViewEmptySupport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CatalogByCompanyActivity extends AppCompatActivity {
    private final String TAG = "CatalogByCompanyAct";

    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private static final int SPAN_COUNT = 1;
    protected LayoutManagerType mCurrentLayoutManagerType;

    private RecyclerViewEmptySupport mRecycleView;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Catalog> mList = new ArrayList<>();
    private CatalogAdapter catalogAdapter;

    private String message;
    private boolean isRedeem = false;
    private String id_company;
    private String company_name;
    private int companyPoints = 0;

    private ProgressBar pb;
    private ProgressDialog progressDialog;

    private Catalog currentCatalog;

    private Bundle args = new Bundle();
    private JSONArray pointsArray = new JSONArray();

    private Profile mProfile;

    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog_by_company);

        EnsivarApplication application = (EnsivarApplication) getApplication();
        mTracker = application.getDefaultTracker();

        Intent intent = getIntent();
        id_company = intent.getStringExtra("id_company");
        company_name = intent.getStringExtra("company_name");

        String btn_clicked = intent.getStringExtra(CompanyProductAdapter.BTN_KEY);
        args.putString(CompanyProductAdapter.BTN_KEY, btn_clicked);
        if (btn_clicked.contentEquals(CompanyProductAdapter.BTN_REDEEM)) {
            isRedeem = true;
        }

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            final ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(getResources().getString(R.string.catalogActivityTitle));
                actionBar.setDisplayShowTitleEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setHomeButtonEnabled(true);
            }
        }

        progressDialog = Common.getInstance().getProgressDialog(this);
        pb = (ProgressBar) findViewById(R.id.pb);
        if (pb != null) {
            pb.setVisibility(View.VISIBLE);
        }

        mRecycleView = (RecyclerViewEmptySupport) findViewById(R.id.catalogList);
        if (mRecycleView != null) {
            mRecycleView.setEmptyView(findViewById(R.id.no_result));
            prepareRecyclerView(savedInstanceState);
        }
        loadProfilePoints();

    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.CATALOG_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_points, menu);
        if (isRedeem) {
            MenuItem item = menu.findItem(R.id.action_open_scanner);
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.action_open_scanner) {
            if (Common.isUserLoggedIn()) {
                openBarcodeScannerSteps();
            } else {
                goToProfile();
            }
        } else if (id == R.id.action_info) {
            if (message != null && !message.isEmpty()) {
                createCouponInfoAlert(message);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    Log.d(TAG, "Barcode read: " + barcode.displayValue);
                    if (!barcode.displayValue.isEmpty()) {
                        progressDialog.show();
                        verifyAuthenticityOfLocation(barcode.displayValue);
                    }
                } else {
                    Log.d(TAG, "No barcode captured, intent data is null");
                }
            } else {
                Log.d(TAG, String.format(getString(R.string.barcode_error),
                        CommonStatusCodes.getStatusCodeString(resultCode)));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    private void prepareRecyclerView(Bundle savedInstanceState) {
        mLayoutManager = new LinearLayoutManager(this);
        mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;

        if (savedInstanceState != null) {
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState.getSerializable
                    (KEY_LAYOUT_MANAGER);
        }

        int scrollPosition = 0;

        if (mRecycleView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecycleView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (mCurrentLayoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(this, SPAN_COUNT);
                mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(this);
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(this);
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecycleView.setLayoutManager(mLayoutManager);
        mRecycleView.scrollToPosition(scrollPosition);
        mRecycleView.setEmptyView(findViewById(R.id.no_result));
    }

    private void loadProfilePoints() {
        Common.getInstance().getUserProfile(new Callback<Profile>() {
            @Override
            public void success(Profile profile) {
                mProfile = profile;
                pointsArray = profile.getPoints();
                if (pointsArray != null) {
                    int len = pointsArray.length();
                    for (int i = 0; i < len; i++) {
                        try {
                            JSONObject obj = (JSONObject) pointsArray.get(i);
                            if (obj.getString("id_company").contentEquals(id_company)) {
                                companyPoints = obj.getInt("points");
                                break;
                            }
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
                loadCatalog();
            }

            @Override
            public void error(String error) {
                loadCatalog();
            }
        });
    }

    private void loadCatalog() {
        Common.getInstance().getCompany(id_company, new Callback<Company>() {
            @Override
            public void success(Company company) {
                JSONArray loyaltyCatalogs = company.getLoyaltyCatalog();

                if (loyaltyCatalogs != null) {
                    int len = loyaltyCatalogs.length();
                    for (int i = 0; i < len; i++) {
                        try {
                            JSONObject obj = (JSONObject) loyaltyCatalogs.get(i);
                            String id_product = obj.getString("id_product");
                            String image = obj.getString("image");
                            String description = obj.getString("description");
                            int points = obj.getInt("points");

                            mList.add(new Catalog(id_company, id_product, image, description,
                                    points, companyPoints));

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                }

                if (mList.size() > 0) {
                    catalogAdapter = new CatalogAdapter(mList, CatalogByCompanyActivity.this, args);
                    if (mRecycleView != null) {
                        mRecycleView.setAdapter(catalogAdapter);
                    }
                    setAdapterCallbacks();
                } else {
                    mRecycleView.setAdapter(null);
                }
                message = isRedeem ? company.getExplanationRedeem() : company
                        .getExplanationAccumulate();
                pb.setVisibility(View.GONE);
            }

            @Override
            public void error(String error) {
                mRecycleView.setAdapter(null);
                pb.setVisibility(View.GONE);
            }
        });
    }

    private void openBarcodeScanner() {
        Intent intent = new Intent(CatalogByCompanyActivity.this, BarcodeCaptureActivity.class);
        startActivityForResult(intent, Constants.RC_BARCODE_CAPTURE);
    }

    private void openBarcodeScannerSteps() {
        if (Common.getInstance().isInternetAvailable(this)) {
            Intent intent = new Intent(CatalogByCompanyActivity.this, PointsStepsActivity.class);
            intent.putExtra("id_company", id_company);
            intent.putExtra("company_name", company_name);
            intent.putExtra("points", companyPoints);
            startActivity(intent);
        } else {
            Toast.makeText(this, R.string.need_inter_scanner, Toast.LENGTH_SHORT).show();
        }
    }

    private void setAdapterCallbacks() {
        catalogAdapter.ProductCatalogCallbackListener(new Callback<Catalog>() {
            @Override
            public void success(Catalog catalog) {
                currentCatalog = catalog;
                if (catalog.companyPoints >= catalog.points) {
                    openBarcodeScanner();
                } else {
                    Toast.makeText(CatalogByCompanyActivity.this, getResources().getString(R
                            .string.userDoesNotHaveEnoughPointsError), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void error(String error) {

            }
        });
    }

    private void verifyAuthenticityOfLocation(String barcodeValue) {
        try {
            JSONObject jsonObject = new JSONObject(barcodeValue);
            final String id_location = jsonObject.getString("id_location");
            final int points;
            points = currentCatalog.points;

            Common.getInstance().verifyAuthenticityOfLocation(id_company, id_location, new
                    Callback<Boolean>() {
                @Override
                public void success(Boolean aBoolean) {
                    redeemPoints(points, id_location);
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(CatalogByCompanyActivity.this, getResources().getString(R
                            .string.locationIsNotOfCompany), Toast.LENGTH_LONG).show();
                }
            });

        } catch (JSONException e) {
            progressDialog.dismiss();
            Toast.makeText(CatalogByCompanyActivity.this, getResources().getString(R.string
                    .qrError), Toast.LENGTH_LONG).show();
        }
    }

    private void redeemPoints(final int redeemPoints, final String id_location) {
        if (pointsArray != null) {
            int len = pointsArray.length();

            for (int i = 0; i < len; i++) {
                try {
                    JSONObject object = (JSONObject) pointsArray.get(i);
                    int totalPoints = object.getInt("points") - redeemPoints;
                    ;
                    if (object.getString("id_company").contentEquals(id_company)) {
                        object.put("id_company", id_company);
                        object.put("points", totalPoints);
                        companyPoints = totalPoints;
                        break;
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }

            mProfile.put("points", pointsArray);
            mProfile.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {

                        saveRedeemStats(id_company, id_location, redeemPoints);

                        progressDialog.dismiss();
                        updateCompanyProductPoints();
                        createRedeemAlertDialog();
                    } else {
                        progressDialog.dismiss();
                    }
                }
            });
        }
    }

    private void updateCompanyProductPoints() {
        List<Catalog> catalogArrayList = catalogAdapter.getItems();
        if (catalogArrayList.size() > 0) {
            for (Catalog catalog : catalogArrayList) {
                catalog.companyPoints = companyPoints;
            }
        }
        catalogAdapter.notifyDataSetChanged();
    }


    private void createRedeemAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(CatalogByCompanyActivity.this);

        String alertMessage = getResources().getString(R.string.pointsRedeemedSuccessfully) + " "
                + company_name;
        builder.setMessage(alertMessage);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.setNegativeButton(R.string.goToPassbook, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(CatalogByCompanyActivity.this, DirectoryActivity.class);
                intent.putExtra("viewPagerPosition", 1);
                startActivity(intent);
            }
        });

        AlertDialog select_general_dialog;
        select_general_dialog = builder.create();
        select_general_dialog.show();
    }

    private void createCouponInfoAlert(String message) {
        String title = getResources().getString(R.string.howToAccumulatePoints);
        if (isRedeem) {
            title = getResources().getString(R.string.howToRedeemPoints);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setNegativeButton(getResources().getString(R.string.closeBtnLabel), new
                DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog;
        alertDialog = builder.create();
        alertDialog.show();
    }

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }

    public void saveRedeemStats(String id_company, String id_location, int points) {
        ParseObject stat = new ParseObject("Stat");
        stat.put("company", ParseObject.createWithoutData("Company", id_company));
        stat.put("location", ParseObject.createWithoutData("Location", id_location));
        stat.put("user", ParseUser.getCurrentUser());
        stat.put("redeem_points", points);
        stat.put("id_product", currentCatalog.id_product);

        stat.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
            }
        });
    }

    private void goToProfile() {
        Intent intent = new Intent(CatalogByCompanyActivity.this, DirectoryActivity.class);
        intent.putExtra("viewPagerPosition", 1);
        intent.putExtra("message", R.string.loggedInFacebookCatalog);
        startActivity(intent);
    }
}
