package net.clevermobileapps.ensivar;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import net.clevermobileapps.ensivar.adapters.CompanyProductAdapter;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.interfaces.RecycleViewRowVariant;
import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.models.Profile;
import net.clevermobileapps.ensivar.models.PromoCoupon;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;
import net.clevermobileapps.ensivar.utils.RecyclerViewEmptySupport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class CouponsByCompanyActivity extends AppCompatActivity {
    public static String TAG = "CouponsByCompanyActivity";
    private RecyclerViewEmptySupport mListView;
    private ArrayList<RecycleViewRowVariant> mList = new ArrayList<>();
    private CompanyProductAdapter companyProductAdapter;
    private ProgressBar pb;
    private ProgressDialog progressDialog;
    private String id_company;
    private ParseQuery<PromoCoupon> couponsQueryWithoutCache;

    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupons_by_company);

        EnsivarApplication application = (EnsivarApplication) getApplication();
        mTracker = application.getDefaultTracker();

        id_company = getIntent().getStringExtra("id_company");

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(toolbar != null){
            setSupportActionBar(toolbar);

            final ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(getResources().getString(R.string.couponsActivityTitle));
                actionBar.setDisplayShowTitleEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setHomeButtonEnabled(true);
            }
        }
        progressDialog = Common.getInstance().getProgressDialog(this);

        pb = (ProgressBar) findViewById(R.id.pb);
        pb.setVisibility(View.VISIBLE);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mListView = (RecyclerViewEmptySupport) findViewById(R.id.couponList);
        if (mListView != null) {
            mListView.setLayoutManager(mLayoutManager);
            mListView.setEmptyView(findViewById(R.id.no_result));
        }

        loadCoupons();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.COUPONS_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(couponsQueryWithoutCache != null){
            couponsQueryWithoutCache.cancel();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadCoupons(){
        Common.getInstance().getCoupons(id_company, new Callback<List<PromoCoupon>>() {
            @Override
            public void success(List<PromoCoupon> promoCouponList) {
                setQuantityByPromoCoupon(promoCouponList);
            }

            @Override
            public void error(String error) {
                mListView.setAdapter(null);
                pb.setVisibility(View.GONE);
            }
        });

    }

    private void setQuantityByPromoCoupon(final List<PromoCoupon> couponListWithCache){
        ParseQuery innerQuery = new ParseQuery("Company");
        innerQuery.whereEqualTo("objectId", id_company);

        if(couponsQueryWithoutCache == null) {
            couponsQueryWithoutCache = ParseQuery.getQuery("PromoCoupon");
        }
        couponsQueryWithoutCache.cancel();

        couponsQueryWithoutCache.whereEqualTo("type", "C");
        couponsQueryWithoutCache.whereEqualTo("active", true);
        couponsQueryWithoutCache.whereNotEqualTo("featured", true);
        couponsQueryWithoutCache.selectKeys(Collections.singletonList("quantity"));
        couponsQueryWithoutCache.whereMatchesQuery("company", innerQuery);
        couponsQueryWithoutCache.findInBackground(new FindCallback<PromoCoupon>() {
            @Override
            public void done(List<PromoCoupon> couponListWithoutCache, ParseException e) {
                if(e == null){

                    HashMap<String, Integer> quantities = new HashMap<>();
                    for(PromoCoupon coupon: couponListWithoutCache){
                        quantities.put(coupon.getObjectId(), coupon.getQuantity());
                    }

                    for(PromoCoupon promoCoupon : couponListWithCache){
                            if(Constants.today.before(promoCoupon.getValidUntil())){
                            int quantity = quantities.get(promoCoupon.getObjectId());
                            if(quantity > 0){
                                promoCoupon.setQuantity(quantity);
                            }else{
                                promoCoupon.setQuantity(0);
                            }

                            promoCoupon.setIsRedeem(false);
                            mList.add(promoCoupon);
                        }
                    }

                    if (mList.size() > 0) {
                        companyProductAdapter = new CompanyProductAdapter(mList, CouponsByCompanyActivity.this);
                        if (mListView != null) {
                            mListView.setAdapter(companyProductAdapter);
                        }
                        setAdapterCallbacks();
                    } else {
                        mListView.setAdapter(null);
                    }

                }else{
                    mListView.setAdapter(null);
                }
                pb.setVisibility(View.GONE);
            }
        });

    }

    private void setAdapterCallbacks(){
        //ADD COUPON TO MY PASSBOOK CLICK EVENT
        companyProductAdapter.BtnCouponCallbackListener(new Callback<PromoCoupon>() {
            @Override
            public void success(PromoCoupon promoCoupon) {
                if(Common.isUserLoggedIn()){
                    if(promoCoupon.getQuantity() > 0){
                        passbook(promoCoupon);
                    }else{
                        Toast.makeText(CouponsByCompanyActivity.this, getResources().getString(R.string.couponQuantityError), Toast.LENGTH_LONG).show();
                    }
                }else{
                    goToProfile();
                }
            }

            @Override
            public void error(String error) {

            }
        });
        //COUPON LOGO CLICK EVENT
        companyProductAdapter.LogoPromoCouponCallbackListener(new Callback<PromoCoupon>() {
            @Override
            public void success(PromoCoupon promoCoupon) {
                Company company = (Company) promoCoupon.getCompany();
                if(company != null){
                    startActivity(Common.getInstance().createIntentForLocationByCompany(CouponsByCompanyActivity.this, company));
                }
            }

            @Override
            public void error(String error) {

            }
        });

    }

    private void passbook(final PromoCoupon promoCoupon){
        progressDialog.show();

        Common.getInstance().getUserProfile(new Callback<Profile>() {
            @Override
            public void success(Profile profile) {
                addCoupon(profile, promoCoupon);
            }

            @Override
            public void error(String error) {
                progressDialog.dismiss();
            }
        });
    }

    private void addCoupon(Profile profile, PromoCoupon coupon){
        JSONArray coupons = profile.getCoupons();
        if(coupons == null){
            coupons = new JSONArray();
        }
        int len = coupons.length();
        boolean couponAllReadyExist = false;
        final String couponId = coupon.getObjectId();

        for (int i=0;i<len;i++){
            try {
                JSONObject obj = (JSONObject) coupons.get(i);
                String objectId = obj.getString("objectId");
                if(objectId.contentEquals(couponId)){
                    couponAllReadyExist = true;
                    break;
                }
            }catch (JSONException e1){
                e1.printStackTrace();
            }
        }

        if(!couponAllReadyExist){
            JSONObject obj = new JSONObject();
            try {
                obj.put("objectId", couponId);
                obj.put("redeemed", false);
                coupons.put(obj);
                profile.put("coupons", coupons);
                profile.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e2) {
                        if(e2 == null){
                            decrementQuantityByCoupon(couponId);
                            couponAddedToPassbookDialog();
                        }else{
                            progressDialog.dismiss();
                        }
                    }
                });
            }catch (JSONException e1){
                e1.printStackTrace();
                progressDialog.dismiss();
            }
        }else{
            progressDialog.dismiss();
            Toast.makeText(CouponsByCompanyActivity.this, getResources().getString(R.string.couponAllReadyExistToast), Toast.LENGTH_SHORT).show();
        }
    }

    private void decrementQuantityByCoupon(final String couponId){
        ParseQuery<PromoCoupon> couponParseQuery = ParseQuery.getQuery("PromoCoupon");
        couponParseQuery.getInBackground(couponId, new GetCallback<PromoCoupon>() {
            public void done(PromoCoupon coupon, ParseException e) {
                if (e == null) {
                    coupon.increment("quantity", -1);
                    coupon.saveInBackground();
                    updateCouponItems(couponId);
                }else{
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void updateCouponItems(String couponId){
        List<RecycleViewRowVariant> couponsList = companyProductAdapter.getItems();
        if(couponsList.size() > 0){
            for(int i = 0; i < couponsList.size(); i++){
                PromoCoupon promoCoupon = (PromoCoupon) couponsList.get(i);
                if(promoCoupon.getObjectId().contentEquals(couponId)){
                    int qty = promoCoupon.getQuantity() - 1;
                    promoCoupon.setQuantity(qty);
                    break;
                }
            }
            companyProductAdapter.notifyDataSetChanged();
        }

        progressDialog.dismiss();
    }

    private void couponAddedToPassbookDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.couponAddedToPassbookToast));
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.setNegativeButton(R.string.goToPassbook, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                goToProfile();
            }
        });

        AlertDialog select_general_dialog;
        select_general_dialog = builder.create();
        select_general_dialog.show();
    }


    private void goToProfile(){
        Intent intent = new Intent(CouponsByCompanyActivity.this, DirectoryActivity.class);
        intent.putExtra("viewPagerPosition", 1);
        intent.putExtra("message", R.string.loggedInFacebookCoupon);
        startActivity(intent);
    }

}
