package net.clevermobileapps.ensivar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;

import net.clevermobileapps.ensivar.adapters.DirectoryFragmentPagerAdapter;
import net.clevermobileapps.ensivar.services.GeofenceZonesLoadedService;
import net.clevermobileapps.ensivar.services.NotiService;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;
import net.clevermobileapps.ensivar.utils.Mocks;

import br.com.customsearchable.SearchActivity;
import hotchemi.android.rate.AppRate;

public class DirectoryActivity extends AppCompatActivity {
    public static String TAG = "DirectoryActivity";
    private ViewPager viewPager;
    private Tracker mTracker;
    private Bundle args = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_directory);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Fab Search")
                            .build());

                    Intent intent = new Intent(DirectoryActivity.this, SearchActivity.class);
                    startActivity(intent);
                }
            });
        }

//        //TODO REMOVE
//        FloatingActionButton open_geofence_logs = (FloatingActionButton) findViewById(R.id.open_geofence_logs);
//        if (open_geofence_logs != null) {
//            open_geofence_logs.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(DirectoryActivity.this, GeofenceHandleLogActivity.class);
//                    startActivity(intent);
//                }
//            });
//        }

        EnsivarApplication application = (EnsivarApplication) getApplication();
        mTracker = application.getDefaultTracker();

        createFragments();

        //Set view pager position
        Intent intent = getIntent();
        int viewPagerPosition = intent.getIntExtra("viewPagerPosition", -1);
        int message = intent.getIntExtra("message", -1);

        setCurrentTab(viewPagerPosition);

        args.putInt("viewPagerPosition", viewPagerPosition);
        if(viewPagerPosition == 1 && !Common.isUserLoggedIn()){
            if(message != -1){
                Toast.makeText(DirectoryActivity.this, getResources().getString(message), Toast.LENGTH_LONG).show();
            }
        }

        setupTabs();

        AppRate.with(this)
                .setInstallDays(1)
                .setLaunchTimes(3)
                .setDebug(false)
                .monitor();

        AppRate.showRateDialogIfMeetsConditions(this);

        Common.getInstance().geofenceInit(this);

        Mocks.getInstance().compareTimes(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.DIRECTORY_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }

    private void setCurrentTab(int position) {
        if (viewPager != null) {
            viewPager.setCurrentItem(position);
        }
    }

    private void createFragments() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        DirectoryFragmentPagerAdapter adapter = DirectoryFragmentPagerAdapter.newInstance(getSupportFragmentManager(), args);

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                final TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
                if (tabLayout != null) {
                    TabLayout.Tab tab = tabLayout.getTabAt(position);

                    resetTabsIcons(tabLayout);

                    if (tab != null) {
                        switch (position) {
                            case 0:
                                tab.setIcon(R.drawable.ic_label_white_24dp);
                                break;
                            case 1:
                                tab.setIcon(R.drawable.ic_account_circle_white_24dp);
                                break;
                            case 2:
                                tab.setIcon(R.drawable.ic_sort_by_alpha_white_24dp);
                                break;
                        }
                    }
                }
            }

            @Override
            public void onPageSelected(int position) {
                //saveLastTab(position);
                switch (position) {
                    case 0:
                        setTitle(getString(R.string.tags_title));
                        mTracker.setScreenName(Constants.TAGS_FRAGMENT);
                        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                        break;
                    case 1:
                        setTitle(getString(R.string.favorite_title));
                        mTracker.setScreenName(Constants.FAVORITE_FRAGMENT);
                        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                        break;
                    case 2:
                        setTitle(getString(R.string.directory_title));
                        mTracker.setScreenName(Constants.
                                DIRECTORY_LIST_FRAGMENT);
                        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private Drawable tintIcon(int icon) {
        final Drawable drawable = ResourcesCompat.getDrawable(getResources(), icon, null);
        if (drawable != null) {
            final Drawable wrapped = DrawableCompat.wrap(drawable);
            try {
                drawable.mutate();
                DrawableCompat.setTint(wrapped, ContextCompat.getColor(getApplicationContext(), R.color.black_alpha));
                return wrapped;
            } catch (NullPointerException e) {
                throw new NullPointerException();
            }
        }

        throw new NullPointerException();
    }

    private void setupTabs() {
        // Give the TabLayout the ViewPager
        final TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);
        }

        resetTabsIcons(tabLayout);
    }

    private void resetTabsIcons(TabLayout tabLayout) {
        TabLayout.Tab directoryTab = tabLayout.getTabAt(2);

        if(directoryTab != null) {
            try {
                final Drawable icon = tintIcon(R.drawable.ic_sort_by_alpha_black_24dp);
                directoryTab.setIcon(icon);
            } catch (NullPointerException e) {
                directoryTab.setIcon(R.drawable.ic_sort_by_alpha_black_24dp);
            }
        }

        TabLayout.Tab tagsTab = tabLayout.getTabAt(0);
        if(tagsTab != null) {
            try {
                final Drawable icon = tintIcon(R.drawable.ic_label_black_24dp);
                tagsTab.setIcon(icon);
            } catch (NullPointerException e) {
                tagsTab.setIcon(R.drawable.ic_label_black_24dp);
            }
        }

        TabLayout.Tab fabTab = tabLayout.getTabAt(1);
        if(fabTab != null) {
            try {
                final Drawable icon = tintIcon(R.drawable.ic_account_circle_black_24dp);
                fabTab.setIcon(icon);
            } catch (NullPointerException e) {
                fabTab.setIcon(R.drawable.ic_account_circle_black_24dp);
            }
        }
    }

}


