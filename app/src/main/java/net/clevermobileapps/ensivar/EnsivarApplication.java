package net.clevermobileapps.ensivar;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.facebook.FacebookSdk;
import com.google.ads.conversiontracking.AdWordsConversionReporter;
import com.google.android.gms.analytics.ExceptionReporter;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.onesignal.OneSignal;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.interceptors.ParseLogInterceptor;

import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.models.Favorite;
import net.clevermobileapps.ensivar.models.GeofenceEnSivar;
import net.clevermobileapps.ensivar.models.GeofenceZone;
import net.clevermobileapps.ensivar.models.Location;
import net.clevermobileapps.ensivar.models.Profile;
import net.clevermobileapps.ensivar.models.PromoCoupon;
import net.clevermobileapps.ensivar.models.Tag;
import net.clevermobileapps.ensivar.utils.ConfigHelper;

public class EnsivarApplication extends Application {

    private Tracker mTracker;
    private static ConfigHelper configHelper;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        FacebookSdk.sdkInitialize(getApplicationContext());

        //Parse.enableLocalDatastore(this);
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId(getResources().getString(R.string.parse_app_id))
                .clientKey(getResources().getString(R.string.parse_client_key))
//                .addNetworkInterceptor(new ParseLogInterceptor())
                .server("https://parse.ensivarapp.com:8081/")
                .build());
//        Parse.initialize(this);
        ParseFacebookUtils.initialize(this);

        ParseObject.registerSubclass(Company.class);
        ParseObject.registerSubclass(Location.class);
        ParseObject.registerSubclass(Tag.class);
        ParseObject.registerSubclass(Favorite.class);
        ParseObject.registerSubclass(PromoCoupon.class);
        ParseObject.registerSubclass(Profile.class);
        ParseObject.registerSubclass(GeofenceZone.class);
        ParseObject.registerSubclass(GeofenceEnSivar.class);

        //ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
        ParseACL.setDefaultACL(defaultACL, true);

        Thread.UncaughtExceptionHandler myHandler = new ExceptionReporter(
                getDefaultTracker(),                           // Currently used Tracker.
                Thread.getDefaultUncaughtExceptionHandler(),   // Current default uncaught exception handler.
                this);                                         // Context of the application.
        // Make myHandler the new default uncaught exception handler.
        Thread.setDefaultUncaughtExceptionHandler(myHandler);


        // En Sivar (Android) first use
        // Google Android first open conversion tracking snippet
        // Add this code to the onCreate() method of your application activity

        AdWordsConversionReporter.reportWithConversionId(this.getApplicationContext(),
                "1045591031", "JA__CISvl2QQ9-fJ8gM", "0.00", false);

        OneSignal.startInit(this).init();

        configHelper = new ConfigHelper();
        configHelper.fetchConfigIfNeeded();

    }

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            if (BuildConfig.BUILD_TYPE.equals("debug")) {
                mTracker = analytics.newTracker("UA-72467404-2");
            } else {
                mTracker = analytics.newTracker("UA-72467404-1");
            }

            mTracker.enableAdvertisingIdCollection(true);
//            mTracker.enableExceptionReporting(true);
        }
        return mTracker;
    }

    public static ConfigHelper getConfigHelper() {
        return configHelper;
    }
}
