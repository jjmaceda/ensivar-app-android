package net.clevermobileapps.ensivar;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.clevermobileapps.ensivar.utils.Constants;

import java.util.Map;

public class GeofenceHandleLogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geofence_handle_log);

        LinearLayout logs_container_ly = (LinearLayout) findViewById(R.id.logs_container);

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.HANDLE_GEOFENCE_LOGS_PREFERENCE, MODE_PRIVATE);
        Map<String, ?> keys = sharedPreferences.getAll();

        for(Map.Entry<String, ?> entry: keys.entrySet()){
            TextView log_tv = new TextView(this);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.bottomMargin = 8;
            log_tv.setLayoutParams(layoutParams);

            String value = "Geofence ID: "+entry.getKey() +"\nTime: "+entry.getValue();
            log_tv.setText(value);

            if(logs_container_ly!=null){
                logs_container_ly.addView(log_tv);
            }
        }
    }
}
