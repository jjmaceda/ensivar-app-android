package net.clevermobileapps.ensivar;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import net.clevermobileapps.ensivar.adapters.CompanyProductAdapter;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.interfaces.RecycleViewRowVariant;
import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.models.Profile;
import net.clevermobileapps.ensivar.models.PromoCoupon;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.RecyclerViewEmptySupport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GeofenceNotificationActivity extends AppCompatActivity {
    public static String TAG = "GeofenceNotificationActivity";
    private ArrayList<RecycleViewRowVariant> mList = new ArrayList<>();
    private RecyclerViewEmptySupport mListView;
    private ProgressBar pb;
    private ProgressDialog progressDialog;
    private Date validUntil;
    private LinearLayout userNotLoggedInWrapper;
    private ArrayList<String> promoCouponKeys;
    private CompanyProductAdapter companyProductAdapter;
    private ParseQuery<PromoCoupon> promoCouponQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geofence_notification);

        Intent intent = getIntent();
        promoCouponKeys = intent.getStringArrayListExtra("promoCouponKeys");
        validUntil = Common.addDays(new Date(), 2);

        progressDialog = Common.getInstance().getProgressDialog(this);

        pb = (ProgressBar) findViewById(R.id.pb);
        if (pb != null) {
            pb.setVisibility(View.VISIBLE);
        }

        mListView = (RecyclerViewEmptySupport) findViewById(R.id.passbookList);
        if (mListView != null) {
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            mListView.setLayoutManager(mLayoutManager);
            mListView.setEmptyView(findViewById(R.id.no_result));
        }

        if (!Common.isUserLoggedIn()) {
            userNotLoggedInWrapper = (LinearLayout) findViewById(R.id.userNotLoggedInWrapper);
            if (userNotLoggedInWrapper != null) {
                userNotLoggedInWrapper.setVisibility(View.VISIBLE);
            }
            Button fb_start_btn = (Button) findViewById(R.id.fb_start_btn);
            if (fb_start_btn != null) {
                fb_start_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loginWithFacebook();
                    }
                });
            }
        }

        loadPromoCoupons();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (promoCouponQuery != null) {
            promoCouponQuery.cancel();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }

    private void loadPromoCoupons() {
        if (promoCouponQuery == null) {
            promoCouponQuery = ParseQuery.getQuery("PromoCoupon");
        }

        promoCouponQuery.cancel();
        promoCouponQuery.whereContainedIn("objectId", promoCouponKeys);
        promoCouponQuery.include("company");
        promoCouponQuery.findInBackground(new FindCallback<PromoCoupon>() {
            @Override
            public void done(List<PromoCoupon> promoCouponList, ParseException e) {
                if (e == null) {

                    for (PromoCoupon promoCoupon : promoCouponList) {
                        promoCoupon.setValidUntil(validUntil);
                        mList.add(promoCoupon);
                    }

                    if (mList.size() > 0) {
                        companyProductAdapter = new CompanyProductAdapter(mList,
                                GeofenceNotificationActivity.this);
                        if (mListView != null) {
                            mListView.setAdapter(companyProductAdapter);
                            setAdapterCallbacks();
                        }
                    } else {
                        mListView.setAdapter(null);
                    }
                } else {
                    mListView.setAdapter(null);
                }
                pb.setVisibility(View.GONE);
            }
        });
    }

    private void setAdapterCallbacks() {
        //LOGO COUPON CLICK EVENT
        companyProductAdapter.LogoPromoCouponCallbackListener(new Callback<PromoCoupon>() {
            @Override
            public void success(PromoCoupon promoCoupon) {
                Company company = (Company) promoCoupon.getCompany();
                if (company != null) {
                    startActivity(Common.getInstance().createIntentForLocationByCompany
                            (GeofenceNotificationActivity.this, company));
                }
            }

            @Override
            public void error(String error) {

            }
        });

        companyProductAdapter.BtnCouponCallbackListener(new Callback<PromoCoupon>() {
            @Override
            public void success(PromoCoupon promoCoupon) {
                if (Common.isUserLoggedIn()) {
                    if (promoCoupon.getQuantity() > 0) {
                        passbook(promoCoupon);
                    } else {
                        Toast.makeText(GeofenceNotificationActivity.this, getResources()
                                .getString(R.string.couponQuantityError), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(GeofenceNotificationActivity.this, getResources().getString(R
                            .string.loggedInFacebookCoupon), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void error(String error) {

            }
        });

    }

    private void passbook(final PromoCoupon promoCoupon) {
        progressDialog.show();

        Common.getInstance().getUserProfile(new Callback<Profile>() {
            @Override
            public void success(Profile profile) {
                addCoupon(profile, promoCoupon);
            }

            @Override
            public void error(String error) {
                progressDialog.dismiss();
            }
        });

    }

    private void addCoupon(Profile profile, PromoCoupon coupon) {
        JSONArray coupons = profile.getCoupons();
        if (coupons == null) {
            coupons = new JSONArray();
        }
        int len = coupons.length();
        boolean couponAllReadyExist = false;
        final String couponId = coupon.getObjectId();

        for (int i = 0; i < len; i++) {
            try {
                JSONObject obj = (JSONObject) coupons.get(i);
                String objectId = obj.getString("objectId");
                if (objectId.contentEquals(couponId)) {
                    couponAllReadyExist = true;
                    break;
                }
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }

        if (!couponAllReadyExist) {
            JSONObject obj = new JSONObject();
            try {

                obj.put("objectId", couponId);
                obj.put("redeemed", false);
                obj.put("valid_until", validUntil.toString());
                coupons.put(obj);

                profile.put("coupons", coupons);
                profile.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e2) {
                        if (e2 == null) {
                            decrementQuantityByCoupon(couponId);
                            couponAddedToPassbookDialog();
                        } else {
                            progressDialog.dismiss();
                        }
                    }
                });
            } catch (JSONException e1) {
                e1.printStackTrace();
                progressDialog.dismiss();
            }
        } else {
            progressDialog.dismiss();
            Toast.makeText(GeofenceNotificationActivity.this, getResources().getString(R.string
                    .couponAllReadyExistToast), Toast.LENGTH_SHORT).show();
        }
    }


    private void updateCouponItems(String couponId) {
        List<RecycleViewRowVariant> couponsList = companyProductAdapter.getItems();
        if (couponsList.size() > 0) {
            for (int i = 0; i < couponsList.size(); i++) {
                PromoCoupon promoCoupon = (PromoCoupon) couponsList.get(i);
                if (promoCoupon.getObjectId().contentEquals(couponId)) {
                    int qty = promoCoupon.getQuantity() - 1;
                    promoCoupon.setQuantity(qty);
                    break;
                }
            }
            companyProductAdapter.notifyDataSetChanged();
        }

        progressDialog.dismiss();
    }

    private void couponAddedToPassbookDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.couponAddedToPassbookToast));
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog select_general_dialog;
        select_general_dialog = builder.create();
        select_general_dialog.show();
    }


    private void decrementQuantityByCoupon(final String couponId) {
        ParseQuery<PromoCoupon> couponParseQuery = ParseQuery.getQuery("PromoCoupon");
        couponParseQuery.getInBackground(couponId, new GetCallback<PromoCoupon>() {
            public void done(PromoCoupon coupon, ParseException e) {
                if (e == null) {
                    coupon.increment("quantity", -1);
                    coupon.saveInBackground();
                    updateCouponItems(couponId);
                } else {
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void loginWithFacebook() {
        progressDialog.show();

        final ArrayList<String> permissions = new ArrayList<>();
        permissions.add("email");
        ParseFacebookUtils.logInWithReadPermissionsInBackground(GeofenceNotificationActivity
                .this, permissions, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException err) {
                if (user == null) {
                    progressDialog.dismiss();
                    //Log.d(TAG, "Uh oh. The user cancelled the Facebook login.");
                } else if (user.isNew()) {
                    //Log.d(TAG, "User signed up and logged in through Facebook!");
                    setNewUser(user);
                } else {
                    //Log.d(TAG, "User logged in through Facebook!");
                    createProfileIfNotExist();
                }
            }
        });
    }

    private void setNewUser(final ParseUser user) {
        //set email from fb
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new
                GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    String email = object.getString("email");
                    if (!email.isEmpty()) {
                        if (user != null) {
                            user.setEmail(email);
                            user.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    createProfileIfNotExist();
                                }
                            });
                        }
                    }
                } catch (JSONException e) {
                }

            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "email");
        request.setParameters(parameters);
        request.executeAsync();

        progressDialog.dismiss();
    }

    private void createProfileIfNotExist() {
        Common.getInstance().getUserProfile(new Callback<Profile>() {
            @Override
            public void success(Profile profile) {
                //PROFILE EXIST FOR ENDED DON'T CREATE A NEW PROFILE
                adapterWasChange();
            }

            @Override
            public void error(String error) {
                //PROFILE NOT EXIST FOR ENDED CREATE A NEW PROFILE
                ParseObject profile = new Profile();
                profile.put("user", ParseUser.getCurrentUser());
                profile.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        adapterWasChange();
                    }
                });
            }
        });

    }

    private void adapterWasChange() {
        if (userNotLoggedInWrapper != null && Common.isUserLoggedIn()) {
            userNotLoggedInWrapper.setVisibility(View.GONE);
        }
        companyProductAdapter.notifyDataSetChanged();
        progressDialog.dismiss();
    }

}
