package net.clevermobileapps.ensivar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;

public class LaunchActivity extends AppCompatActivity {
    private Tracker mTracker;
    public static final String TAG = "LaunchActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EnsivarApplication application = (EnsivarApplication) getApplication();
        mTracker = application.getDefaultTracker();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String lastAppVersion = preferences.getString(Constants.LAST_APP_VERSION, "");

        String version;
        PackageManager manager = getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
            version = info.versionName;
        } catch(PackageManager.NameNotFoundException e) {
            version = "";
        }

        //Log.d(TAG, "enter launch activity");

        Intent intent;
        if (lastAppVersion.isEmpty() || !lastAppVersion.equals(version)) {
            intent = new Intent(this, TourActivity.class);
        } else {
            boolean isRemote = Common.getInstance().checkIfNeedFromRemote(Constants.COMPANY_STATUS_DOWNLOAD_CHECK, this);
            //this is only for testing, comment for release
            //isRemote = true;
            if (isRemote) {
                intent = new Intent(this, LoadingActivity.class);
            } else {
                intent = new Intent(this, DirectoryActivity.class);
            }
        }
        //this is only for testing, comment for release
        //intent = new Intent(this, TourActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.LAUNCH_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}
