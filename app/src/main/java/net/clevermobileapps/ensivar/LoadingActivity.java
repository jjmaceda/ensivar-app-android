package net.clevermobileapps.ensivar;

import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v7.app.AppCompatActivity;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;
import net.clevermobileapps.ensivar.utils.DirectorySuggestionsProvider;

import java.util.List;

public class LoadingActivity extends AppCompatActivity {

    private static final String TAG = "LoadingActivity";
    private int pages = 0;
    private int loopCounter = 0;
    private SQLiteDatabase mDatabase;

    private static class SuggestionColumns implements BaseColumns {
        public static final String DISPLAY1 = "display1";
        public static final String QUERY = "query";
        public static final String DATE = "date";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        //init the suggestions DB
        DirectorySuggestionsProvider.DatabaseHelper databaseHelper = new DirectorySuggestionsProvider.DatabaseHelper(
                this, DirectorySuggestionsProvider.DATABASE_VERSION);
        mDatabase = databaseHelper.getWritableDatabase();

        //start downloading companies
        getTotalCompanies();
    }

    private void getTotalCompanies() {
        ParseQuery<ParseObject> count = ParseQuery.getQuery("Counters");
        count.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    pages = (int) Math.ceil(object.getDouble("total_companies") / 1000.0);
                    loopCounter = 0;
                    getCompaniesInBatch();
                    //Log.d(TAG, String.valueOf(pages));
                }
            }
        });
    }

    private void getCompaniesInBatch() {
        if (loopCounter < pages) {
            ParseQuery<Company> query = ParseQuery.getQuery("Company");
            query.addAscendingOrder("name");
            query.setLimit(1000);
            query.whereEqualTo("status", 1);
            query.setSkip(loopCounter * 1000);
            query.findInBackground(new FindCallback<Company>() {
                @Override
                public void done(final List<Company> objects, ParseException e) {
                    if (e == null) {
                        if (loopCounter == 0) {
                            saveLocally(objects, true);
                        } else {
                            saveLocally(objects, false);
                        }
                        loopCounter++;
                        getCompaniesInBatch();
                    } else {
                        //Log.d(TAG, "interrupted");
                    }
                }
            });
        } else {
            Common.getInstance().updateLastTimeDowloaded(Constants.COMPANY_STATUS_DOWNLOAD_CHECK, this);

            //mDatabase.close();

            Intent intent = new Intent(this, DirectoryActivity.class);
            startActivity(intent);
            finish();
            //Log.d(TAG, "done with loading");
        }
    }

    private void saveLocally(final List<Company> objects, boolean deleteFirst) {
        if (deleteFirst) {
//            ParseObject.unpinAllInBackground("directory");
//            ParseObject.unpinAllInBackground("tags");
//            ParseObject.unpinAllInBackground("favorite");
            //suggestions
            try {
                mDatabase.execSQL("DELETE FROM " + DirectorySuggestionsProvider.sSuggestions);
            } catch (SQLException e) {}
            loadDirectory(objects);
        } else {
//            ParseObject.pinAllInBackground("directory", objects);
            //suggestions
            loadDirectory(objects);
        }
    }

    public void loadDirectory(final List<Company> objects) {
        new Thread(new Runnable() {
            public void run() {
                loadCompanies(objects);
            }
        }).start();
    }

    private void loadCompanies(List<Company> objects) {
        for (Company company: objects) {
            addCompany(company.getName());
        }
    }

    private long addCompany(String company) {
        long now = System.currentTimeMillis();
        ContentValues initialValues = new ContentValues();
        initialValues.put(SuggestionColumns.DISPLAY1, company);
        initialValues.put(SuggestionColumns.QUERY, company);
        initialValues.put(SuggestionColumns.DATE, now);

        return mDatabase.insert(DirectorySuggestionsProvider.sSuggestions, null, initialValues);
    }


}
