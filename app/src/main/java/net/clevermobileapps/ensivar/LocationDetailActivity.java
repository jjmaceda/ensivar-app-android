package net.clevermobileapps.ensivar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import net.clevermobileapps.ensivar.models.Favorite;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class LocationDetailActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String address;
    private String phone;
    private String name;
    private LatLng geo;
    private Long date_verify;
    private Integer verified;
    private String id_location;
    private String company_name;
    LatLngBounds.Builder locationBoundBuilder;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_location_detail);

        EnsivarApplication application = (EnsivarApplication) getApplication();
        mTracker = application.getDefaultTracker();

        final Intent intent = getIntent();
        address = intent.getStringExtra("address");
        phone = intent.getStringExtra("phone");
        name = intent.getStringExtra("name");
        geo = intent.getParcelableExtra("geo");
        date_verify = intent.getLongExtra("date_verify", -1);
        verified = intent.getIntExtra("verified", 0);
        id_location = intent.getStringExtra("id");
        company_name = intent.getStringExtra("company_name");

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle(name);
        }

        TextView phone_tv = (TextView) findViewById(R.id.phone);
        TextView address_tv = (TextView) findViewById(R.id.address);
        TextView verified_text_tv = (TextView) findViewById(R.id.verified_text);
        LinearLayout address_wrapper = (LinearLayout) findViewById(R.id.address_wrapper);
        RelativeLayout phone_wrapper = (RelativeLayout) findViewById(R.id.phone_wrapper);
        LinearLayout verified_wrapper = (LinearLayout) findViewById(R.id.verified);

        AppCompatButton directions_btn = (AppCompatButton) findViewById(R.id.directions_btn);
        Common.getInstance()
                .setButtonTint(
                        directions_btn,
                        ContextCompat.getColorStateList(this, R.color.default_btn_color_selector));

        ImageButton call_btn = (ImageButton) findViewById(R.id.call);
        Common.getInstance()
                .setImageButtonTint(
                        call_btn,
                        ContextCompat.getColorStateList(this, R.color.default_btn_color_selector));

        AppCompatButton share_btn = (AppCompatButton) findViewById(R.id.share_btn);
        Common.getInstance()
                .setButtonTint(
                        share_btn,
                        ContextCompat.getColorStateList(this, R.color.default_btn_color_selector));

        SwitchCompat fav_switch = (SwitchCompat) findViewById(R.id.fav_switch);
        if (checkIfIsFav()) {
            if (fav_switch != null) {
                fav_switch.setChecked(true);
            }
        }

        if (fav_switch != null) {
            fav_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        addFavorites();
                    } else {
                        removeFavorite();
                    }
                }
            });
        }

        if(directions_btn != null) {
            directions_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    doDirections(view);
                }
            });
        }

        if(share_btn != null) {
            share_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shareLocation(view);
                }
            });
        }

        if(call_btn != null) {
            call_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callPhoneNumber(view);
                }
            });
        }

        if (phone.isEmpty()) {
            if (phone_wrapper != null) {
                phone_wrapper.setVisibility(View.GONE);
            }
        } else {
            if (phone_tv != null) {
                phone_tv.setText(String.format("Tel: %s", phone));
            }
        }

        if (address.isEmpty()) {
            if (address_wrapper != null) {
                address_wrapper.setVisibility(View.GONE);
            }
        } else {
            if (address_tv != null) {
                address_tv.setText(address);
            }
        }

        if (verified == 1 && date_verify > 0) {
            Date d = new Date();
            d.setTime(date_verify);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            if (verified_wrapper != null) {
                verified_wrapper.setVisibility(View.VISIBLE);
            }
            if (verified_text_tv != null) {
                verified_text_tv.setText(String.format("%s: %s", getString(R.string.verified_prefix), sdf.format(d)));
            }
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    private void setMapCenter() {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(geo, 14));
    }

    private void setMapBounds() {
        Point dimensions = Common.getInstance().getDimensions(LocationDetailActivity.this);

        final LatLngBounds latLngBounds = locationBoundBuilder.build();
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
                latLngBounds, dimensions.x, dimensions.y,
                Common.getInstance().getSpacingForDensity(LocationDetailActivity.this, 40)));
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.LOCATION_DETAIL_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private boolean checkIfIsFav() {
        if (Common.isUserLoggedIn()) {
            ParseQuery<Favorite> query = ParseQuery.getQuery("Favorite");
            query.whereEqualTo("user", ParseUser.getCurrentUser());
            query.whereEqualTo("id_location", id_location);
            try{
                return query.count()>0;
            }catch(ParseException e){
                return false;
            }
        } else {
            SharedPreferences preferences = getSharedPreferences(
                    Constants.FAVORITES_PREFERENCE, MODE_PRIVATE);
            if (!preferences.getString(id_location, "").isEmpty()) {
                return true;
            }
        }

        return false;
    }

    private void removeFavorite() {
        final String full_name = String.format("%s: %s", company_name, name);

        if (Common.isUserLoggedIn()) {
            ParseQuery<Favorite> query = ParseQuery.getQuery("Favorite");
            query.whereEqualTo("user", ParseUser.getCurrentUser());
            query.whereEqualTo("id_location", id_location);
            query.getFirstInBackground(new GetCallback<Favorite>() {
                @Override
                public void done(final Favorite favorite, ParseException e) {
                    if (favorite != null) {
                        favorite.deleteInBackground();
                    }
                }
            });
        } else {
            SharedPreferences preferences = getSharedPreferences(
                    Constants.FAVORITES_PREFERENCE, MODE_PRIVATE);
            if (!preferences.getString(id_location, "").isEmpty()) {
                preferences.edit().remove(id_location).apply();
            }
        }

        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Remove Fav")
                .setLabel(full_name)
                .build());
    }

    public void addFavorites() {
        final String fav_name = String.format("%s: %s", company_name, name);

        if (Common.isUserLoggedIn()) {
            Favorite favorite = new Favorite();
            favorite.setIdLocation(id_location);
            favorite.setName(fav_name);
            favorite.setUser(ParseUser.getCurrentUser());
            favorite.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {

                }
            });
        } else {
            SharedPreferences preferences = getSharedPreferences(
                    Constants.FAVORITES_PREFERENCE, MODE_PRIVATE);
            if (preferences.getString(id_location, "").isEmpty()) {
                preferences.edit().putString(id_location, fav_name).apply();
            }
        }

        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Add Favorite")
                .setLabel(fav_name)
                .build());
    }

    public void doDirections(View v) {
        final String full_name = String.format("%s: %s", company_name, name);
        final Uri uri = Uri.parse("geo:" + geo.latitude + "," +
                geo.longitude + "?q=" +
                geo.latitude + "," + geo.longitude);
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(uri);

        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Get Directions")
                .setLabel(full_name)
                .build());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            //TODO alert there is no map app
            Toast.makeText(this, getString(R.string.no_maps), Toast.LENGTH_LONG)
                    .show();
        }
    }

    public void shareLocation(View v) {
        final String full_name = String.format("%s: %s", company_name, name);
        String uri = "http://maps.google.com/maps?q=" + geo.latitude + "," + geo.longitude;
//        String uri = "waze://?ll=" + geo.latitude + "," + geo.longitude;

        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share Location")
                .setLabel(full_name)
                .build());

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        sharingIntent.setType("text/plain");
        String ShareSub = getString(R.string.share_subject);
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, ShareSub);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, uri);
        startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_chooser_title)));
    }

    public void callPhoneNumber(View v) {
        if (!phone.isEmpty()) {
            final String full_name = String.format("%s: %s", company_name, name);

            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + phone));
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(callIntent);

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("Call Phone")
                    .setLabel(full_name)
                    .build());
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);

        UiSettings settings = googleMap.getUiSettings();
        settings.setAllGesturesEnabled(true);
        settings.setCompassEnabled(true);
        settings.setMyLocationButtonEnabled(true);
        settings.setZoomControlsEnabled(true);

        if (geo != null && mMap != null) {
            locationBoundBuilder = new LatLngBounds.Builder();

            mMap.addMarker(new MarkerOptions()
                    .position(geo)
                    .title(String.format("%s : %s", name, phone)));
            locationBoundBuilder.include(geo);
            setMapCenter();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private LatLngBounds createBoundsWithMinDiagonal(LatLng locationLatLng, LatLng myLocationLatLng) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(locationLatLng);
        builder.include(myLocationLatLng);

        LatLngBounds tmpBounds = builder.build();
        /** Add 2 points 1000m northEast and southWest of the center.
         * They increase the bounds only, if they are not already larger
         * than this.
         * 1000m on the diagonal translates into about 709m to each direction. */
        LatLng center = tmpBounds.getCenter();
        LatLng northEast = move(center, 709, 709);
        LatLng southWest = move(center, -709, -709);
        builder.include(southWest);
        builder.include(northEast);
        return builder.build();
    }

    private static final double EARTHRADIUS = 6366198;
    /**
     * Create a new LatLng which lies toNorth meters north and toEast meters
     * east of startLL
     */
    private static LatLng move(LatLng startLL, double toNorth, double toEast) {
        double lonDiff = meterToLongitude(toEast, startLL.latitude);
        double latDiff = meterToLatitude(toNorth);
        return new LatLng(startLL.latitude + latDiff, startLL.longitude
                + lonDiff);
    }

    private static double meterToLongitude(double meterToEast, double latitude) {
        double latArc = Math.toRadians(latitude);
        double radius = Math.cos(latArc) * EARTHRADIUS;
        double rad = meterToEast / radius;
        return Math.toDegrees(rad);
    }

    private static double meterToLatitude(double meterToNorth) {
        double rad = meterToNorth / EARTHRADIUS;
        return Math.toDegrees(rad);
    }
}
