package net.clevermobileapps.ensivar;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import net.clevermobileapps.ensivar.fragments.LocationByCompanyFragment;
import net.clevermobileapps.ensivar.fragments.ProductOverviewFragment;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import info.hoang8f.android.segmented.SegmentedGroup;

public class LocationsByCompanyActivity extends AppCompatActivity {

    private String id_company;
    private String company_name;
    private String web;
    private String email;
    private String logo;
    private String main_phone;
    private ArrayList<String> predominantColor;
    private String categoryId;
    private boolean isSponsor;
    private ArrayList<Parcelable> socialList;
    private static final String TAG = "LocationsByCompany";

    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_locations_by_company);

        final Intent intent = getIntent();
        logo = getStringExtra("cover");
        company_name = getStringExtra("company_name");
        id_company = getStringExtra("id_company");
        main_phone = getStringExtra("main_phone");
        categoryId = getStringExtra("categoryId");
        predominantColor = intent.getStringArrayListExtra("predominant_color");
        web = getStringExtra("web");
        email = getStringExtra("email");
        isSponsor =  intent.getBooleanExtra("sponsor", false);

        socialList = intent.getParcelableArrayListExtra("social");

        EnsivarApplication application = (EnsivarApplication) getApplication();
        mTracker = application.getDefaultTracker();

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if(predominantColor == null || predominantColor.size() == 0){
                actionBar.setTitle(company_name);
            }

            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        final SegmentedGroup segmentedGroup = (SegmentedGroup) findViewById(R.id.locationSections);
        if (segmentedGroup != null) {

            if(predominantColor != null && predominantColor.size() > 0){
                segmentedGroup.setTintColor(Color.parseColor(
                        predominantColor.get(0)), Color.parseColor("#FFFFFFFF"));
            }

            if(isSponsor){
                segmentedGroup.check(R.id.optionProductBtn);
            }else{
                segmentedGroup.check(R.id.optionLocationBtn);
            }

            segmentedGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    int radioButtonID = segmentedGroup.getCheckedRadioButtonId();
                    View radioButton = segmentedGroup.findViewById(radioButtonID);
                    int idx = segmentedGroup.indexOfChild(radioButton);
                    if (idx == 0) {
                        createLocationFragment();
                    } else {
                        createProductOverviewFragment();
                    }
                }
            });
        }

        if(isSponsor){
            createProductOverviewFragment();
        }else{
            createLocationFragment();
        }
        createGeneral();
    }

    private void createLocationFragment() {
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.main_fragment_container);
        if (frameLayout != null) {
            final Bundle bundle = new Bundle();
            bundle.putString("main_phone", main_phone);
            bundle.putString("id_company", id_company);
            bundle.putString("company_name", company_name);
            bundle.putString("categoryId", categoryId);
            bundle.putBoolean("sponsor", isSponsor);
            bundle.putStringArrayList("predominant_color", predominantColor);

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            Fragment fragment = LocationByCompanyFragment.newInstance(bundle);
            fragmentTransaction.replace(frameLayout.getId(), fragment, "locationFrag");
            fragmentTransaction.commit();
        }
    }

    private void createProductOverviewFragment() {
        final Intent intent = getIntent();
        intent.getSerializableExtra("social");

        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.main_fragment_container);
        if (frameLayout != null) {
             Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("social", socialList);
            bundle.putString("web", web);
            bundle.putString("email", email);
            bundle.putString("id_company", id_company);
            bundle.putBoolean("sponsor", isSponsor);
            bundle.putString("company_name", company_name);

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            Fragment fragment = ProductOverviewFragment.newInstance(bundle);
            fragmentTransaction.replace(frameLayout.getId(), fragment, "productFrag");
            fragmentTransaction.commit();
        }
    }

    private void createGeneral() {
        int color = 0;
        if (predominantColor != null && predominantColor.size() != 0) {
            color = Color.parseColor(predominantColor.get(0));
        }

        if (isSponsor && !logo.isEmpty() && color != 0) {
            final CircleImageView logoView = (CircleImageView) findViewById(R.id.logo);

            if (logoView != null) {
                logoView.setVisibility(View.VISIBLE);
                logoView.setBorderColor(color);

                String url = Common.getInstance()
                        .getAssetUrl(this, id_company+"/logo_page/__150_225_300_450__/logo.jpg", 150);
                Picasso.with(this).load(url).into(logoView);
            }

            View foreground_logo = findViewById(R.id.foreground_logo);
            if (foreground_logo != null) {
                foreground_logo.setBackgroundColor(color);
            }

            final CollapsingToolbarLayout collapsingToolbarLayout =
                    (CollapsingToolbarLayout) this.findViewById(R.id.collapsing_toolbar);

            if(collapsingToolbarLayout != null) {
                collapsingToolbarLayout.setContentScrimColor(color);

                AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);
                appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                    boolean isShow = false;
                    int scrollRange = -1;

                    @Override
                    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                        if (scrollRange == -1) {
                            scrollRange = appBarLayout.getTotalScrollRange();
                        }
                        if (scrollRange + verticalOffset == 0) {
                            collapsingToolbarLayout.setTitle(company_name);
                            isShow = true;
                        } else if(isShow) {
                            collapsingToolbarLayout.setTitle("");
                            isShow = false;
                        }
                    }
                });
            }
        } else {
            final ImageView default_cover = (ImageView) findViewById(R.id.default_cover);
            if (default_cover != null) {
                default_cover.setVisibility(View.VISIBLE);
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if(predominantColor != null && predominantColor.size() > 1){
                changeStatusBarColor(predominantColor.get(1));
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void changeStatusBarColor(String color) {
        //only loolipop
        final Window window = this.getWindow();
        window.setStatusBarColor(Color.parseColor(color));
    }


    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.LOCATION_BY_COMPANY_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private String getStringExtra(String extra) {
        final Intent intent = getIntent();
        return intent.getStringExtra(extra) != null ? intent.getStringExtra(extra) : "";
    }

}
