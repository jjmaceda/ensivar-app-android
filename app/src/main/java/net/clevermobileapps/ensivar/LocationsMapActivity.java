package net.clevermobileapps.ensivar;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;

public class LocationsMapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    public static final String TAG = "LocationsMapActivity";
    LatLngBounds.Builder locationBoundBuilder;

    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations_map);

        EnsivarApplication application = (EnsivarApplication) getApplication();
        mTracker = application.getDefaultTracker();

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void setMapCenter(LatLng latLng) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));
    }

    private void setMapBounds() {
        //TODO check a better solution
        try {
            Point dimensions = Common.getInstance().getDimensions(LocationsMapActivity.this);

            final LatLngBounds latLngBounds = locationBoundBuilder.build();
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, dimensions.x, dimensions.y, Common.getInstance().getSpacingForDensity(LocationsMapActivity.this, (int) (dimensions.x * 0.05))));
        }catch(IllegalStateException e){
            //nothing
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.LOCATION_MAP_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);

        UiSettings settings = googleMap.getUiSettings();
        settings.setAllGesturesEnabled(true);
        settings.setCompassEnabled(true);
        settings.setMyLocationButtonEnabled(true);
        settings.setZoomControlsEnabled(true);

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                final ArrayList<HashMap<String, String>> locations = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("locations");

                if (locations != null && mMap != null) {
                    locationBoundBuilder = new LatLngBounds.Builder();

                    for (HashMap<String, String> location : locations) {
                        final LatLng latLng = new LatLng(Double.parseDouble(location.get("lat")), Double.parseDouble(location.get("lng")));
                        locationBoundBuilder.include(latLng);
                        mMap.addMarker(new MarkerOptions().position(latLng).title(location.get("name") + ": " + location.get("phone")));
                    }

                    if (locations.size() == 1) {
                        final HashMap<String, String> location = locations.get(0);
                        final LatLng latLng = new LatLng(Double.parseDouble(location.get("lat")), Double.parseDouble(location.get("lng")));
                        setMapCenter(latLng);
                    } else {
                        setMapBounds();
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_empty, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
