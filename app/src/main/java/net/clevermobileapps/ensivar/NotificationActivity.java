package net.clevermobileapps.ensivar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import net.clevermobileapps.ensivar.adapters.NotificationAdapter;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.interfaces.RecycleViewRowVariant;
import net.clevermobileapps.ensivar.models.AlertMessage;
import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.models.Profile;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.DividerItemDecoration;
import net.clevermobileapps.ensivar.utils.RecyclerViewEmptySupport;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity {
    private ArrayList<RecycleViewRowVariant> mList = new ArrayList<>();
    private ProgressBar pb;
    private RecyclerViewEmptySupport mListView;
    private NotificationAdapter mNotificationAdapter;
    private ParseQuery<Company> companyParseQuery;
    private List<String> blackList = new ArrayList<>();
    public static String NOTIFICATION_SETTINGS_FILE_NAME = "notification_settings";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(toolbar != null){
            setSupportActionBar(toolbar);

            final ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setHomeButtonEnabled(true);
            }
        }

        pb = (ProgressBar) findViewById(R.id.pb);
        pb.setVisibility(View.VISIBLE);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mListView = (RecyclerViewEmptySupport) findViewById(R.id.companyList);
        if (mListView != null) {
            mListView.setLayoutManager(mLayoutManager);
            mListView.addItemDecoration(new DividerItemDecoration(NotificationActivity.this));
            mListView.setEmptyView(findViewById(R.id.no_result));
        }

        loadBlackList();
    }

    private void loadBlackList(){
        if(Common.getInstance().isInternetAvailable(this)){
            Common.getInstance().getUserProfile(new Callback<Profile>() {
                @Override
                public void success(Profile profile) {
                    blackList = profile.getBlackList();
                    loadSponsorCompanies();
                }

                @Override
                public void error(String error) {
                    loadSponsorCompanies();
                }
            });
        }else{
            loadSponsorCompanies();
        }
    }

    private void loadSponsorCompanies(){
        companyParseQuery = ParseQuery.getQuery("Company");
        companyParseQuery.whereEqualTo("sponsor", true);
        companyParseQuery.findInBackground(new FindCallback<Company>() {
            @Override
            public void done(List<Company> companyList, ParseException e) {
                if(e == null){
                    if(companyList.size() > 0){

                        if(!Common.isUserLoggedIn()){
                            mList.add(new AlertMessage());
                        }

                        for(Company company:companyList){
                            mList.add(company);
                        }

                        mNotificationAdapter = new NotificationAdapter(mList, NotificationActivity.this, blackList);
                        if (mListView != null) {
                            mListView.setAdapter(mNotificationAdapter);
                        }

                        setAdapterCallbacks();
                    }else{
                        mListView.setAdapter(null);
                    }
                }else{
                    mListView.setAdapter(null);
                }

                pb.setVisibility(View.GONE);
            }
        });
    }
    private void setAdapterCallbacks(){
        mNotificationAdapter.NotificationStatusCallbackListener(new Callback<Bundle>() {
            @Override
            public void success(Bundle args) {
                saveNotificationStatus(args);
            }

            @Override
            public void error(String error) {

            }
        });
        mNotificationAdapter.goToFacebookCallbackListener(new Callback<Bundle>() {
            @Override
            public void success(Bundle bundle) {
                goToProfile();
            }

            @Override
            public void error(String error) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void saveNotificationStatus(Bundle args){
        pb.setVisibility(View.VISIBLE);

        final String companyId = args.getString("objectId", "");
        final boolean checked = args.getBoolean("checked", false);

        Common.getInstance().getUserProfile(new Callback<Profile>() {
            @Override
            public void success(Profile profile) {
                List<String> blackList = profile.getBlackList();
                if(blackList == null){
                    blackList = new ArrayList<>();
                }

                int len = blackList.size();
                boolean companyExistInBlackList = false;
                int companyIndex = 0;
                for(int i = 0; i < len; i++){
                    String companyIdFromProfile = blackList.get(i);
                    if(companyIdFromProfile.contentEquals(companyId)){
                        companyExistInBlackList = true;
                        companyIndex = i;
                        break;
                    }
                }

                if(companyExistInBlackList){
                    blackList.remove(companyIndex);
                }else{
                    blackList.add(companyId);
                }

                profile.put("black_list", blackList);
                profile.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e == null){
                            saveOnSharePreference(companyId, checked);
                        }
                        pb.setVisibility(View.GONE);
                    }
                });
            }

            @Override
            public void error(String error) {
                pb.setVisibility(View.GONE);
            }
        });

    }

    private void saveOnSharePreference(String companyId, boolean checked){
        SharedPreferences preferences = getSharedPreferences(NOTIFICATION_SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        if(!preferences.contains(companyId)){
            editor.putBoolean(companyId, checked);
        }else{
            editor.remove(companyId);
        }

        editor.apply();
    }


    private void goToProfile() {
        Intent intent = new Intent(NotificationActivity.this, DirectoryActivity.class);
        intent.putExtra("viewPagerPosition", 1);
        startActivity(intent);
    }
}
