package net.clevermobileapps.ensivar;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

import net.clevermobileapps.ensivar.models.Profile;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;
import net.clevermobileapps.ensivar.widgets.LogoImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PointsStepsActivity extends AppCompatActivity {
    private static final String TAG = "PointsStepsActivity";

    private String imageName;
    private String captured_image;

    private ProgressDialog progressDialog;
    private ImageView checkStep1;
    private ImageView checkStep2;

    private Profile mProfile;

    private boolean step1Complete = false;
    private boolean step2Complete = false;

    private String id_company;
    private String company_name;
    private JSONArray pointsArray = new JSONArray();
    private int companyPoints = 0;

    private int accumulatePoints = 0;
    private String id_location;
    private boolean isReceiptTake = false;
    private boolean isBarcodeTake = false;
    private TextView company_points_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_points_steps);

        Intent intent = getIntent();
        id_company = intent.getStringExtra("id_company");
        company_name = intent.getStringExtra("company_name");

        progressDialog = Common.getInstance().getProgressDialog(this);
        checkStep1 = (ImageView) findViewById(R.id.checkStep1);
        checkStep2 = (ImageView) findViewById(R.id.checkStep2);

        Button capturarQRBtn = (Button) findViewById(R.id.capturaQRBtn);
        if (capturarQRBtn != null) {
            capturarQRBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(PointsStepsActivity.this,
                            BarcodeCaptureActivity.class);
                    startActivityForResult(intent, Constants.RC_BARCODE_CAPTURE);
                }
            });
        }

        Button capturarReciboBtn = (Button) findViewById(R.id.capturaReciboBtn);
        if (capturarReciboBtn != null) {
            capturarReciboBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    imageName = System.currentTimeMillis() + ".jpg";
                    File file = new File(Environment.getExternalStorageDirectory(), imageName);
                    captured_image = file.getAbsolutePath();
                    Uri outputFileUri = Uri.fromFile(file);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, Constants.RES_IMAGE_CAPTURE);
                }
            });
        }

        Button finishBtn = (Button) findViewById(R.id.finishBtn);
        if(finishBtn != null){
            finishBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isReceiptTake && isBarcodeTake){
                        progressDialog.show();
                        uploadFile("http://assets.ensivarapp.com/?r=resources/proccess-image/",
                                new File(captured_image));
                    }else{
                        Toast.makeText(PointsStepsActivity.this,
                                getResources().getString(R.string.finish_steps_error),
                                Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

        LinearLayout actionBtns = (LinearLayout) findViewById(R.id.actionBtns);
        if(actionBtns!=null){
            actionBtns.setVisibility(View.GONE);
        }

        LogoImageView company_logo_iv = (LogoImageView) findViewById(R.id.logo);
        if(company_logo_iv!=null){
            String logoUrl = Common.getInstance().getAssetUrl(
                    PointsStepsActivity.this,
                    id_company+"/logo/__120-180-240-360__/logo.png",
                    120);
            Picasso.with(PointsStepsActivity.this).
                    load(logoUrl).placeholder(R.drawable.empty_360).into(company_logo_iv);
        }

        TextView company_name_tv = (TextView) findViewById(R.id.name);
        if(company_name_tv!=null){
            company_name_tv.setText(company_name);
        }

        company_points_tv = (TextView) findViewById(R.id.points);
        if(company_points_tv !=null){
            company_points_tv.setText(String.valueOf(companyPoints));
        }

        loadPoints();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.RES_IMAGE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS_CACHE) {
                //compress image
                new CompressImageTask().execute(new File(captured_image));

                isReceiptTake = true;
                checkStep1.setVisibility(View.VISIBLE);
            }else{
                isReceiptTake = false;
                checkStep1.setVisibility(View.INVISIBLE);
            }
        } else if (requestCode == Constants.RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    if (!barcode.displayValue.isEmpty()) {
                        verifyLocation(barcode.displayValue);
                    }
                } else {
                    Log.d(TAG, "No barcode captured, intent data is null");
                }
            } else {
                Log.d(TAG, String.format(getString(R.string.barcode_error),
                        CommonStatusCodes.getStatusCodeString(resultCode)));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void loadPoints() {
        Common.getInstance().getUserProfile(new net.clevermobileapps.ensivar.interfaces.Callback<Profile>() {
            @Override
            public void success(Profile profile) {
                mProfile = profile;
                pointsArray = profile.getPoints();
                if (pointsArray != null) {
                    int len = pointsArray.length();
                    for (int i = 0; i < len; i++) {
                        try {
                            JSONObject obj = (JSONObject) pointsArray.get(i);
                            if (obj.getString("id_company").contentEquals(id_company)) {
                                companyPoints = obj.getInt("points");
                                break;
                            }
                        } catch (JSONException e1) {
                            throw new RuntimeException(e1);
                        }
                    }
                }

                if(company_points_tv !=null){
                    company_points_tv.setText(String.valueOf(companyPoints));
                }
            }

            @Override
            public void error(String error) {

            }
        });

    }

    private void verifyLocation(String barcodeValue) {
        try {
            JSONObject jsonObject = new JSONObject(barcodeValue);
            id_location = jsonObject.getString("id_location");
            accumulatePoints = jsonObject.getInt("points");

            Common.getInstance().verifyAuthenticityOfLocation(id_company, id_location, new net.clevermobileapps.ensivar.interfaces.Callback<Boolean>() {
                @Override
                public void success(Boolean aBoolean) {
                    isBarcodeTake = true;
                    checkStep2.setVisibility(View.VISIBLE);
                }

                @Override
                public void error(String error) {
                    isBarcodeTake = false;
                    progressDialog.dismiss();
                    checkStep2.setVisibility(View.INVISIBLE);
                    Toast.makeText(PointsStepsActivity.this,
                            getResources().getString(R.string.locationIsNotOfCompany),
                            Toast.LENGTH_LONG).show();
                }
            });


        } catch (JSONException e) {
            isBarcodeTake = false;
            progressDialog.dismiss();
            Toast.makeText(PointsStepsActivity.this,
                    getResources().getString(R.string.qrError),
                    Toast.LENGTH_LONG).show();
        }
    }

    private boolean uploadFile(String serverURL, File file) {
        try {
            MediaType MEDIA_TYPE = MediaType.parse("image/jpeg");

            RequestBody requestBody2 = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("objectId", id_company)
                    .addFormDataPart("type", "recibo")
                    .addFormDataPart("image_name", imageName)
                    .addFormDataPart("imageFile", file.getName(),
                            MultipartBody.create(MEDIA_TYPE, file))
                    .build();

            Request request = new Request.Builder()
                    .url(serverURL)
                    .post(requestBody2)
                    .build();

            OkHttpClient client = new OkHttpClient();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                    PointsStepsActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            showError();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {

                    PointsStepsActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(response.isSuccessful()){
                                if (checkStep1 != null) {
                                    step1Complete = true;
                                    accumulatePoints();
                                }
                            }else{
                                progressDialog.dismiss();
                                showError();
                            }

                        }
                    });

                }
            });

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            PointsStepsActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                    showError();
                }
            });
        }
        return false;
    }



    private void accumulatePoints() {
        boolean companyAllReadyExists = false;

        if (pointsArray != null) {
            int len = pointsArray.length();

            for (int i = 0; i < len; i++) {
                try {
                    JSONObject object = (JSONObject) pointsArray.get(i);
                    int totalPoints = object.getInt("points") + accumulatePoints;

                    if (object.getString("id_company").contentEquals(id_company)) {
                        object.put("id_company", id_company);
                        object.put("points", totalPoints);
                        companyPoints = totalPoints;
                        companyAllReadyExists = true;
                        break;
                    }

                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
        }

        if (!companyAllReadyExists) {
            try {
                JSONObject object = new JSONObject();
                object.put("id_company", id_company);
                object.put("points", accumulatePoints);
                pointsArray.put(object);
                companyPoints = accumulatePoints;
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }

        mProfile.put("points", pointsArray);
        mProfile.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    accumulateStats();

                    progressDialog.dismiss();
                    step2Complete = true;
                    company_points_tv.setText(String.valueOf(companyPoints));

                    stepsFinished();
                } else{
                    progressDialog.dismiss();
                    showError();
                }
            }
        });
    }

    private void showError(){
        Toast.makeText(PointsStepsActivity.this,
                getResources().getString(R.string.upload_image_error),
                Toast.LENGTH_LONG).show();
    }

    private void stepsFinished(){
        if(step1Complete && step2Complete){
            accumulateAlertDialog();
        }
    }

    private void accumulateStats(){
        ParseObject stat = new ParseObject("Stat");
        stat.put("company", ParseObject.createWithoutData("Company", id_company));
        stat.put("location", ParseObject.createWithoutData("Location", id_location));
        stat.put("user", ParseUser.getCurrentUser());
        stat.put("accumulate_points", accumulatePoints);
        stat.put("receipt", imageName);

        stat.saveInBackground(new SaveCallback() {
            @Override
            public void done(com.parse.ParseException e) {}
        });
    }

    private void accumulateAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        String alertMessage = getResources().
                getString(R.string.pointsAccumulateSuccessfully) + " " + company_name;
        builder.setMessage(alertMessage);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                step1Complete = false;
                step2Complete = false;
                Intent intent = new Intent(PointsStepsActivity.this, DirectoryActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("viewPagerPosition", 1);
                startActivity(intent);
            }
        });


        AlertDialog select_general_dialog;
        select_general_dialog = builder.create();
        select_general_dialog.show();
    }
}

class CompressImageTask extends AsyncTask<File, Void, Boolean> {

    private static final String TAG = "CompressImageTask";

    @Override
    protected Boolean doInBackground(File... file) {
        File f = file[0];
        String path = f.getAbsolutePath();

        Bitmap b = BitmapFactory.decodeFile(path);

        try {
            FileOutputStream out = new FileOutputStream(f);
            b.compress(Bitmap.CompressFormat.JPEG, 60, out);
            out.flush();
            out.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        Log.d(TAG, "onPostExecute");
    }
}
