package net.clevermobileapps.ensivar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.clevermobileapps.ensivar.adapters.CompanyProductAdapter;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.interfaces.RecycleViewRowVariant;
import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.models.PromoCoupon;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;
import net.clevermobileapps.ensivar.utils.RecyclerViewEmptySupport;

import java.util.ArrayList;
import java.util.List;

public class PromotionsByCompanyActivity extends AppCompatActivity {
    public static String TAG = "PromotionsByCompanyActivity";
    private RecyclerViewEmptySupport mListView;
    private ProgressBar pb;
    private String id_company;
    private ArrayList<RecycleViewRowVariant> mList = new ArrayList<>();
    private CompanyProductAdapter companyProductAdapter;

    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_promotions_by_company);

        EnsivarApplication application = (EnsivarApplication) getApplication();
        mTracker = application.getDefaultTracker();

        Intent intent = getIntent();
        id_company = intent.getStringExtra("id_company");

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getResources().getString(R.string.promotionsActivityTitle));
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        pb = (ProgressBar) findViewById(R.id.pb);
        if(pb != null){
            pb.setVisibility(View.VISIBLE);
        }

        mListView = (RecyclerViewEmptySupport) findViewById(R.id.promotionList);
        if(mListView != null){
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            mListView.setLayoutManager(mLayoutManager);
            mListView.setEmptyView(findViewById(R.id.no_result));
        }

        loadPromotions();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.PROMOTIONS_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadPromotions(){
        Common.getInstance().getPromotions(id_company, new Callback<List<PromoCoupon>>() {
            @Override
            public void success(List<PromoCoupon> promoCouponList) {
                for(PromoCoupon promoCoupon : promoCouponList){
                    mList.add(promoCoupon);
                }

                if(mList.size() > 0){
                    if(mListView != null){
                        companyProductAdapter = new CompanyProductAdapter(mList, PromotionsByCompanyActivity.this);
                        mListView.setAdapter(companyProductAdapter);
                    }
                    setAdapterCallbacks();
                }else{
                    mListView.setAdapter(null);
                }
                pb.setVisibility(View.GONE);
            }

            @Override
            public void error(String error) {
                mListView.setAdapter(null);
                pb.setVisibility(View.GONE);
            }
        });

    }


    private void setAdapterCallbacks(){
        companyProductAdapter.LogoPromoCouponCallbackListener(new Callback<PromoCoupon>() {
            @Override
            public void success(PromoCoupon promoCoupon) {
                Company company = (Company) promoCoupon.getCompany();
                if(company != null){
                    startActivity(Common.getInstance().createIntentForLocationByCompany(PromotionsByCompanyActivity.this, company));
                }
            }
            @Override
            public void error(String error) {

            }
        });
    }

}
