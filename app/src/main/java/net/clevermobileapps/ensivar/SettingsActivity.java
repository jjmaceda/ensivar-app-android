package net.clevermobileapps.ensivar;

import android.content.DialogInterface;
import android.content.SearchRecentSuggestionsProvider;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
        }

        TextView version_tv = (TextView) findViewById(R.id.version);
        String version;
        PackageManager manager = getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
            version = info.versionName;
        } catch(PackageManager.NameNotFoundException e) {
            version = "";
        }
        version_tv.setText(getString(R.string.version_prefix, version));
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_directory, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        return super.onOptionsItemSelected(item);
//    }

    public void clearSearch(View v) {
        createAlertDialog();
    }

    private void createAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(getString(R.string.confirm_delete_search));
        builder.setPositiveButton(R.string.general_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String recentProviderAuthority = "net.clevermobileapps.ensivar.utils.RecentSuggestionsProvider";
                SearchRecentSuggestions suggestions = new SearchRecentSuggestions(
                        SettingsActivity.this, recentProviderAuthority, SearchRecentSuggestionsProvider.DATABASE_MODE_QUERIES);
                suggestions.clearHistory();
            }
        });
        builder.setNegativeButton(R.string.general_cancel, null);

        AlertDialog select_general_dialog;
        select_general_dialog=builder.create();
        select_general_dialog.show();
        }
    }
