package net.clevermobileapps.ensivar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.pixplicity.sharp.Sharp;

import net.clevermobileapps.ensivar.utils.Common;

public class SweepstakeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sweepstake);

        AppCompatButton participate_btn = (AppCompatButton) findViewById(R.id.participate_btn);
        Common.getInstance().setButtonTint(participate_btn, ContextCompat.getColorStateList(this, R.color.sweepstake_btn_color_selector));
        participate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SweepstakeActivity.this, SweepstakeWebActivity.class);
                startActivity(intent);
                finish();
            }
        });

        Button later_btn = (Button) findViewById(R.id.later_btn);
        later_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SweepstakeActivity.this, LoadingActivity.class);
                startActivity(intent);
                finish();
            }
        });

        ImageView winner = (ImageView) findViewById(R.id.winner);
        Sharp.loadResource(getResources(), R.raw.winner_cup).into(winner);
        
    }

    @Override
    public void onBackPressed() {
    }
}
