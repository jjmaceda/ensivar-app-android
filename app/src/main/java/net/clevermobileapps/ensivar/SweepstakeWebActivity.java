package net.clevermobileapps.ensivar;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class SweepstakeWebActivity extends AppCompatActivity {

    private WebView wv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sweepstake_web);

        wv1 = (WebView)findViewById(R.id.webview);
        wv1.clearCache(true);
        wv1.setWebViewClient(new CustomWebViewClient());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(false);
        }

        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        //wv1.loadUrl("");
        wv1.loadUrl("http://directory.clevermobileapps.net/contest");
    }

    private class CustomWebViewClient extends WebViewClient {
        private static final String TAG = "CustomWebViewClient";

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            ProgressBar pg = (ProgressBar) findViewById(R.id.web_loader);
            pg.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            ProgressBar pg = (ProgressBar) findViewById(R.id.web_loader);
            pg.setVisibility(View.GONE);
        }
    }
}
