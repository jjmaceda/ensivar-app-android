package net.clevermobileapps.ensivar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.vlonjatg.android.apptourlibrary.AppTour;

import net.clevermobileapps.ensivar.fragments.AppTourMaterialSlideFragment;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;

public class TourActivity extends AppTour {
    private static final int NUM_PAGES = 8;
    private Tracker mTracker;
    SharedPreferences preferences;

    int[] images = {
            R.raw.tour_new,
            R.raw.tour2,
            R.raw.tour3,
            R.raw.tour1,
            R.raw.tourfav,
            R.raw.tour5,
            R.raw.tour6,
            R.raw.tour7
    };
    String[] colors = {
            "#0097A7",
            "#009866",
            "#ffab1a",
            "#058385",
            "#ffab1a",
            "#058385",
            "#009866",
            "#0097A7"
    };
    String[] titles = {
            "PROMOCIONES, CUPONES, CLIENTE FRECUENTE",
            "ENCUENTRA FACIL Y RAPIDO",
            "CREADO PARA SALVADOREÑOS",
            "ÚTIL CON O SIN INTERNET",
            "MANTEN TUS FAVORITOS A LA MANO",
            "SUCURSALES CERCA DE TI",
            "NO NECESITAS SABER EL NOMBRE DE LA EMPRESA",
            "INTEGRADO CON SERVICIOS DE MAPA"
    };
    String[] copies = {
            "Estamos trabajando con comercios afiliados para llevarte lo que quieres a tus manos",
            "Búsqueda global de empresas y comercios, agrupadas por etiquetas de interés",
            "El directorio es manejado por editores salvadoreños",
            "Puedes buscar información en el directorio sin tener conexión a internet",
            "Puedes mantener una lista personal de favoritos de sucursales",
            "Filtra tus resultados, solo los que están cerca",
            "Todas las empresas están agrupadas con distintas etiquetas, busca por necesidad",
            "Encuentra como llegar y la mejor ruta, integrado con Waze® y Google Maps®"
    };

    @Override
    public void init(Bundle savedInstanceState) {
        preferences = PreferenceManager.getDefaultSharedPreferences(TourActivity.this);

        EnsivarApplication application = (EnsivarApplication) getApplication();
        mTracker = application.getDefaultTracker();

        String version;
        PackageManager manager = getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
            version = info.versionName;
        } catch(PackageManager.NameNotFoundException e) {
            version = "";
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString("lastAppVersion", version);
        editor.apply();

        for(int i=0; i<NUM_PAGES; i++){
            int color = Color.parseColor(colors[i]);
            Fragment firstSlide = AppTourMaterialSlideFragment.newInstance(images[i], titles[i],
                    copies[i], Color.WHITE, Color.WHITE);

            //Add slides
            addSlide(firstSlide, color);
        }

        //Customize tour
        setSkipButtonTextColor(Color.WHITE);
        setNextButtonColorToWhite();
        setDoneButtonTextColor(Color.WHITE);
        setSkipText("Terminar");
        setDoneText("Hecho");
    }

    @Override
    public void onSkipPressed() {
        skipTour();
    }

    @Override
    public void onDonePressed() {
        skipTour();
    }

    public void skipTour(){
        Common.getInstance().showSweepstakeIfNeeded(this, new Callback<Boolean>() {
            @Override
            public void success(Boolean isActive) {
                if (isActive) {
                    //is active
                    //show sweepstake activity
                    Intent intent = new Intent(TourActivity.this, SweepstakeActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(TourActivity.this, LoadingActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void error(String error) {
                //TODO show a no connection error maybe?
                Intent intent = new Intent(TourActivity.this, LoadingActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.TOUR_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}
