
package net.clevermobileapps.ensivar.adapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.models.Catalog;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.widgets.CoverImageView;

import java.util.ArrayList;
import java.util.List;


public class CatalogAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Catalog> values;
    private Context mContext;
    private Callback<Catalog> cbCatalog;
    private Bundle args = new Bundle();
    private String btn_clicked;

    public CatalogAdapter(ArrayList<Catalog> values, Context mContext, Bundle args) {
        this.values = values;
        this.mContext = mContext;
        this.args = args;
        btn_clicked = args.getString(CompanyProductAdapter.BTN_KEY);
    }

    public void ProductCatalogCallbackListener(Callback<Catalog> cbCatalog){
        this.cbCatalog = cbCatalog;
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public Catalog getItem(int position) {
        return values.get(position);
    }

    public List<Catalog> getItems() {
        return values;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.catalog_list_row, viewGroup, false);
        return new CatalogViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        Catalog catalog = getItem(position);
        if(catalog != null){
            CatalogViewHolder catalogViewHolder = (CatalogViewHolder) viewHolder;

            catalogViewHolder.mDescription.setText(catalog.description);

            String points = Integer.toString(catalog.points);
            catalogViewHolder.mTotalPoints.setText(points);

            String imageUrl = Common.getInstance().getAssetUrl(mContext, catalog.id_company+"/puntos/__160-240-320-480__/"+catalog.image, 160);
            Picasso.with(mContext).load(imageUrl).placeholder(R.drawable.placeholder).into(catalogViewHolder.mImage);

            if(btn_clicked != null && btn_clicked.contentEquals(CompanyProductAdapter.BTN_REDEEM)){
                catalogViewHolder.redeemAction.setVisibility(View.VISIBLE);

                int color;
                if(catalog.companyPoints >= catalog.points){
                    color = R.color.tin_color;
                }else{
                    color = R.color.grey600;
                }

                Drawable icon = tintIcon(R.drawable.ic_redeem_white_18dp, color);
                catalogViewHolder.mRedeemIcon.setImageDrawable(icon);

                catalogViewHolder.mRedeemLabel.setTextColor(ContextCompat.getColor(mContext, color));
            }else{
                catalogViewHolder.redeemAction.setVisibility(View.GONE);
            }

        }
    }

    public class CatalogViewHolder extends RecyclerView.ViewHolder {
        private CoverImageView mImage;
        private TextView mDescription;
        private TextView mTotalPoints;
        private LinearLayout mRedeem;
        private LinearLayout redeemAction;
        private ImageView mRedeemIcon;
        private TextView mRedeemLabel;

        public CatalogViewHolder(View v) {
            super(v);
            mImage = (CoverImageView) v.findViewById(R.id.image);
            mDescription = (TextView) v.findViewById(R.id.description);
            mTotalPoints = (TextView) v.findViewById(R.id.totalPoints);
            mRedeem = (LinearLayout) v.findViewById(R.id.btnRedeem);
            redeemAction = (LinearLayout) v.findViewById(R.id.redeemAction);
            mRedeemIcon = (ImageView) v.findViewById(R.id.redeemIcon);
            mRedeemLabel = (TextView) v.findViewById(R.id.redeemLabel);

            if(mRedeem != null){
                mRedeem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Catalog catalog =  getItem(getAdapterPosition());
                        if(catalog!=null){
                            cbCatalog.success(catalog);
                        }
                    }
                });
            }
        }
    }

    private Drawable tintIcon(int icon, int color) {
        final Drawable drawable = ResourcesCompat.getDrawable(mContext.getResources(), icon, null);
        if(drawable != null){
            final Drawable wrapped = DrawableCompat.wrap(drawable);
            try {
                drawable.mutate();
                DrawableCompat.setTint(wrapped, ContextCompat.getColor(mContext, color));
                return wrapped;
            } catch (NullPointerException e) {
                throw new NullPointerException();
            }
        }else{
            return null;
        }
    }
}
