package net.clevermobileapps.ensivar.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.interfaces.RecycleViewRowVariant;
import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.models.Separator;
import net.clevermobileapps.ensivar.utils.Common;

import java.util.ArrayList;
import java.util.List;


public class CompanyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private ArrayList<RecycleViewRowVariant> values;

    private static final int TYPE_SEPARATOR = 1;
    private static final int TYPE_COMPANY = 2;
    private static final int TYPE_FEATURED_COMPANY = 3;

    private boolean featured;

    private Callback<Company> cbCompany;

    public CompanyAdapter(ArrayList<RecycleViewRowVariant> values, Context context, boolean featured) {
        this.values = values;
        this.mContext = context;
        this.featured = featured;
    }

    public void companyCallbackListener(Callback<Company> cbCompany) {
        this.cbCompany = cbCompany;
    }

    @Override
    public int getItemViewType(int position) {
        if(getItem(position) instanceof Company){
            Company company = (Company) getItem(position);
            if(company.getFeatured() && featured){
                return TYPE_FEATURED_COMPANY;
            }else{
                return TYPE_COMPANY;
            }
        }else if(getItem(position) instanceof Separator){
            return TYPE_SEPARATOR;
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public RecycleViewRowVariant getItem(int position) {
        return values.get(position);
    }

    public List<RecycleViewRowVariant> getItems() {
        return values;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType){
            case TYPE_COMPANY:
                View view = inflater.inflate(R.layout.company_list_row, viewGroup, false);
                viewHolder = new CompanyViewHolder(view);
                break;
            case TYPE_FEATURED_COMPANY:
                View view2 = inflater.inflate(R.layout.featured_company_list_row, viewGroup, false);
                viewHolder = new FeaturedCompanyViewHolder(view2);
                break;
            case TYPE_SEPARATOR:
                View view3 = inflater.inflate(R.layout.featured_company_separator_list_row, viewGroup, false);
                viewHolder = new SeparatorViewHolder(view3);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()){
            case TYPE_COMPANY:
                CompanyViewHolder viewHolder1 = (CompanyViewHolder) viewHolder;
                setUpCompanyViewHolder(viewHolder1, position);
                break;
            case TYPE_FEATURED_COMPANY:
                FeaturedCompanyViewHolder viewHolder2 = (FeaturedCompanyViewHolder) viewHolder;
                setUpFeaturedCompanyViewHolder(viewHolder2, position);
                break;
            case TYPE_SEPARATOR:
                SeparatorViewHolder viewHolder3 = (SeparatorViewHolder) viewHolder;
                setUpSeparatorViewHolder(viewHolder3, position);
                break;
        }
    }

    private void setUpCompanyViewHolder(CompanyViewHolder viewHolder, int pos){
        Company company = (Company) getItem(pos);
        if(company != null){
            viewHolder.mCompanyName.setText(company.getName());
        }
    }
    private void setUpFeaturedCompanyViewHolder(FeaturedCompanyViewHolder viewHolder, int pos){
        Company company = (Company) getItem(pos);
        if(company != null){
            viewHolder.mCompanyName.setText(company.getName());

            String imageUrl = Common.getInstance().getAssetUrl(mContext, company.getObjectId()+"/logo/__120-180-240-360__/logo.png", 120);
            Picasso.with(mContext).load(imageUrl).placeholder(R.drawable.empty_360).into(viewHolder.mCompanyLogo);
        }
    }
    private void setUpSeparatorViewHolder(SeparatorViewHolder viewHolder, int pos){

    }

    public class CompanyViewHolder extends RecyclerView.ViewHolder{
        public TextView mCompanyName;
        public CompanyViewHolder(View v) {
            super(v);
            mCompanyName = (TextView) v.findViewById(R.id.name);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Company company = (Company) getItem(getAdapterPosition());
                    if(company != null){
                        cbCompany.success(company);
                    }
                }
            });
        }
    }
    public class FeaturedCompanyViewHolder extends RecyclerView.ViewHolder{
        public TextView mCompanyName;
        public ImageView mCompanyLogo;

        public FeaturedCompanyViewHolder(View v) {
            super(v);
            mCompanyName = (TextView) v.findViewById(R.id.name);
            mCompanyLogo = (ImageView) v.findViewById(R.id.logo);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Company company = (Company) getItem(getAdapterPosition());
                    if(company != null){
                        cbCompany.success(company);
                    }
                }
            });
        }
    }
    public class SeparatorViewHolder extends RecyclerView.ViewHolder{
        public SeparatorViewHolder(View v) {
            super(v);

        }
    }
}
