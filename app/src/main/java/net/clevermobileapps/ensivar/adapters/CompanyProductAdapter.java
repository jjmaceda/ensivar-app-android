package net.clevermobileapps.ensivar.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.clevermobileapps.ensivar.CouponsByCompanyActivity;
import net.clevermobileapps.ensivar.DirectoryActivity;
import net.clevermobileapps.ensivar.GeofenceNotificationActivity;
import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.interfaces.RecycleViewRowVariant;
import net.clevermobileapps.ensivar.models.AlertMessage;
import net.clevermobileapps.ensivar.models.Point;
import net.clevermobileapps.ensivar.models.PromoCoupon;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;
import net.clevermobileapps.ensivar.widgets.LogoImageView;
import net.clevermobileapps.ensivar.widgets.SquareImageView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class CompanyProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;

    private static final int TYPE_COUPON_ROW = 1;
    private static final int TYPE_PROMOTION_ROW = 2;
    private static final int TYPE_POINT_ROW = 3;

    private List<RecycleViewRowVariant> values;

    public static final String COUPON_INTO_PASSBOOK = DirectoryActivity.TAG;
    public static final String COUPON_INTO_COMPANY = CouponsByCompanyActivity.TAG;
    public static final String COUPON_INTO_GEOFENCE_NOTIFICATION = GeofenceNotificationActivity.TAG;

    public static final String BTN_REDEEM = "redeem";
    public static final String BTN_ACCUMULATE = "accumulate";
    public static final String BTN_KEY = "redeem.or.accumulate";

    private Callback<PromoCoupon> cbBtnCoupon;
    private Callback<PromoCoupon> cbPromoCouponLogo;
    private Callback<Bundle> cbBtnRemove;

    private Callback<Point> cbBtnRedeemPoint;
    private Callback<Point> cbBtnAccumulatePoint;
    private Callback<Point> cbPointLogo;

    public CompanyProductAdapter(List<RecycleViewRowVariant> values, Context context) {
        this.values = values;
        this.mContext = context;
    }

    public void BtnCouponCallbackListener(Callback<PromoCoupon> cbCoupon) {this.cbBtnCoupon = cbCoupon;}
    public void BtnRemoveCallbackListener(Callback<Bundle> cbBtnRemove) {this.cbBtnRemove = cbBtnRemove;}
    public void BtnRedeemCallbackListener(Callback<Point> cbBtnRedeemPoint){this.cbBtnRedeemPoint = cbBtnRedeemPoint;}
    public void BtnAccumulateCallbackListener(Callback<Point> cbBtnAccumulatePoint){this.cbBtnAccumulatePoint = cbBtnAccumulatePoint;}
    public void LogoPointCallbackListener(Callback<Point> cbLogo){
        this.cbPointLogo = cbLogo;
    }
    public void LogoPromoCouponCallbackListener(Callback<PromoCoupon> cbPromoCouponLogo){this.cbPromoCouponLogo = cbPromoCouponLogo;}

    @Override
    public int getItemViewType(int position) {
        if(getItem(position) instanceof PromoCoupon){
            PromoCoupon promoCoupon = (PromoCoupon) getItem(position);
            if(promoCoupon.getType().contentEquals("C")){
                return TYPE_COUPON_ROW;
            }
            if(promoCoupon.getType().contentEquals("P")){
                return TYPE_PROMOTION_ROW;
            }
        }else if(getItem(position) instanceof Point){
            return TYPE_POINT_ROW;
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public RecycleViewRowVariant getItem(int position) {
        return values.get(position);
    }

    public void removeItem(int position){
        values.remove(position);
    }

    public List<RecycleViewRowVariant> getItems() {
        return values;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType){
            case TYPE_COUPON_ROW:
                View view = inflater.inflate(R.layout.coupon_list_row, viewGroup, false);
                viewHolder = new CouponViewHolder(view);
                break;
            case TYPE_PROMOTION_ROW:
                View view2 = inflater.inflate(R.layout.promotion_list_row, viewGroup, false);
                viewHolder = new PromotionViewHolder(view2);
                break;
            case TYPE_POINT_ROW:
                View view3 = inflater.inflate(R.layout.point_list_row, viewGroup, false);
                viewHolder = new PointViewHolder(view3);
                break;

        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()){
            case TYPE_COUPON_ROW:
                CouponViewHolder viewHolder1 = (CouponViewHolder) viewHolder;
                setUpCouponViewHolder(viewHolder1, position);
                break;
            case TYPE_PROMOTION_ROW:
                PromotionViewHolder viewHolder2 = (PromotionViewHolder) viewHolder;
                setUpPromotionViewHolder(viewHolder2, position);
                break;
            case TYPE_POINT_ROW:
                PointViewHolder viewHolder3 = (PointViewHolder) viewHolder;
                setUpPointViewHolder(viewHolder3, position);
                break;
        }
    }

    private void setUpCouponViewHolder(CouponViewHolder viewHolder, int pos){
        PromoCoupon promoCoupon = (PromoCoupon) values.get(pos);
        if(promoCoupon != null){

            //COMPANY LOGO
            String logoUrl = Common.getInstance().getAssetUrl(mContext, promoCoupon.getCompany().getObjectId()+"/logo/__40-60-80-120__/logo.png", 40);
            Picasso.with(mContext).load(logoUrl).placeholder(R.drawable.empty_360).into(viewHolder.mCompanyLogoImageView);

            //COMPANY NAME
            viewHolder.mCompanyNameTextView.setText(promoCoupon.getCompany().getString("name"));

            //COUPON DESCRIPTION
            if(!promoCoupon.getDescription().isEmpty()){
                viewHolder.mDescTextView.setText(promoCoupon.getDescription());
                viewHolder.mDescTextView.setVisibility(View.VISIBLE);
            }

            //COUPON IMAGE
            if(!promoCoupon.getImage().isEmpty()){
                String couponImage =  Common.getInstance().getAssetUrl(mContext, promoCoupon.getCompany().getObjectId()+"/cupones/__320-480-640-960__/"+promoCoupon.getImage(), 320);
                Picasso.with(mContext).load(couponImage).placeholder(R.drawable.empty_360).into(viewHolder.mSquareImageView);
            }

            //COUPON ACTION BUTTON
            String activityName = mContext.getClass().getSimpleName();

            //INTO PASSBOOK
            if(activityName.contentEquals(COUPON_INTO_PASSBOOK)){
                int color =  R.color.tin_color;
                if(promoCoupon.getIsRedeem() || Constants.today.after(promoCoupon.getValidUntil())){
                    color = R.color.grey600;
                }

                //BTN REDEEM ICON
                Drawable icon = Common.tintIcon(R.drawable.ic_redeem_white_18dp, color, mContext);
                viewHolder.mIconBtn.setImageDrawable(icon);

                //BTN REDEEM LABEL
                viewHolder.mCouponLabel.setText(mContext.getResources().getText(R.string.redeem_btn));
                viewHolder.mCouponLabel.setTextColor(ContextCompat.getColor(mContext, color));

                //QUANTITY TEXTVIEW
                viewHolder.mQuantityOfCouponTv.setVisibility(View.GONE);

                //CHANGE GRAVITY DIRECTION FOR VALID UNTIL TEXTVIEW
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) viewHolder.mValidUntilCouponTextView.getLayoutParams();
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                viewHolder.mValidUntilCouponTextView.setLayoutParams(layoutParams);


                //INTO COMPANY OR INTO GEOFENCE NOTIFICATION
            }else if(activityName.contentEquals(COUPON_INTO_COMPANY) || activityName.contentEquals(COUPON_INTO_GEOFENCE_NOTIFICATION)){

                int color = R.color.tin_color;
                if(promoCoupon.getQuantity() <= 0){ // //Quantity of coupons available
                    color = R.color.grey600;
                }else if(activityName.contentEquals(COUPON_INTO_GEOFENCE_NOTIFICATION) && !Common.isUserLoggedIn()){  //Quantity of coupons available
                    color = R.color.grey600;
                }

                //BTN ACCUMULATE ICON
                Drawable icon = Common.tintIcon(R.drawable.ic_add_white_18dp, color, mContext);
                viewHolder.mIconBtn.setImageDrawable(icon);

                //BTN ACCUMULATE LABEL
                viewHolder.mCouponLabel.setText(mContext.getResources().getText(R.string.addToPassbook_btn));
                viewHolder.mCouponLabel.setTextColor(ContextCompat.getColor(mContext, color));

                //BTN REMOVE
                viewHolder.mRemoveCouponBtn.setVisibility(View.GONE);

                //QUANTITY TEXTVIEW
                String qty = Integer.toString(promoCoupon.getQuantity()) +" disponibles";
                viewHolder.mQuantityOfCouponTv.setText(qty);
            }

            //COUPON VALID DATE
            String formattedDate = "Validez: " + Common.formatDate(promoCoupon.getValidUntil());
            viewHolder.mValidUntilCouponTextView.setText(formattedDate);

        }
    }
    private void setUpPromotionViewHolder(PromotionViewHolder viewHolder, int pos){
        PromoCoupon promoCoupon = (PromoCoupon) values.get(pos);
        if(promoCoupon != null){

            //COMPANY LOGO
            String logoUrl = Common.getInstance().getAssetUrl(mContext, promoCoupon.getCompany().getObjectId()+"/logo/__40-60-80-120__/logo.png", 40);
            Picasso.with(mContext).load(logoUrl).placeholder(R.drawable.empty_360).into(viewHolder.mCompanyLogoImageView);

            //COMPANY NAME
            viewHolder.mCompanyNameTextView.setText(promoCoupon.getCompany().getString("name"));

            //PROMOTION IMAGE
            if(!promoCoupon.getImage().isEmpty()){
                String couponImage =  Common.getInstance().getAssetUrl(mContext, promoCoupon.getCompany().getObjectId()+"/promos/__320-480-640-960__/"+promoCoupon.getImage(), 320);
                Picasso.with(mContext).load(couponImage).placeholder(R.drawable.empty_360).into(viewHolder.mSquareImageView);
            }

            //PROMOTION DESCRIPTION
            if(!promoCoupon.getDescription().isEmpty()){
                viewHolder.mDescTextView.setText(promoCoupon.getDescription());
                viewHolder.mDescTextView.setVisibility(View.VISIBLE);
            }
        }
    }
    private void setUpPointViewHolder(PointViewHolder viewHolder, int pos){
        Point point = (Point) values.get(pos);

        if(point != null){

            //COMPANY LOGO
            String logoUrl = Common.getInstance().getAssetUrl(mContext, point.id_company+"/logo/__120-180-240-360__/logo.png", 120);
            Picasso.with(mContext).load(logoUrl).placeholder(R.drawable.empty_360).into(viewHolder.mCompanyLogoImageView);

            //COMPANY NAME
            viewHolder.mCompanyNameTextView.setText(point.name);

            //ICON FOR REDEEM BUTTON
            Drawable iconRedeem = Common.tintIcon(R.drawable.ic_redeem_white_18dp, R.color.tin_color, mContext);
            viewHolder.mIconRedeem.setImageDrawable(iconRedeem);

            //ICON FOR ACCUMULATE BUTTON
            Drawable iconAccumulate = Common.tintIcon(R.drawable.ic_add_white_18dp, R.color.tin_color, mContext);
            viewHolder.mIconAccumulate.setImageDrawable(iconAccumulate);

            //TOTAL POINTS
            if(point.points > 0){
                String number = Integer.toString(point.points);
                double amount = Double.parseDouble(number);
                DecimalFormat formatter = new DecimalFormat("#,###");
                String output = formatter.format(amount);
                viewHolder.mAccumulatedPointsTextView.setText(output);
            }else{
                viewHolder.mAccumulatedPointsTextView.setText("0");
            }

            if(point.predominantColor.size() > 0){
                viewHolder.mAccumulatedPointsTextView.setTextColor(Color.parseColor(point.predominantColor.get(0)));
                viewHolder.getmAccumulatedPointsLabelTextView.setTextColor(Color.parseColor(point.predominantColor.get(0)));
            }
        }
    }

    private class CouponViewHolder extends RecyclerView.ViewHolder {
        public ImageView mCompanyLogoImageView;
        public TextView mCompanyNameTextView;
        public SquareImageView mSquareImageView;
        public TextView mValidUntilCouponTextView;
        public TextView mDescTextView;
        public ImageView mIconBtn;
        public LinearLayout mCouponBtn;
        public TextView mCouponLabel;
        public ImageView mRemoveCouponBtn;
        public TextView mQuantityOfCouponTv;

        public CouponViewHolder(View v) {
            super(v);
            mCompanyNameTextView = (TextView) v.findViewById(R.id.name);
            //
            mCompanyLogoImageView = (ImageView) v.findViewById(R.id.logo);
            if(mCompanyLogoImageView != null){
                mCompanyLogoImageView.setClickable(true);
                mCompanyLogoImageView.setFocusable(true);
                mCompanyLogoImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PromoCoupon promoCoupon = (PromoCoupon) getItem(getAdapterPosition());
                        if(promoCoupon != null){
                            cbPromoCouponLogo.success(promoCoupon);
                        }
                    }
                });
            }
            //
            mSquareImageView = (SquareImageView) v.findViewById(R.id.imageCardView);
            mValidUntilCouponTextView = (TextView) v.findViewById(R.id.validityOfCoupon);
            mDescTextView = (TextView) v.findViewById(R.id.contentCardView);
            mIconBtn = (ImageView) v.findViewById(R.id.iconBtn);
            //
            mCouponBtn = (LinearLayout) v.findViewById(R.id.couponBtn);
            if(mCouponBtn != null){
                mCouponBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PromoCoupon promoCoupon = (PromoCoupon) getItem(getAdapterPosition());
                        if(promoCoupon != null){
                            cbBtnCoupon.success(promoCoupon);
                        }
                    }
                });
            }

            //
            mCouponLabel = (TextView) v.findViewById(R.id.couponBtnLabel);
            mRemoveCouponBtn = (ImageView) v.findViewById(R.id.removeCouponBtn);
            if(mRemoveCouponBtn!= null){
                mRemoveCouponBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PromoCoupon promoCoupon = (PromoCoupon) getItem(getAdapterPosition());
                        if(promoCoupon != null){
                            Bundle bundle = new Bundle();
                            bundle.putString("objectId", promoCoupon.getObjectId());
                            bundle.putInt("position", getAdapterPosition());
                            cbBtnRemove.success(bundle);
                        }
                    }
                });
            }

            mQuantityOfCouponTv = (TextView) v.findViewById(R.id.quantityOfCoupon);

        }

    }

    private class PromotionViewHolder extends RecyclerView.ViewHolder{
        public ImageView mCompanyLogoImageView;
        public TextView mCompanyNameTextView;
        public SquareImageView mSquareImageView;
        public TextView mDescTextView;

        public PromotionViewHolder(View v) {
            super(v);
            mCompanyNameTextView = (TextView) v.findViewById(R.id.name);
            //
            mCompanyLogoImageView = (ImageView) v.findViewById(R.id.logo);
            if(mCompanyLogoImageView != null){
                mCompanyLogoImageView.setClickable(true);
                mCompanyLogoImageView.setFocusable(true);
                mCompanyLogoImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PromoCoupon promoCoupon = (PromoCoupon) getItem(getAdapterPosition());
                        if(promoCoupon != null){
                            cbPromoCouponLogo.success(promoCoupon);
                        }
                    }
                });
            }

            //
            mSquareImageView = (SquareImageView) v.findViewById(R.id.imageCardView);
            mDescTextView = (TextView) v.findViewById(R.id.contentCardView);
        }
    }

    private class PointViewHolder extends RecyclerView.ViewHolder{
        public TextView mCompanyNameTextView;
        public LogoImageView mCompanyLogoImageView;
        public TextView mAccumulatedPointsTextView;
        public TextView getmAccumulatedPointsLabelTextView;
        public LinearLayout mBtnRedeem;
        public LinearLayout mBtnAccumulate;
        public ImageView mIconRedeem;
        public ImageView mIconAccumulate;

        public PointViewHolder(View v) {
            super(v);

            mCompanyNameTextView = (TextView) v.findViewById(R.id.name);
            //
            mCompanyLogoImageView = (LogoImageView) v.findViewById(R.id.logo);
            if(mCompanyLogoImageView!= null){
                mCompanyLogoImageView.setClickable(true);
                mCompanyLogoImageView.setFocusable(true);
                mCompanyLogoImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Point point = (Point) getItem(getAdapterPosition());
                        if(point != null){
                            cbPointLogo.success(point);
                        }
                    }
                });
            }

            //
            mAccumulatedPointsTextView = (TextView) v.findViewById(R.id.points);
            getmAccumulatedPointsLabelTextView = (TextView) v.findViewById(R.id.pointsLabel);
            //
            mBtnRedeem = (LinearLayout) v.findViewById(R.id.btnRedeem);
            if(mBtnRedeem!= null){
                mBtnRedeem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Point point = (Point) getItem(getAdapterPosition());
                        if(point != null){
                            cbBtnRedeemPoint.success(point);
                        }
                    }
                });
            }


            mBtnAccumulate = (LinearLayout) v.findViewById(R.id.btnAccumulate);
            if(mBtnAccumulate!=null){
                mBtnAccumulate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Point point = (Point) getItem(getAdapterPosition());
                        if(point != null){
                            cbBtnAccumulatePoint.success(point);
                        }
                    }
                });
            }

            //
            mIconRedeem = (ImageView) v.findViewById(R.id.iconBtnRedeem);
            mIconAccumulate = (ImageView) v.findViewById(R.id.iconBtnAccumulate);
        }
    }


}
