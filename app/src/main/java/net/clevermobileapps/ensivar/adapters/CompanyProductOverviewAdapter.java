package net.clevermobileapps.ensivar.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.models.CompanyInfo;
import net.clevermobileapps.ensivar.models.CompanyOverview;
import net.clevermobileapps.ensivar.models.Separator;
import net.clevermobileapps.ensivar.utils.Constants;

import java.util.List;

public class CompanyProductOverviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Object> values;
    private Context context;
    private Callback<CompanyInfo> cbCompanyInfo;
    private Callback<CompanyOverview> cbCompanyProduct;
    private static final int TYPE_COMPANY_INFO = 1, TYPE_COMPANY_PRODUCT = 2, TYPE_SEPARATOR = 3;

    public CompanyProductOverviewAdapter(List<Object> values, Context context, Callback<CompanyInfo> cbCompanyInfo, Callback<CompanyOverview> cbCompanyProduct) {
        this.values = values;
        this.context = context;
        this.cbCompanyInfo = cbCompanyInfo;
        this.cbCompanyProduct = cbCompanyProduct;
    }

    @Override
    public int getItemViewType(int position) {
        if(values.get(position) instanceof CompanyInfo){
            return TYPE_COMPANY_INFO;
        }else if(values.get(position) instanceof CompanyOverview){
            return TYPE_COMPANY_PRODUCT;
        }else if(values.get(position) instanceof Separator){
            return TYPE_SEPARATOR;
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public Object getItem(int position) {
        return values.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType){
            case TYPE_COMPANY_INFO:
                View view = inflater.inflate(R.layout.product_info_list_row, viewGroup, false);
                viewHolder = new ProductInfoViewHolder(view);
                break;
            case TYPE_COMPANY_PRODUCT:
                View view2 = inflater.inflate(R.layout.product_list_row, viewGroup, false);
                viewHolder = new ProductViewHolder(view2);
                break;
            case TYPE_SEPARATOR:
                View view3 = inflater.inflate(R.layout.separator_list_row, viewGroup, false);
                viewHolder = new SeparatorViewHolder(view3);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()){
            case TYPE_COMPANY_INFO:
                ProductInfoViewHolder viewHolder1 = (ProductInfoViewHolder) viewHolder;
                setUpProductInfoViewHolder(viewHolder1, position);
                break;
            case TYPE_COMPANY_PRODUCT:
                ProductViewHolder viewHolder2 = (ProductViewHolder) viewHolder;
                setUpProductViewHolder(viewHolder2, position);
                break;
        }
    }
    public List<Object> getItems() {
        return values;
    }

    private void setUpProductInfoViewHolder(ProductInfoViewHolder viewHolder, int position){
        CompanyInfo companyInfo = (CompanyInfo) values.get(position);
        if(companyInfo != null){
            switch (companyInfo.name){
                case Constants.COMPANY_EMAIL:
                    Drawable icon = tintIcon(R.drawable.ic_email_black_36dp, R.color.primaryColor);
                    viewHolder.mCompanyInfoIcon.setImageDrawable(icon);
                    viewHolder.mCompanyInfoName.setText(context.getResources().getString(R.string.companyProductEmailString));
                    break;
                case Constants.COMPANY_WEB:
                    Drawable icon2 = tintIcon(R.drawable.ic_web_black_36dp, R.color.primaryColor);
                    viewHolder.mCompanyInfoIcon.setImageDrawable(icon2);
                    viewHolder.mCompanyInfoName.setText(context.getResources().getString(R.string.companyProductWebString));
                    break;
                case Constants.COMPANY_SOCIAL:
                    Drawable icon3 = tintIcon(R.drawable.ic_group_black_36dp, R.color.primaryColor);
                    viewHolder.mCompanyInfoIcon.setImageDrawable(icon3);
                    viewHolder.mCompanyInfoName.setText(context.getResources().getString(R.string.companyProductSocialString));
                    break;
            }
        }
    }
    private void setUpProductViewHolder(ProductViewHolder viewHolder, int position){
        CompanyOverview companyOverview = (CompanyOverview) values.get(position);
        if(companyOverview != null){
            switch (companyOverview.type){
                case Constants.COMPANY_PROMOTION:
                    Drawable icon1 = tintIcon(R.drawable.ic_local_offer_black_36dp, R.color.tin_color);
                    viewHolder.mProductIcon.setImageDrawable(icon1);
                    break;
                case Constants.COMPANY_COUPON:
                    Drawable icon2 = tintIcon(R.drawable.ic_local_activity_black_36dp, R.color.tin_color);
                    viewHolder.mProductIcon.setImageDrawable(icon2);
                    break;
                case Constants.COMPANY_POINT:
                    Drawable icon3 = tintIcon(R.drawable.ic_stars_black_36dp, R.color.tin_color);
                    viewHolder.mProductIcon.setImageDrawable(icon3);
                    break;
            }
            viewHolder.mProductName.setText(companyOverview.name);
        }
    }


    public class ProductInfoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mCompanyInfoName;
        public ImageView mCompanyInfoIcon;

        @Override
        public void onClick(View v) {
            CompanyInfo companyInfo = (CompanyInfo) getItem(getAdapterPosition());
            if(companyInfo != null){
                cbCompanyInfo.success(companyInfo);
            }
        }

        public ProductInfoViewHolder(View v) {
            super(v);
            mCompanyInfoName = (TextView) v.findViewById(R.id.companyInfoName);
            mCompanyInfoIcon = (ImageView) v.findViewById(R.id.companyInfoIcon);
            v.setOnClickListener(this);
        }
    }
    public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mProductName;
        public ImageView mProductIcon;

        @Override
        public void onClick(View v) {
            CompanyOverview companyOverview = (CompanyOverview) getItem(getAdapterPosition());
            if(companyOverview != null){
              cbCompanyProduct.success(companyOverview);
            }
        }

        public ProductViewHolder(View v) {
            super(v);
            mProductName = (TextView) v.findViewById(R.id.description);
            mProductIcon = (ImageView) v.findViewById(R.id.productIcon);
            v.setOnClickListener(this);
        }
    }

    public class SeparatorViewHolder extends RecyclerView.ViewHolder{
        public SeparatorViewHolder(View v) {
            super(v);
        }
    }


    private Drawable tintIcon(int icon, int color) {
        final Drawable drawable = ResourcesCompat.getDrawable(context.getResources(), icon, null);
        final Drawable wrapped = DrawableCompat.wrap(drawable);
        try {
            drawable.mutate();
            DrawableCompat.setTint(wrapped, ContextCompat.getColor(context, color));
            return wrapped;
        } catch (NullPointerException e) {
            throw new NullPointerException();
        }
    }


}
