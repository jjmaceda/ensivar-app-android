package net.clevermobileapps.ensivar.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androiddeveloperlb.listviewvariants.SearchablePinnedHeaderListViewAdapter;
import com.androiddeveloperlb.listviewvariants.StringArrayAlphabetIndexer;

import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.utils.CircularContactView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CompanyVariantListAdapter extends SearchablePinnedHeaderListViewAdapter<Company>{
    private List<Company> values;
    private Context context;
    private int[] PHOTO_TEXT_BACKGROUND_COLORS = {};

    public CompanyVariantListAdapter(Context context, List<Company> values) {
        this.context = context;
        this.values = values;
        setData(values);
        if(context != null){
            PHOTO_TEXT_BACKGROUND_COLORS = context.getResources().getIntArray(R.array.contacts_text_background_colors);
        }
    }

    @Override
    public CharSequence getSectionTitle(int sectionIndex) {
        return ((StringArrayAlphabetIndexer.AlphaBetSection)getSections()[sectionIndex]).getName();
    }

    public void setData(final List<Company> companies)
    {
        final String[] generatedContactNames=generateContactNames(companies);
        setSectionIndexer(new StringArrayAlphabetIndexer(generatedContactNames,true));
    }

    private String[] generateContactNames(final List<Company> companies)
    {
        final ArrayList<String> companyNames= new ArrayList<>();
        if(companies!=null)
            for(final Company companyEntity : companies)
                companyNames.add(companyEntity.getName());
        return companyNames.toArray(new String[companyNames.size()]);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final View view;
        final Company company = getItem(position);
        final String companyName = company.getName();
        final String idCompany = company.getObjectId();

        if(convertView == null){
            holder = new ViewHolder();
            LayoutInflater mInflater = LayoutInflater.from(context);
            view = mInflater.inflate(R.layout.list_view_item, parent, false);
            holder.friendProfileCircularContactView = (CircularContactView) view.findViewById(R.id.listview_item__friendPhotoImageView);
            holder.friendProfileCircularContactView.getTextView().setTextColor(0xFFffffff);
            holder.companyNameTxt = (TextView)view.findViewById(R.id.listview_item__friendNameTextView);
            holder.headerTxt = (TextView)view.findViewById(R.id.header_text);

            view.setTag(holder);
        }else{
            view = convertView;
            holder = (ViewHolder)view.getTag();
        }

        holder.companyNameTxt.setText(companyName);

        final int backgroundColorToUse = PHOTO_TEXT_BACKGROUND_COLORS[position%PHOTO_TEXT_BACKGROUND_COLORS.length];
        final String characterToShow = companyName.substring(0, 1).toUpperCase(Locale.getDefault());
        holder.friendProfileCircularContactView.setTextAndBackgroundColor(characterToShow, backgroundColorToUse);

        bindSectionHeader(holder.headerTxt, null, position);

        holder.name = companyName;
        holder.idCompany = idCompany;

        return view;
    }

    @Override
    public boolean doFilter(Company item, CharSequence constraint) {

        if(TextUtils.isEmpty(constraint)){
            return true;
        }
        final String companyIndexer = item.getName();
        return !TextUtils.isEmpty(companyIndexer) && companyIndexer.toLowerCase(Locale.getDefault()).contains(constraint.toString().toLowerCase(Locale.getDefault()));
    }

    @Override
    public ArrayList<Company> getOriginalList() {
        if(values != null) {
            return (ArrayList<Company>) values;
        }
        return new ArrayList<>();
    }

    private static class ViewHolder
    {
        public CircularContactView friendProfileCircularContactView;
        TextView companyNameTxt, headerTxt;
        String idCompany, name;
    }
}


