package net.clevermobileapps.ensivar.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import net.clevermobileapps.ensivar.fragments.CompanyVariantListFragment;
import net.clevermobileapps.ensivar.fragments.ProfileFragment;
import net.clevermobileapps.ensivar.fragments.TagsFragment;

public class DirectoryFragmentPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    private static String TAG = "DirectoryTabsAdapter";
    private Bundle args = new Bundle();

    public static DirectoryFragmentPagerAdapter newInstance(FragmentManager fm, Bundle args) {
        DirectoryFragmentPagerAdapter fragment = new DirectoryFragmentPagerAdapter(fm);
        fragment.args = args;
        return fragment;
    }

    public DirectoryFragmentPagerAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        Bundle bundle = new Bundle();

        Log.d(TAG, "getItem DirectoryFragmentPagerAdapter");

        switch (position) {
            case 0:
                //second tab
                bundle.putString("action", "getTagList");
                bundle.putString("type", "withTags");
                fragment = TagsFragment.newInstance(bundle);
                break;
            case 1:
                //third tab
                fragment = ProfileFragment.newInstance(args);
                break;
            case 2:
                //first tab
                bundle.putString("action", "getCompanyList");
                bundle.putString("type", "withAlphabeticalOrder");
                fragment = CompanyVariantListFragment.newInstance(bundle);
                break;
            default:
                bundle.putString("action", "getTagList");
                bundle.putString("type", "withTags");
                fragment = TagsFragment.newInstance(bundle);
        }

        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate name based on item position
        return null;
    }
}
