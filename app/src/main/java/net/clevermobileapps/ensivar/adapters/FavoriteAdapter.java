package net.clevermobileapps.ensivar.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.models.Favorite;

import java.util.List;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {
    private final static String TAG = "FavoriteListAdapter";
    private List<Favorite> values;
    private Callback<Integer> cb;

    public FavoriteAdapter(List<Favorite> values, Callback<Integer> cb) {
        this.values = values;
        this.cb = cb;
    }

    @Override
    public FavoriteAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int pos) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favorite_list_row, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int pos) {
        Favorite favorite = values.get(pos);
        holder.mTextView.setText(favorite.getName());
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public Favorite getItem(int position) {
        return values.get(position);
    }

    public List<Favorite> getItems() {
        return values;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mTextView;

        @Override
        public void onClick(View v) {
            //Log.d(TAG, Integer.toString(getAdapterPosition()));
            showLocationDetail(getAdapterPosition());
        }

        public ViewHolder(View v) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.favorite_name);
            v.setOnClickListener(this);
        }
    }

    private void showLocationDetail(int pos) {
        cb.success(pos);
    }

}
