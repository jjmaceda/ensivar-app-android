package net.clevermobileapps.ensivar.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.interfaces.LocationType;
import net.clevermobileapps.ensivar.models.Location;
import net.clevermobileapps.ensivar.utils.Common;

import java.util.List;
import java.util.Locale;

import info.hoang8f.android.segmented.SegmentedGroup;

public class LocationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements
        Filterable {
    private final static String TAG = "LocationListAdapter";
    private List<LocationType> values;
    private Callback<Integer> cbLocation;
    private Callback<Boolean> cbInfoNear;
    private Callback<String> cbInfoDial;
    private Callback<Integer> cbBankFilter;
    private Context context;
    private android.location.Location currentLocation;
    private String currentBankFilter;
    private static final int TYPE_INFO = 1, TYPE_LOCATION = 2;

    public LocationAdapter(
            Context context,
            Callback<Integer> cbLocation,
            Callback<String> cbInfoDial,
            Callback<Boolean> cbInfoNear,
            Callback<Integer> cbBankFilter,
            android.location.Location currentLocation,
            String currentBankFilter,
            List<LocationType> values) {
        this.context = context;
        this.values = values;
        this.cbLocation = cbLocation;
        this.cbInfoDial = cbInfoDial;
        this.cbInfoNear = cbInfoNear;
        this.cbBankFilter = cbBankFilter;
        this.currentLocation = currentLocation;
        this.currentBankFilter = currentBankFilter;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case TYPE_INFO:
                View view = inflater.inflate(R.layout.info_general_row, viewGroup, false);
                viewHolder = new InfoViewHolder(view);
                break;
            case TYPE_LOCATION:
                View view2 = inflater.inflate(R.layout.location_list_row, viewGroup, false);
                viewHolder = new LocationViewHolder(view2);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            case TYPE_INFO:
                InfoViewHolder viewHolder1 = (InfoViewHolder) viewHolder;
                setUpInfoHolder(viewHolder1, position);
                break;
            case TYPE_LOCATION:
                LocationViewHolder viewHolder2 = (LocationViewHolder) viewHolder;
                setUpLocationViewHolder(viewHolder2, position);
                break;
        }
    }

    void setUpInfoHolder(InfoViewHolder viewHolder, int pos) {
        if (pos == 0) {
            final Location.Info item = (Location.Info) values.get(pos);

            if (item.predominantColor != null && item.predominantColor.size() > 1) {
                int lightColor = Color.parseColor(item.predominantColor.get(0));
                int darkColor = Color.parseColor(item.predominantColor.get(1));
                ColorStateList colorStateList = new ColorStateList(
                        new int[][]{
                                new int[]{android.R.attr.state_pressed},
                                new int[]{android.R.attr.state_focused},
                                new int[]{android.R.attr.color},
                                new int[]{}
                        },
                        new int[]{
                                lightColor,
                                lightColor,
                                darkColor,
                                lightColor
                        });

                Common.getInstance().setImageButtonTint(viewHolder.mCallBtn, colorStateList);
            } else {
                Common.getInstance().setImageButtonTint(viewHolder.mCallBtn, ContextCompat
                        .getColorStateList(context, R.color.default_btn_color_selector));
            }

            if (item.predominantColor != null && item.predominantColor.size() > 0) {
                int lightColor = Color.parseColor(item.predominantColor.get(0));
                int darkColor = Color.parseColor(item.predominantColor.get(1));
                ColorStateList switchThumbState = new ColorStateList(
                        new int[][]{
                                new int[]{android.R.attr.state_checked},
                                new int[]{-android.R.attr.state_checked},
                                new int[]{}
                        },
                        new int[]{
                                lightColor,
                                ContextCompat.getColor(context, R.color.grey200),
                                ContextCompat.getColor(context, R.color.grey200)
                        }
                );
                ColorStateList switchTrackState = new ColorStateList(
                        new int[][]{
                                new int[]{android.R.attr.state_checked},
                                new int[]{-android.R.attr.state_checked},
                                new int[]{}
                        },
                        new int[]{
                                darkColor,
                                ContextCompat.getColor(context, R.color.grey400),
                                ContextCompat.getColor(context, R.color.grey400)
                        }
                );
                viewHolder.mNearSwitch.setThumbTintList(switchThumbState);
                viewHolder.mNearSwitch.setTrackTintList(switchTrackState);

                if(item.isBank) {
                    viewHolder.mBankFilter.setTintColor(Color.parseColor(item.predominantColor.get
                            (0)),
                            Color.parseColor("#FFFFFFFF"));
                }
            }

            if(item.isBank) {
                viewHolder.mBankFilter.setVisibility(View.VISIBLE);
                viewHolder.mBankFilter.setOnCheckedChangeListener(null);
                if(currentBankFilter.equals("ATM")) {
                    viewHolder.mBankFilter.check(R.id.filterForBankATMBtn);
                } else {
                    viewHolder.mBankFilter.check(R.id.filterForBankBankBtn);
                }
                viewHolder.mBankFilter.setOnCheckedChangeListener(viewHolder);
            }
        }
    }

    void setUpLocationViewHolder(LocationViewHolder viewHolder, int pos) {
        final Location item = (Location) values.get(pos);
        final String phone = item.getPhone();
        viewHolder.mTextView.setText(item.getName());
        viewHolder.mPhoneView.setText(phone.isEmpty() ? "No Disponible" : phone);

        if (currentLocation != null) {
            viewHolder.mDistanceView.setVisibility(View.VISIBLE);
            android.location.Location loc1 = new android.location.Location("");
            LatLng locationGeo = item.getGeo();
            loc1.setLatitude(locationGeo.latitude);
            loc1.setLongitude(locationGeo.longitude);

            float distanceInMeters = loc1.distanceTo(currentLocation);
            double distanceInKm = distanceInMeters * 0.001;
            viewHolder.mDistanceView.setText(String.format(Locale.US, "%.2f km", distanceInKm));
        } else {
            viewHolder.mDistanceView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_INFO;
        } else {
            return TYPE_LOCATION;
        }
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public LocationType getItem(int position) {
        return values.get(position);
    }

    public List<LocationType> getItems() {
        return values;
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    public class InfoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            CompoundButton.OnCheckedChangeListener, RadioGroup.OnCheckedChangeListener {
        public AppCompatImageButton mCallBtn;
        public SwitchCompat mNearSwitch;
        final public SegmentedGroup mBankFilter;

        @Override
        public void onClick(View v) {
            cbInfoDial.success("DIAL");
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            cbInfoNear.success(isChecked);
        }

        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            int radioButtonID = mBankFilter.getCheckedRadioButtonId();
            View radioButton = mBankFilter.findViewById(radioButtonID);
            int idx = mBankFilter.indexOfChild(radioButton);
            cbBankFilter.success(idx);
        }

        public InfoViewHolder(View v) {
            super(v);
            mCallBtn = (AppCompatImageButton) v.findViewById(R.id.call);
            mNearSwitch = (SwitchCompat) v.findViewById(R.id.location_near);
            mBankFilter = (SegmentedGroup) v.findViewById(R.id.filterForBank);

            mCallBtn.setOnClickListener(this);
            mNearSwitch.setOnCheckedChangeListener(this);
            //only if bank filter is enable
            mBankFilter.setOnCheckedChangeListener(this);

            if (currentLocation != null) {
                mNearSwitch.setOnCheckedChangeListener(null);
                mNearSwitch.setChecked(true);
                mNearSwitch.setOnCheckedChangeListener(this);
            }
        }
    }

    public class LocationViewHolder extends RecyclerView.ViewHolder implements View
            .OnClickListener {
        public TextView mTextView;
        public TextView mPhoneView;
        public TextView mDistanceView;

        @Override
        public void onClick(View v) {
            showLocationDetail(getAdapterPosition());
        }

        public LocationViewHolder(View v) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.location_name);
            mPhoneView = (TextView) v.findViewById(R.id.phone);
            mDistanceView = (TextView) v.findViewById(R.id.distance);
            v.setOnClickListener(this);
        }
    }

    private void showLocationDetail(int position) {
        cbLocation.success(position);
    }


}
