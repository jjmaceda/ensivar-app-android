
package net.clevermobileapps.ensivar.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import net.clevermobileapps.ensivar.NotificationActivity;
import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.interfaces.RecycleViewRowVariant;
import net.clevermobileapps.ensivar.models.AlertMessage;
import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.utils.Common;

import java.util.List;


public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<RecycleViewRowVariant> values;
    private List<String> blackList;
    private Context mContext;
    private Callback<Bundle> cbNotificationStatus;
    private Callback<Bundle> cbGoToFacebook;
    private SharedPreferences preferences;
    private static final int TYPE_ALERT_MESSAGE_ROW = 1;
    private static final int TYPE_COMPANY_ROW = 2;
    private boolean isUserLoggedIn = false;

    public NotificationAdapter(List<RecycleViewRowVariant> values, Context mContext, List<String> blackList) {
        isUserLoggedIn = Common.isUserLoggedIn();

        this.values = values;
        this.blackList = blackList;
        this.mContext = mContext;
        if(mContext != null){
            preferences = mContext.getSharedPreferences(NotificationActivity.NOTIFICATION_SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
        }
    }

    public void NotificationStatusCallbackListener(Callback<Bundle> cbNotificationStatus){
        this.cbNotificationStatus = cbNotificationStatus;
    }
    public void goToFacebookCallbackListener(Callback<Bundle> cbGoToFacebook){
        this.cbGoToFacebook = cbGoToFacebook;
    }

    @Override
    public int getItemViewType(int position) {
        if(getItem(position) instanceof AlertMessage){
            return TYPE_ALERT_MESSAGE_ROW;
        }else if(getItem(position) instanceof Company){
            return TYPE_COMPANY_ROW;
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public RecycleViewRowVariant getItem(int position) {
        return values.get(position);
    }

    public List<RecycleViewRowVariant> getItems() {
        return values;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType){
            case TYPE_ALERT_MESSAGE_ROW:
                View view = inflater.inflate(R.layout.notification_alert_message, viewGroup, false);
                viewHolder = new AlertMessageViewHolder(view);
                break;
            case TYPE_COMPANY_ROW:
                View view2 = inflater.inflate(R.layout.notification_list_row, viewGroup, false);
                viewHolder = new NotificationViewHolder(view2);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        switch (viewHolder.getItemViewType()){
            case TYPE_ALERT_MESSAGE_ROW:
                AlertMessageViewHolder viewHolder1 = (AlertMessageViewHolder) viewHolder;
                setMessageInfoViewHolder(viewHolder1, position);
                break;
            case TYPE_COMPANY_ROW:
                NotificationViewHolder viewHolder2 = (NotificationViewHolder) viewHolder;
                setNotificationViewHolder(viewHolder2, position);
                break;
        }


    }

    private void setNotificationViewHolder(NotificationViewHolder viewHolder, int position){
        Company company = (Company) getItem(position);
        if(company != null){

            String imageUrl = Common.getInstance().getAssetUrl(mContext, company.getObjectId()+"/logo/__120-180-240-360__/logo.png", 120);
            Picasso.with(mContext).load(imageUrl).placeholder(R.drawable.placeholder).into(viewHolder.mLogo);

            viewHolder.mName.setText(company.getName());

            if(isUserLoggedIn){
                if(preferences.contains(company.getObjectId())){
                    viewHolder.mStatus.setChecked(false);
                }
                if(blackList.contains(company.getObjectId())){
                    viewHolder.mStatus.setChecked(false);
                }
            }else{
                viewHolder.mStatus.setEnabled(false);
            }
        }
    }

    private void setMessageInfoViewHolder(AlertMessageViewHolder viewHolder, int position){
        AlertMessage alertMessage = (AlertMessage) getItem(position);
        if(alertMessage != null){
            viewHolder.mMessage.setText(mContext.getResources().getString(R.string.loggedInFacebookNotifications));
        }
    }

    private class NotificationViewHolder extends RecyclerView.ViewHolder {
        private ImageView mLogo;
        private TextView mName;
        private SwitchCompat mStatus;

        public NotificationViewHolder(View v) {
            super(v);
            mLogo = (ImageView) v.findViewById(R.id.logo);
            mName = (TextView) v.findViewById(R.id.name);
            mStatus = (SwitchCompat) v.findViewById(R.id.status);
            if(mStatus!=null){
                mStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    }
                });
                mStatus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Company company = (Company) getItem(getAdapterPosition());
                        if(company != null){
                            Bundle args = new Bundle();
                            args.putString("objectId", company.getObjectId());
                            args.putBoolean("checked", false);
                            cbNotificationStatus.success(args);
                        }
                    }
                });
            }
        }
    }


    private class AlertMessageViewHolder extends RecyclerView.ViewHolder {
        private TextView mMessage;
        private Button mGoToFacebook;

        public AlertMessageViewHolder(View v) {
            super(v);
            mMessage = (TextView) v.findViewById(R.id.alertMessage);
            mGoToFacebook = (Button) v.findViewById(R.id.goToFacebook);
            if(mGoToFacebook!=null){
                mGoToFacebook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cbGoToFacebook.success(new Bundle());
                    }
                });
            }
        }
    }

}
