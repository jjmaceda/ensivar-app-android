package net.clevermobileapps.ensivar.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pixplicity.sharp.Sharp;

import net.clevermobileapps.ensivar.R;

/**
 * @author Vlonjat Gashi (vlonjatg)
 */
public class AppTourMaterialSlideFragment extends Fragment {

    private static final String ARG_DRAWABLE = "drawable";
    private static final String ARG_TITLE = "name";
    private static final String ARG_CONTENT = "content";
    private static final String ARG_TITLE_TEXT_COLOR = "titleTextColor";
    private static final String ARG_CONTENT_TEXT_COLOR = "contentTextColor";

    int drawable;
    String title;
    String content;
    int titleTextColor;
    int contentTextColor;

    RelativeLayout slideRelativeLayout;
    ImageView slideImageView;
    TextView slideTitleTextView;
    TextView slideContentTextView;

    public AppTourMaterialSlideFragment() {
    }

    /**
     * Create Material Slide
     * @param imageDrawable Image resource for the slide
     * @param title String value of the slide name
     * @param content String value of the slide content
     * @param titleTextColor Color value of the name text color
     * @param contentTextColor Color value of the content text color
     * @return Return the created slide
     */
    public static AppTourMaterialSlideFragment newInstance(int imageDrawable, String title, String content, int titleTextColor, int contentTextColor) {

        AppTourMaterialSlideFragment materialSlide = new AppTourMaterialSlideFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_DRAWABLE, imageDrawable);
        args.putString(ARG_TITLE, title);
        args.putString(ARG_CONTENT, content);
        args.putInt(ARG_TITLE_TEXT_COLOR, titleTextColor);
        args.putInt(ARG_CONTENT_TEXT_COLOR, contentTextColor);
        materialSlide.setArguments(args);

        return materialSlide;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().size() != 0) {
            drawable = getArguments().getInt(ARG_DRAWABLE);
            title = getArguments().getString(ARG_TITLE);
            content = getArguments().getString(ARG_CONTENT);
            titleTextColor = getArguments().getInt(ARG_TITLE_TEXT_COLOR);
            contentTextColor = getArguments().getInt(ARG_CONTENT_TEXT_COLOR);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(com.vlonjatg.android.apptourlibrary.R.layout.fragment_material_slide, container, false);

        slideRelativeLayout = (RelativeLayout) rootView.findViewById(com.vlonjatg.android.apptourlibrary.R.id.slideRelativeLayout);
        slideImageView = (ImageView) rootView.findViewById(com.vlonjatg.android.apptourlibrary.R.id.slideImageView);
        slideTitleTextView = (TextView) rootView.findViewById(com.vlonjatg.android.apptourlibrary.R.id.slideTitleTextView);
        slideContentTextView = (TextView) rootView.findViewById(com.vlonjatg.android.apptourlibrary.R.id.slideContentTextView);

        Sharp.loadResource(getResources(), drawable).into(slideImageView);

//        slideImageView.setImageResource(drawable);
        slideTitleTextView.setText(title);
        slideContentTextView.setText(content);

        if (titleTextColor != 0) {
            slideTitleTextView.setTextColor(titleTextColor);
        }

        if (contentTextColor != 0) {
            slideContentTextView.setTextColor(contentTextColor);
        }

        return rootView;
    }
}
