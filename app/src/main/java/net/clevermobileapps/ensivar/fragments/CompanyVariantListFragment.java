package net.clevermobileapps.ensivar.fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;

import com.androiddeveloperlb.listviewvariants.PinnedHeaderListView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.SettingsActivity;
import net.clevermobileapps.ensivar.SweepstakeActivity;
import net.clevermobileapps.ensivar.WebActivity;
import net.clevermobileapps.ensivar.adapters.CompanyVariantListAdapter;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import br.com.customsearchable.SearchActivity;

public class CompanyVariantListFragment extends Fragment {

    private CompanyVariantListAdapter mAdapter;
    private static String TAG = "CompanyVariantListFragment";
    private PinnedHeaderListView mListView;
    private boolean loadingPage = false;
    private boolean allPagesLoaded = false;
    private int currentPage = 0;
    private ArrayList<Company> companyArrayList = new ArrayList<>();
    ParseQuery<Company> query;

    public static CompanyVariantListFragment newInstance(Bundle args) {
        CompanyVariantListFragment fragment = new CompanyVariantListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public CompanyVariantListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (query != null)
            query.cancel();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) return null;

        View view = inflater.inflate(R.layout.fragment_company_variant_list, container, false);

        currentPage = 0;
        loadingPage = false;
        allPagesLoaded = false;

        mListView = (PinnedHeaderListView) view.findViewById(R.id.pinnedListView);
        mAdapter = new CompanyVariantListAdapter(getActivity(), new ArrayList<Company>());
        mListView.setAdapter(mAdapter);

        mAdapter.setPinnedHeaderTextColor(ContextCompat.getColor(getActivity(), R.color.pinned_header_text));
        mListView.setPinnedHeaderView(inflater.inflate(R.layout.pinned_header_listview_side_header, mListView, false));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mListView.setNestedScrollingEnabled(true);
        }

        mListView.setEnableHeaderTransparencyChanges(false);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Company company = mAdapter.getItem(position);
                if(company != null){
                    startActivity(Common.getInstance().createIntentForLocationByCompany(getActivity(), company));
                }
            }
        });

        getCompaniesList(currentPage, view);

        return view;
    }

    private void getCompaniesList(final int page, final View view) {
        if (!allPagesLoaded && !loadingPage) {
            loadingPage = true;

            final ProgressBar pb;
            pb = (ProgressBar) view.findViewById(R.id.pb);
            pb.setVisibility(View.VISIBLE);

            if(query == null) query = ParseQuery.getQuery("Company");

            query.cancel();
            query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
            query.setMaxCacheAge(Constants.MAX_CACHE_TIME);
            query.orderByAscending("name");
            query.setLimit(1000);
            query.setSkip(page * 1000);
            query.whereEqualTo("status", 1);

            query.findInBackground(new FindCallback<Company>() {
                @Override
                public void done(final List<Company> objects, ParseException e) {
                    final int size = objects.size();
                    pb.setVisibility(View.GONE);

                    if (e == null) {
                        if (size > 0) {
                            if (page == 0) {
                                companyArrayList = new ArrayList<>(size);
                            }
                            companyArrayList.addAll(objects);

                            loadingPage = false;
                            currentPage++;
                            getCompaniesList(currentPage, view);
                        } else {
                            loadingPage = false;
                            allPagesLoaded = true;

                            mAdapter = new CompanyVariantListAdapter(getActivity(), companyArrayList);
                            mListView.setOnScrollListener(mAdapter);
                            mListView.setAdapter(mAdapter);
                        }
                    } else {
                        mListView.setOnScrollListener(null);
                        mListView.setAdapter(null);
                    }
                }
            });
        }
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_directory, menu);

        if (getActivity() != null) {
            Common.getInstance().showSweepstakeIfNeeded(getActivity(), new Callback<Boolean>() {
                @Override
                public void success(Boolean isActive) {
                    if (isActive) {
                        MenuItem item = menu.findItem(R.id.action_sweepstake);
                        if (item != null) {
                            item.setVisible(true);
                        }
                    }
                }

                @Override
                public void error(String error) {}
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_suggestion) {
            Intent intent = new Intent(getActivity(), WebActivity.class);
            intent.putExtra("endPoint", WebActivity.SUGGEST_URL);
            intent.putExtra("name", R.string.suggestion_title);
            startActivity(intent);
        }

        if (id == R.id.action_search) {
            Intent intent = new Intent(getActivity(), SearchActivity.class);
            startActivity(intent);

            return true;
        }

        if (id == R.id.action_settings) {
            Intent intent = new Intent(getActivity(), SettingsActivity.class);
            startActivity(intent);

            return true;
        }

        if (id == R.id.action_sweepstake) {
            Intent intent = new Intent(getActivity(), SweepstakeActivity.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
