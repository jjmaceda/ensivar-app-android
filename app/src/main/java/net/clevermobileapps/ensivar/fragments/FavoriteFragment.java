package net.clevermobileapps.ensivar.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import net.clevermobileapps.ensivar.ExplainFavActivity;
import net.clevermobileapps.ensivar.LocationDetailActivity;
import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.adapters.FavoriteAdapter;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.models.Favorite;
import net.clevermobileapps.ensivar.models.Location;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;
import net.clevermobileapps.ensivar.utils.DividerItemDecoration;
import net.clevermobileapps.ensivar.utils.RecyclerViewEmptySupport;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FavoriteFragment extends Fragment {

    private RecyclerViewEmptySupport mListView;
    private FavoriteAdapter favoriteAdapter;
    private ArrayList<Favorite> mFavorites;
    private final String TAG = "FavoriteFragment";
    private ContentLoadingProgressBar pb;
    private LinearLayout detailInstructionWrapper;

    public static FavoriteFragment newInstance() {
        return new FavoriteFragment();
    }

    public FavoriteFragment() {

    }

    @Override
    public void onResume() {
        super.onResume();

        getFavorites(getView());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) return null;

        View view = inflater.inflate(R.layout.fragment_favorite, container, false);

        if(getActivity() != null) {
            Button explain_fav = (Button) view.findViewById(R.id.explain_fav);
            if(explain_fav != null){
                explain_fav.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), ExplainFavActivity.class);
                        startActivity(intent);
                    }
                });

            }

            pb = (ContentLoadingProgressBar) view.findViewById(R.id.pb);
        }

        setupList(view);

        return view;
    }

    private void setupList(View view) {
        mListView = (RecyclerViewEmptySupport) view.findViewById(R.id.favoriteList);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mListView.setLayoutManager(mLayoutManager);
        mListView.addItemDecoration(new DividerItemDecoration(getActivity()));
        mListView.setEmptyView(view.findViewById(R.id.no_result));
    }

    private void getFavorites(final View view) {
        mListView = (RecyclerViewEmptySupport) view.findViewById(R.id.favoriteList);
        detailInstructionWrapper = (LinearLayout) view.findViewById(R.id.detail_instruction);

        if (Common.isUserLoggedIn()) {
            ParseQuery<Favorite> favoriteParseQuery = ParseQuery.getQuery("Favorite");
            favoriteParseQuery.addAscendingOrder("name");
            favoriteParseQuery.whereEqualTo("user", ParseUser.getCurrentUser());
            favoriteParseQuery.setCachePolicy(ParseQuery.CachePolicy.CACHE_THEN_NETWORK);

            pb.setVisibility(View.VISIBLE);

            favoriteParseQuery.findInBackground(new FindCallback<Favorite>() {
                @Override
                public void done(final List<Favorite> list, ParseException e) {
                    pb.setVisibility(View.GONE);

                    if (e == null) {
                        mFavorites = (ArrayList<Favorite>) list;

                        if (mFavorites.size() > 0) {
                            favoriteAdapter = new FavoriteAdapter(mFavorites, new Callback<Integer>() {
                                @Override
                                public void success(Integer position) {
                                    goToLocationDetail(position);
                                }

                                @Override
                                public void error(String error) {

                                }
                            });
                            mListView.setAdapter(favoriteAdapter);

                            detailInstructionWrapper.setVisibility(View.VISIBLE);
                        } else {
                            mListView.setAdapter(null);
                            detailInstructionWrapper.setVisibility(View.GONE);
                        }
                    }
                }
            });

        } else {
            mFavorites = createFavoritesList();

            if (mFavorites.size() > 0) {
                favoriteAdapter = new FavoriteAdapter(mFavorites, new Callback<Integer>() {
                    @Override
                    public void success(Integer position) {
                        goToLocationDetail(position);
                    }

                    @Override
                    public void error(String error) {

                    }
                });
                mListView.setAdapter(favoriteAdapter);

                detailInstructionWrapper.setVisibility(View.VISIBLE);
            } else {
                mListView.setAdapter(null);
                detailInstructionWrapper.setVisibility(View.GONE);
            }
        }
    }


    private ArrayList<Favorite> createFavoritesList() {
        SharedPreferences preferences = getActivity().getSharedPreferences(Constants.FAVORITES_PREFERENCE, AppCompatActivity.MODE_PRIVATE);
        Map<String, ?> map = preferences.getAll();
        ArrayList<Favorite> list = new ArrayList<>();

        for (Map.Entry<String, ?> entry : map.entrySet()) {
            Favorite favorite = new Favorite();
            favorite.setIdLocation(entry.getKey());
            favorite.setName((String) entry.getValue());

            if (Common.isUserLoggedIn()) {
                favorite.setUser(ParseUser.getCurrentUser());
            }
            list.add(favorite);
        }

        return list;
    }

    private void goToLocationDetail(Object o) {
        final int position = (int) o;
        final Favorite favorite = favoriteAdapter.getItem(position);

        ParseQuery<Location> query = ParseQuery.getQuery("Location");
        query.include("company");
        query.whereEqualTo("status", "1");
        query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
        query.setMaxCacheAge(Constants.MAX_CACHE_TIME);

        pb.setVisibility(View.VISIBLE);
        query.getInBackground(favorite.getIdLocation(), new GetCallback<Location>() {
            //TODO if no records, the fav is not active anymore, handle that
            @Override
            public void done(Location location, ParseException e) {
               pb.setVisibility(View.GONE);

                if (e == null) {
                    Intent intent = new Intent(getActivity(), LocationDetailActivity.class);
                    intent.putExtra("name", favorite.getName());
                    intent.putExtra("phone", location.getPhone());
                    intent.putExtra("address", location.getAddress());
                    intent.putExtra("geo", location.getGeo());
                    intent.putExtra("verified", location.getVerified());
                    intent.putExtra("id", location.getObjectId());
                    intent.putExtra("company_name", location.getCompany().getName());
                    if (location.getDateVerified() != null) {
                        intent.putExtra("date_verify", location.getDateVerified().getTime());
                    }

                    startActivity(intent);
                }
            }
        });
    }

}
