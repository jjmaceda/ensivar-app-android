package net.clevermobileapps.ensivar.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import net.clevermobileapps.ensivar.EnsivarApplication;
import net.clevermobileapps.ensivar.LocationDetailActivity;
import net.clevermobileapps.ensivar.LocationsMapActivity;
import net.clevermobileapps.ensivar.NotificationActivity;
import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.adapters.LocationAdapter;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.interfaces.LocationType;
import net.clevermobileapps.ensivar.models.Location;
import net.clevermobileapps.ensivar.utils.Constants;
import net.clevermobileapps.ensivar.utils.DividerItemDecoration;
import net.clevermobileapps.ensivar.utils.RecyclerViewEmptySupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;

public class LocationByCompanyFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String TAG = "LocationByCompanyFrag";

    private String bankCategoryId = "VZEkDvcwbo";
    private String currentFilter = "ATM";
    private String id_company;
    private String company_name;
    private String main_phone;
    private String categoryId;
    private boolean isSponsor = false;
    private ArrayList<String> predominantColor;
    private boolean nearIsActive = false;
    private boolean wasFiltersSetup = false;
    private List<Location> locations = new ArrayList<>();

    private static int VIEW_LOCATIONS_ON_MAP = 1, NOTIFICATION_SETTINGS = 2;
    private SharedPreferences preferences;
    private MenuItem notificationSettings = null;

    private ContentLoadingProgressBar pb;
    private RecyclerViewEmptySupport mRecyclerView;
    private LocationAdapter mAdapter;

    private Tracker mTracker;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    public static LocationByCompanyFragment newInstance(Bundle args) {
        LocationByCompanyFragment fragment = new LocationByCompanyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public LocationByCompanyFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
        if (container == null) return null;

        View view = inflater.inflate(R.layout.fragment_locations_by_company, container, false);

        EnsivarApplication application = (EnsivarApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        Bundle bundle = getArguments();
        id_company = bundle.getString("id_company");
        company_name = bundle.getString("company_name");
        main_phone = bundle.getString("main_phone");
        categoryId = bundle.getString("categoryId");
        predominantColor = bundle.getStringArrayList("predominant_color");
        isSponsor = bundle.getBoolean("sponsor");
        preferences = getActivity().getSharedPreferences(NotificationActivity
                .NOTIFICATION_SETTINGS_FILE_NAME, Context.MODE_PRIVATE);

        createGoogleClient();
        pb = (ContentLoadingProgressBar) view.findViewById(R.id.pb);
        prepareLocationRecycledView(view);
        getLocations(view);

        return view;
    }

    //handle location geo
    @Override
    public void onStart() {
        super.onStart();

        //Added
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        //Added
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        //TODO need to add the company id or name
        mTracker.setScreenName(Constants.LOCATION_BY_COMPANY_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        //Added
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }

        if (notificationSettings != null) {
            changeNotificationIcon();
        }
    }

    //handle menu

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        MenuItem viewLocationsOnMap = menu.add(0, VIEW_LOCATIONS_ON_MAP, 0, R.string
                .view_locations);
        viewLocationsOnMap.setIcon(R.drawable.ic_place_white_24dp);
        viewLocationsOnMap.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        if (isSponsor) {
            notificationSettings = menu.add(0, NOTIFICATION_SETTINGS, 0, R.string
                    .openNotificationSettings);
            changeNotificationIcon();
            notificationSettings.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }

        inflater.inflate(R.menu.menu_locations_by_company, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == VIEW_LOCATIONS_ON_MAP) {
            final Intent intent = new Intent(getActivity(), LocationsMapActivity.class);
            final ArrayList<HashMap<String, ?>> locations = new ArrayList<>();
            for (LocationType loc : mAdapter.getItems()) {
                if (loc instanceof Location) {
                    final Location location = (Location) loc;
                    final HashMap<String, String> map = new HashMap<>();
                    final LatLng geo = location.getGeo();
                    map.put("name", location.getName());
                    map.put("phone", location.getPhone());
                    map.put("lat", Double.toString(geo.latitude));
                    map.put("lng", Double.toString(geo.longitude));
                    locations.add(map);
                }
            }
            intent.putExtra("locations", locations);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        } else if (id == NOTIFICATION_SETTINGS) {
            Intent intent = new Intent(getActivity(), NotificationActivity.class);
            startActivity(intent);
        } else if (id == android.R.id.home) {
            //getActivity().onBackPressed();
            //return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void changeNotificationIcon() {
        if (!preferences.contains(id_company)) {
            notificationSettings.setIcon(R.drawable.ic_notifications_active_white_24dp);
        } else {
            notificationSettings.setIcon(R.drawable.ic_notifications_off_white_24dp);
        }
    }

    private void prepareLocationRecycledView(View view) {
        mRecyclerView = (RecyclerViewEmptySupport) view.findViewById(R.id.my_recycler_view);

        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        mRecyclerView.setEmptyView(view.findViewById(R.id.no_result));
    }

    private void createGoogleClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API).build();
        }
    }

    //handle locationList
    private void getLocations() {
        getLocations(getView());
    }

    private void doDial() {
        if (!main_phone.isEmpty()) {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + main_phone));
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            if (callIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(callIntent);
            } else {
                //TODO alert there is no map app
                Toast.makeText(getActivity(), getString(R.string.no_dial), Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

    private LocationAdapter getAdapter(ArrayList<LocationType> locationsType, @Nullable android
            .location.Location location) {
        return new LocationAdapter(getActivity(), new Callback<Integer>() {
            @Override
            public void success(Integer position) {
                Location location = (Location) mAdapter.getItem(position);

                Intent intent = new Intent(getActivity(), LocationDetailActivity.class);
                intent.putExtra("name", location.getName());
                intent.putExtra("phone", location.getPhone());
                intent.putExtra("address", location.getAddress());
                intent.putExtra("geo", location.getGeo());
                intent.putExtra("verified", location.getVerified());
                intent.putExtra("id", location.getObjectId());
                intent.putExtra("company_name", company_name);
                if (location.getDateVerified() != null) {
                    intent.putExtra("date_verify", location.getDateVerified().getTime());
                }

                startActivity(intent);
            }

            @Override
            public void error(String error) {

            }
        }, new Callback<String>() {
            @Override
            public void success(String s) {
                doDial();
            }

            @Override
            public void error(String error) {

            }
        }, new Callback<Boolean>() {
            @Override
            public void success(Boolean aBoolean) {
                if (aBoolean) {
                    nearIsActive = true;
                    filterNearOnly();
                } else {
                    nearIsActive = false;
                    getLocations();
                }
            }

            @Override
            public void error(String error) {

            }
        }, new Callback<Integer>() {
            @Override
            public void success(Integer option) {
                if (option == 0) {
                    currentFilter = "ATM";
                } else {
                    currentFilter = "BANCO";
                }

                if (nearIsActive) {
                    filterNearOnly();
                } else {
                    getLocations();
                }
            }

            @Override
            public void error(String error) {

            }
        }, location, currentFilter, locationsType);
    }

    private void getLocations(View view) {
        ParseQuery<Location> query = ParseQuery.getQuery("Location");
        query.addAscendingOrder("name");
        query.whereEqualTo("company", ParseObject.createWithoutData("Company", id_company));
        if (checkIfIsBank()) {
            query.whereEqualTo("options", currentFilter);
        }
        query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
        query.setMaxCacheAge(Constants.MAX_CACHE_TIME);

        pb.setVisibility(View.VISIBLE);
        query.findInBackground(new FindCallback<Location>() {
            @Override
            public void done(final List<Location> objects, ParseException e) {
                pb.setVisibility(View.GONE);

                if (e == null) {
                    locations = objects;

                    ArrayList<LocationType> locationsType = new ArrayList<>();
                    locationsType.add(new Location.Info(predominantColor, checkIfIsBank()));

                    //configureFilterIfNeeded();

                    for (Location loc : locations) {
                        locationsType.add(loc);
                    }

                    mAdapter = getAdapter(locationsType, null);
                    mRecyclerView.setAdapter(mAdapter);

                    if (locations.size() == 0) {
                        Toast.makeText(getActivity(), getResources().getString(R.string
                                .no_result_locations), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void doFilterNearMe(final android.location.Location location) {
        final ParseGeoPoint geo = new ParseGeoPoint(location.getLatitude(), location.getLongitude
                ());

        ParseQuery<Location> query = ParseQuery.getQuery("Location");
        query.whereEqualTo("company", ParseObject.createWithoutData("Company", id_company));
        query.whereWithinKilometers("geo", geo, 5);
        if (checkIfIsBank()) {
            query.whereEqualTo("options", currentFilter);
        }
        query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
        query.setMaxCacheAge(Constants.MAX_CACHE_TIME);

        pb.setVisibility(View.VISIBLE);
        query.findInBackground(new FindCallback<Location>() {
            @Override
            public void done(final List<Location> objects, ParseException e) {
                pb.setVisibility(View.GONE);

                if (e == null) {
                    locations = objects;

                    ArrayList<LocationType> locationsType = new ArrayList<>();
                    locationsType.add(new Location.Info(predominantColor, checkIfIsBank()));

                    for (Location loc : locations) {
                        locationsType.add(loc);
                    }

                    mAdapter = getAdapter(locationsType, location);
                    mRecyclerView.setAdapter(mAdapter);

                    if (locations.size() == 0) {
                        Toast.makeText(getActivity(), getResources().getString(R.string
                                .no_result_locations), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void filterNearOnly() {
        getLastLocation();
    }

    private boolean checkIfIsBank() {
        return categoryId.equals(bankCategoryId);
    }

    private void configureFilterIfNeeded() {
        if (!wasFiltersSetup && checkIfIsBank()) {
            wasFiltersSetup = true;
            if (getView() != null) {
                final SegmentedGroup segmentedGroup = (SegmentedGroup) getView().findViewById(R
                        .id.filterForBank);

                if (segmentedGroup != null) {
                    segmentedGroup.setVisibility(View.VISIBLE);

                    if (predominantColor != null && predominantColor.size() > 0) {
                        segmentedGroup.setTintColor(Color.parseColor(predominantColor.get(0)),
                                Color.parseColor("#FFFFFFFF"));
                    }

                    segmentedGroup.check(R.id.filterForBankATMBtn);
                    segmentedGroup.setOnCheckedChangeListener(new RadioGroup
                            .OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup radioGroup, int i) {
                            int radioButtonID = segmentedGroup.getCheckedRadioButtonId();
                            View radioButton = segmentedGroup.findViewById(radioButtonID);
                            int idx = segmentedGroup.indexOfChild(radioButton);
                            if (idx == 0) {
                                currentFilter = "ATM";
                            } else {
                                currentFilter = "BANCO";
                            }

                            if (nearIsActive) {
                                filterNearOnly();
                            } else {
                                getLocations();
                            }
                        }
                    });
                }
            }
        }
    }


    private void createLocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)        // 10 seconds, in milliseconds
                .setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS); // 1 second, in
        // milliseconds
    }

    public void getLastLocation() {
        if (mGoogleApiClient.isConnected()) {
            android.location.Location location = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);

            if (location != null) {
                handleNewLocation(location);
            } else {
                startLocationUpdates();
            }
        } else {
            Toast.makeText(getActivity(), getString(R.string.no_location), Toast.LENGTH_LONG)
                    .show();
        }

    }

    private void handleNewLocation(android.location.Location location) {
        doFilterNearMe(location);
    }

    public void startLocationUpdates() {
        createLocationRequest();
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
        );
    }

    @Override
    public void onConnected(@Nullable Bundle connectionHint) {
        Log.d(TAG, "onConnected");
    }

    //Added
    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended");
    }

    //Added
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed with error code " + connectionResult.getErrorCode());
    }

    @Override
    public void onLocationChanged(android.location.Location location) {
        Log.d(TAG, "onLocationChanged");
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        handleNewLocation(location);
    }


}
