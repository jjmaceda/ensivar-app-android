package net.clevermobileapps.ensivar.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import net.clevermobileapps.ensivar.BarcodeCaptureActivity;
import net.clevermobileapps.ensivar.CatalogByCompanyActivity;
import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.adapters.CompanyProductAdapter;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.interfaces.RecycleViewRowVariant;
import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.models.Point;
import net.clevermobileapps.ensivar.models.Profile;
import net.clevermobileapps.ensivar.models.PromoCoupon;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;
import net.clevermobileapps.ensivar.utils.RecyclerViewEmptySupport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PassbookFragment extends Fragment {

    private static final String TAG = "PassbookFragment";
    private ArrayList<RecycleViewRowVariant> mList = new ArrayList<>();
    private RecyclerViewEmptySupport mListView;

    private boolean couponsLoaded = false;
    private boolean pointsLoaded = false;

    private ProgressBar pb;
    private ProgressDialog progressDialog;

    private JSONArray couponsArray = new JSONArray();
    private JSONArray pointsArray = new JSONArray();

    private Profile mProfile;
    private PromoCoupon currentPromoCoupon;

    private CompanyProductAdapter companyProductAdapter;
    private ParseQuery<PromoCoupon> couponQuery;
    private ParseQuery<Company> companyQuery;
    private ParseQuery<Profile> profileQuery;
    private LinearLayout no_result;
    private boolean needRefresh = false;

    public static PassbookFragment newInstance() {
        return new PassbookFragment();
    }

    public PassbookFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) return null;
        View view = inflater.inflate(R.layout.fragment_passbook, container, false);

        progressDialog = Common.getInstance().getProgressDialog(getActivity());

        pb = (ProgressBar) view.findViewById(R.id.pb);

        no_result = (LinearLayout) view.findViewById(R.id.no_result);
        mListView = (RecyclerViewEmptySupport) view.findViewById(R.id.passbookList);
        if(mListView != null){
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            mListView.setLayoutManager(mLayoutManager);
            mListView.setEmptyView(no_result);
        }
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {

                if (data != null) {
                    needRefresh = false;
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    if(currentPromoCoupon != null && !barcode.displayValue.isEmpty()){
                        verifyLocation(barcode.displayValue);
                    }
                } else {
                    needRefresh = true;
                    Log.d(TAG, "No barcode captured, intent data is null");
                }
            } else {
                needRefresh = true;
                Log.d(TAG, String.format(getString(R.string.barcode_error),
                        CommonStatusCodes.getStatusCodeString(resultCode)));
            }
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if(!needRefresh){
            pointsLoaded = false;
            couponsLoaded = false;
            needRefresh = false;

            mList = new ArrayList<>();
            mListView.setAdapter(null);

            pb.setVisibility(View.VISIBLE);
            no_result.setVisibility(View.GONE);

            loadProfile();
        }else{
            needRefresh = false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if(couponQuery != null){
            couponQuery.cancel();
        }
        if(companyQuery != null){
            companyQuery.cancel();
        }

    }

    private void loadProfile(){

        if(!Common.getInstance().isInternetAvailable(getActivity())){
            pb.setVisibility(View.GONE);
            no_result.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), getResources().getString(R.string.no_connection), Toast.LENGTH_LONG).show();;
            return;
        }

        Common.getInstance().getUserProfile(new Callback<Profile>() {
            @Override
            public void success(Profile profile) {
                mProfile = profile;
                couponsArray = profile.getCoupons();
                pointsArray = profile.getPoints();

                loadPoints();
                loadCoupons();
            }

            @Override
            public void error(String error) {
                pb.setVisibility(View.GONE);
                mListView.setAdapter(null);
            }
        });

    }


    private void loadPoints(){
        ArrayList<String> companyArray = new ArrayList<>();
        final ArrayList<JSONObject> mProfilePoints = new ArrayList<>();

        if(pointsArray != null){
            int len = pointsArray.length();

            for (int i=0;i<len;i++){
                try {
                    JSONObject obj = (JSONObject) pointsArray.get(i);
                    String objectId = obj.getString("id_company");

                    companyArray.add(objectId);
                    mProfilePoints.add(obj);

                }catch (JSONException e1){
                    e1.printStackTrace();
                }
            }
        }

        if(companyQuery == null){
            companyQuery = ParseQuery.getQuery("Company");
        }
        companyQuery.cancel();
        companyQuery.whereContainedIn("objectId", companyArray);
        companyQuery.findInBackground(new FindCallback<Company>() {
            @Override
            public void done(List<Company> companyList, ParseException e) {
                if(e==null){
                    for(Company company : companyList){
                        int points = 0;
                        for(JSONObject pointItem : mProfilePoints){
                            try {
                                String objectId = pointItem.getString("id_company");
                                int objectPoints = pointItem.getInt("points");

                                if(objectId.contentEquals(company.getObjectId())){
                                    points = objectPoints;
                                    break;
                                }
                            }catch (JSONException e1){
                                e1.printStackTrace();
                            }
                        }
                        if(points > 0){
                            mList.add(new Point(company.getObjectId(), company.getName(), company.getLogo(), points, company.getPredominantColor(),company));
                        }
                    }
                }
                pointsLoaded = true;
                setAdapter(mList);

            }
        });

    }

    private void loadCoupons(){
        ArrayList<String> couponsKeys = new ArrayList<>();
        final ArrayList<JSONObject> mProfileCoupons = new ArrayList<>();

        if(couponsArray != null){
            int len = couponsArray.length();
            for (int i=0;i<len;i++){
                try {
                    JSONObject obj = (JSONObject) couponsArray.get(i);
                    String objectId = obj.getString("objectId");

                    couponsKeys.add(objectId);
                    mProfileCoupons.add(obj);
                }catch (JSONException e1){
                    e1.printStackTrace();
                }
            }
        }

        if(couponQuery == null){
            couponQuery = ParseQuery.getQuery("PromoCoupon");
        }
        couponQuery.cancel();
        //couponQuery.whereEqualTo("active", true);
        couponQuery.whereContainedIn("objectId", couponsKeys);
        couponQuery.include("company");
        couponQuery.findInBackground(new FindCallback<PromoCoupon>() {
            @Override
            public void done(List<PromoCoupon> promoCouponList, ParseException e) {
                if(e == null){
                    for(PromoCoupon promoCoupon : promoCouponList){
                        for(JSONObject couponItem : mProfileCoupons){
                            try {
                                String objectId = couponItem.getString("objectId");
                                boolean isRedeem = couponItem.getBoolean("redeemed");
                                String validUntil = couponItem.getString("valid_until");

                                if(objectId.contentEquals(promoCoupon.getObjectId())){
                                    promoCoupon.setIsRedeem(isRedeem);
                                    if(promoCoupon.getFeatured()){
                                        Date validUntilObject = Common.convertStringDateToDateObject(validUntil);
                                        promoCoupon.setValidUntil(validUntilObject);
                                    }
                                    break;
                                }
                            }catch (JSONException e1){
                                e1.printStackTrace();
                            }
                        }
                        mList.add(promoCoupon);
                    }
                }
                couponsLoaded = true;
                setAdapter(mList);
            }
        });
    }


    private void setAdapter(ArrayList<RecycleViewRowVariant> mList){
        if(couponsLoaded && pointsLoaded){
            if (mList.size() > 0) {
                companyProductAdapter = new CompanyProductAdapter(mList, getActivity());

                //POINT LOGO CLICK EVENT
                companyProductAdapter.LogoPointCallbackListener(new Callback<Point>() {
                    @Override
                    public void success(Point point) {
                        goToLocationsByCompany(point.company);
                    }

                    @Override
                    public void error(String error) {

                    }
                });

                //POINT BTN REDEEM CLICK EVENT
                companyProductAdapter.BtnRedeemCallbackListener(new Callback<Point>() {
                    @Override
                    public void success(Point point) {
                        Intent intent = new Intent(getActivity(), CatalogByCompanyActivity.class);
                        intent.putExtra("id_company", point.id_company);
                        intent.putExtra("company_name", point.name);
                        intent.putExtra(CompanyProductAdapter.BTN_KEY, CompanyProductAdapter.BTN_REDEEM);
                        startActivity(intent);
                    }

                    @Override
                    public void error(String error) {

                    }
                });
                //POINT BTN ACCUMULATE CLICK EVENT
                companyProductAdapter.BtnAccumulateCallbackListener(new Callback<Point>() {
                    @Override
                    public void success(Point point) {
                        Intent intent = new Intent(getActivity(), CatalogByCompanyActivity.class);
                        intent.putExtra("id_company", point.id_company);
                        intent.putExtra("company_name", point.name);
                        intent.putExtra(CompanyProductAdapter.BTN_KEY, CompanyProductAdapter.BTN_ACCUMULATE);
                        startActivity(intent);
                    }

                    @Override
                    public void error(String error) {

                    }
                });

                //LOGO COUPON CLICK EVENT
                companyProductAdapter.LogoPromoCouponCallbackListener(new Callback<PromoCoupon>() {
                    @Override
                    public void success(PromoCoupon promoCoupon) {
                        Company company = (Company) promoCoupon.getCompany();
                        if(company != null){
                            goToLocationsByCompany(company);
                        }
                    }

                    @Override
                    public void error(String error) {

                    }
                });

                //COUPON BTN REDEEM CLICK EVENT
                companyProductAdapter.BtnCouponCallbackListener(new Callback<PromoCoupon>() {
                    @Override
                    public void success(PromoCoupon promoCoupon) {
                        if(Constants.today.before(promoCoupon.getValidUntil())){
                            if(!promoCoupon.getIsRedeem()){
                                currentPromoCoupon = promoCoupon;
                                Intent intent = new Intent(getActivity(), BarcodeCaptureActivity.class);
                                startActivityForResult(intent, Constants.RC_BARCODE_CAPTURE);
                            }else{
                                Toast.makeText(getActivity(), getResources().getString(R.string.couponIsRedeemedError), Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(getActivity(), getResources().getString(R.string.couponValidUntilError), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void error(String error) {

                    }
                });

                //COUPON BTN REMOVE CLICK EVENT
                companyProductAdapter.BtnRemoveCallbackListener(new Callback<Bundle>() {
                    @Override
                    public void success(Bundle bundle) {
                        createRemoveAlertDialog(bundle);
                    }

                    @Override
                    public void error(String error) {

                    }
                });

                mListView.setAdapter(companyProductAdapter);
            }else{
                mListView.setAdapter(null);
            }
            pb.setVisibility(View.GONE);
        }
    }

    private void verifyLocation(final String barcodeValue){
        progressDialog.show();

        try {
            JSONObject jsonObject =  new JSONObject(barcodeValue);
            final String id_location = jsonObject.getString("id_location");
            String id_company = currentPromoCoupon.getCompany().getObjectId();

            Common.getInstance().verifyAuthenticityOfLocation(id_company, id_location, new Callback<Boolean>() {
                @Override
                public void success(Boolean aBoolean) {
                    redeemCurrentCoupon(id_location);
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), getResources().getString(R.string.locationIsNotOfCompany), Toast.LENGTH_LONG).show();
                }
            });

        }catch (JSONException e){
            progressDialog.dismiss();
            Toast.makeText(getActivity(), getResources().getString(R.string.locationIsNotOfCompany), Toast.LENGTH_LONG).show();
        }
    }

    private void redeemCurrentCoupon(final String id_location){
        boolean isRedeemed = false;

        if(couponsArray != null){
            int len = couponsArray.length();
            for (int i=0;i<len;i++){
                try {
                    JSONObject obj = (JSONObject) couponsArray.get(i);

                    if(obj.getString("objectId").contentEquals(currentPromoCoupon.getObjectId())){
                        if(!obj.getBoolean("redeemed")){
                            obj.put("redeemed", true);
                        }else{
                            isRedeemed = true;
                        }
                        break;
                    }
                }catch (JSONException e1){
                    e1.printStackTrace();
                }
            }
        }

        if(!isRedeemed){
            mProfile.put("coupons", couponsArray);
            mProfile.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if(e == null){
                        saveCouponStats(currentPromoCoupon.getCompany().getObjectId(), id_location);

                        currentPromoCoupon.setIsRedeem(true);
                        companyProductAdapter.notifyDataSetChanged();
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), getResources().getString(R.string.couponWasRedeemedSuccessfully), Toast.LENGTH_LONG).show();
                    }else{
                        progressDialog.dismiss();
                    }
                }
            });
        }else{
            progressDialog.dismiss();
            Toast.makeText(getActivity(), getResources().getString(R.string.couponIsRedeemedError), Toast.LENGTH_LONG).show();
        }

    }

    private void removeCoupon(final Bundle bundle){
        progressDialog.show();
        JSONArray newCoupons = new JSONArray();
        if(couponsArray != null){
            String promoCouponObjectId = bundle.getString("objectId", "");
            int len = couponsArray.length();
            for (int i=0;i<len;i++){
                try {
                    JSONObject obj = (JSONObject) couponsArray.get(i);
                    if(!obj.getString("objectId").contentEquals(promoCouponObjectId)){
                        newCoupons.put(obj);
                    }
                }catch (JSONException e1){
                    e1.printStackTrace();
                }
            }
        }

        mProfile.put("coupons", newCoupons);
        mProfile.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null){
                    companyProductAdapter.removeItem(bundle.getInt("position"));
                    companyProductAdapter.notifyDataSetChanged();
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), getResources().getString(R.string.couponRemovedSuccessfully), Toast.LENGTH_SHORT).show();
                }else{
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), getResources().getString(R.string.removeCouponError), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    private void createRemoveAlertDialog(final Bundle bundle){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        String alertMessage= getResources().getString(R.string.removeCouponQuestion);
        builder.setMessage(alertMessage);
        builder.setNegativeButton(R.string.general_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                removeCoupon(bundle);
            }
        });

        AlertDialog select_general_dialog;
        select_general_dialog = builder.create();
        select_general_dialog.show();
    }

    private void goToLocationsByCompany(Company company){
        if(company != null){
            startActivity(Common.getInstance().createIntentForLocationByCompany(getActivity(), company));
        }
    }

    public void saveCouponStats(String id_company, String id_location){
        ParseObject stat = new ParseObject("Stat");
        stat.put("company", ParseObject.createWithoutData("Company", id_company));
        stat.put("location", ParseObject.createWithoutData("Location", id_location));
        stat.put("user", ParseUser.getCurrentUser());
        stat.put("promocoupon", ParseObject.createWithoutData("PromoCoupon", currentPromoCoupon.getObjectId()));

        stat.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {}
        });
    }

}
