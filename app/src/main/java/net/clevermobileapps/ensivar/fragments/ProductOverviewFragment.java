package net.clevermobileapps.ensivar.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.clevermobileapps.ensivar.CatalogByCompanyActivity;
import net.clevermobileapps.ensivar.CouponsByCompanyActivity;
import net.clevermobileapps.ensivar.EnsivarApplication;
import net.clevermobileapps.ensivar.NotificationActivity;
import net.clevermobileapps.ensivar.PromotionsByCompanyActivity;
import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.adapters.CompanyProductAdapter;
import net.clevermobileapps.ensivar.adapters.CompanyProductOverviewAdapter;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.models.CompanyInfo;
import net.clevermobileapps.ensivar.models.CompanyOverview;
import net.clevermobileapps.ensivar.models.PromoCoupon;
import net.clevermobileapps.ensivar.models.Separator;
import net.clevermobileapps.ensivar.models.Social;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;
import net.clevermobileapps.ensivar.utils.RecyclerViewEmptySupport;
import net.clevermobileapps.ensivar.utils.SocialDialog;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class ProductOverviewFragment extends Fragment {

    private RecyclerViewEmptySupport mListView;
    private RecyclerView.LayoutManager mLayoutManager;
    private ProgressBar pb;
    private ProgressDialog progressDialog;
    private String id_company;
    private boolean isSponsor = false;
    private String company_name;
    private SharedPreferences preferences;
    private MenuItem notificationSettings = null;
    private static int NOTIFICATION_SETTINGS = 1;

    private Tracker mTracker;

    public static ProductOverviewFragment newInstance(Bundle args) {
        ProductOverviewFragment fragment = new ProductOverviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ProductOverviewFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) return null;

        View view = inflater.inflate(R.layout.fragment_product_overview, container, false);

        EnsivarApplication application = (EnsivarApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        progressDialog = Common.getInstance().getProgressDialog(getActivity());

        Bundle arguments = getArguments();
        String web = arguments.getString("web");
        String email = arguments.getString("email");
        id_company = arguments.getString("id_company");
        isSponsor = arguments.getBoolean("sponsor");
        company_name = arguments.getString("company_name");
        ArrayList<Parcelable> socialList =  arguments.getParcelableArrayList("social");
        preferences = getActivity().getSharedPreferences(NotificationActivity.NOTIFICATION_SETTINGS_FILE_NAME, Context.MODE_PRIVATE);

        mListView = (RecyclerViewEmptySupport) view.findViewById(R.id.productList);
        mLayoutManager = new LinearLayoutManager(getContext());
        mListView.setLayoutManager(mLayoutManager);
        mListView.setEmptyView(view.findViewById(R.id.no_result));

        pb = (ProgressBar) view.findViewById(R.id.pb);
        pb.setVisibility(View.VISIBLE);

        List<Object> mList = new ArrayList<>();

        boolean needSeparator = false;
        if(web != null && !web.isEmpty()){
            needSeparator = true;
        }
        if(email != null && !email.isEmpty()){
            needSeparator = true;
        }
        if(socialList != null && socialList.size() > 0){
            needSeparator = true;
        }

        mList.add(new Separator());
        mList.add(new CompanyOverview(getResources().getString(R.string.product_overview_promotion_label), Constants.COMPANY_PROMOTION));
        mList.add(new CompanyOverview(getResources().getString(R.string.product_overview_coupon_label), Constants.COMPANY_COUPON));
        mList.add(new CompanyOverview(getResources().getString(R.string.product_overview_point_label), Constants.COMPANY_POINT));

        if(needSeparator){
            mList.add(new Separator());
        }

        if(web != null && !web.isEmpty()){
            Bundle bundle1 = new Bundle();
            bundle1.putString("args",web);
            mList.add(new CompanyInfo(Constants.COMPANY_WEB,bundle1));
        }

        if(email != null && !email.isEmpty()){
            Bundle bundle2 = new Bundle();
            bundle2.putString("args",email);
            mList.add(new CompanyInfo(Constants.COMPANY_EMAIL, bundle2));
        }

        if(socialList != null && socialList.size() > 0){
            Bundle bundle3 = new Bundle();
            bundle3.putParcelableArrayList("args", socialList);
            mList.add(new CompanyInfo(Constants.COMPANY_SOCIAL, bundle3));
        }

        setAdapter(mList);

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        if(notificationSettings != null){
            changeNotificationIcon();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        if(isSponsor){
            notificationSettings = menu.add(0, NOTIFICATION_SETTINGS, 0, R.string.openNotificationSettings);
            changeNotificationIcon();
            notificationSettings.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {

        }else if(id == NOTIFICATION_SETTINGS){
            Intent intent = new Intent(getActivity(), NotificationActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void changeNotificationIcon(){
        if(!preferences.contains(id_company)){
            notificationSettings.setIcon(R.drawable.ic_notifications_active_white_24dp);
        }else{
            notificationSettings.setIcon(R.drawable.ic_notifications_off_white_24dp);
        }
    }


    private void setAdapter(List<Object> mList){
        if (mList.size() > 0) {
            CompanyProductOverviewAdapter companyProductOverviewAdapter =
                    new CompanyProductOverviewAdapter(mList, getActivity(), new Callback<CompanyInfo>() {
                @Override
                public void success(CompanyInfo companyInfo) {
                    if(companyInfo != null){
                        Bundle args = companyInfo.args;
                        switch (companyInfo.name){
                            case Constants.COMPANY_WEB:
                                createAlertDialog(companyInfo.name, R.string.general_web_message, args.getString("args"));
                                break;
                            case Constants.COMPANY_EMAIL:
                                createAlertDialog(companyInfo.name, R.string.general_email_message, args.getString("args"));
                                break;
                            case Constants.COMPANY_SOCIAL:
                                ArrayList<Social> socialArrayList = args.getParcelableArrayList("args");

                                if(socialArrayList != null && socialArrayList.size() > 0){
                                    Bundle bundle = new Bundle();
                                    bundle.putParcelableArrayList("social", socialArrayList);

                                    SocialDialog socialDialog = SocialDialog.newInstance(bundle);
                                    socialDialog.show(getActivity().getFragmentManager(), "social");
                                }

                                break;
                        }
                    }
                }

                @Override
                public void error(String error) {

                }
            }, new Callback<CompanyOverview>() {
                @Override
                public void success(CompanyOverview companyProduct) {
                    switch (companyProduct.type){
                        case Constants.COMPANY_COUPON:
                            companyHasCoupons();
                            break;
                        case Constants.COMPANY_PROMOTION:
                            companyHasPromotions();
                            break;
                        case Constants.COMPANY_POINT:
                            companyHasCatalog();
                            break;
                    }
                }

                @Override
                public void error(String error) {

                }
            });
            mListView.setAdapter(companyProductOverviewAdapter);
        } else {
            mListView.setAdapter(null);
        }

        pb.setVisibility(View.GONE);
    }

    private void companyHasCatalog(){
        progressDialog.show();
        Common.getInstance().getCompany(id_company, new Callback<Company>() {
            @Override
            public void success(Company company) {
                progressDialog.hide();
                JSONArray catalog = company.getLoyaltyCatalog();
                if(catalog!=null && catalog.length() > 0){
                    Intent intent = new Intent(getActivity(), CatalogByCompanyActivity.class);
                    intent.putExtra("id_company", id_company);
                    intent.putExtra("company_name", company_name);
                    intent.putExtra(CompanyProductAdapter.BTN_KEY, CompanyProductAdapter.BTN_ACCUMULATE);
                    startActivity(intent);
                }else{
                    createCompanyNotSponsoredAlertDialog(R.string.companyNotHavePoints);
                }
            }

            @Override
            public void error(String error) {
                progressDialog.hide();
                createCompanyNotSponsoredAlertDialog(R.string.companyNotHavePoints);
            }
        });
    }

    private void companyHasCoupons(){
        progressDialog.show();
        Common.getInstance().getCoupons(id_company, new Callback<List<PromoCoupon>>() {
            @Override
            public void success(List<PromoCoupon> promoCouponList) {
                progressDialog.hide();
                if(promoCouponList.size() > 0){
                    Intent intent = new Intent(getActivity(), CouponsByCompanyActivity.class);
                    intent.putExtra("id_company", id_company);
                    intent.putExtra("company_name", company_name);
                    startActivity(intent);
                }else{
                    createCompanyNotSponsoredAlertDialog(R.string.companyNotHaveCoupons);
                }
            }

            @Override
            public void error(String error) {
                progressDialog.hide();
                createCompanyNotSponsoredAlertDialog(R.string.companyNotHaveCoupons);
            }
        });
    }

    private void companyHasPromotions(){
        progressDialog.show();
        Common.getInstance().getPromotions(id_company, new Callback<List<PromoCoupon>>() {
            @Override
            public void success(List<PromoCoupon> promoCouponList) {
                progressDialog.hide();
                if(promoCouponList.size() > 0){
                    Intent intent = new Intent(getActivity(), PromotionsByCompanyActivity.class);
                    intent.putExtra("id_company", id_company);
                    intent.putExtra("company_name", company_name);
                    startActivity(intent);
                }else{
                    createCompanyNotSponsoredAlertDialog(R.string.companyNotHavePromotions);
                }
            }

            @Override
            public void error(String error) {
                progressDialog.hide();
                createCompanyNotSponsoredAlertDialog(R.string.companyNotHavePromotions);
            }
        });
    }

    private void createAlertDialog(final String tag, int message, final String args) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(message);
        builder.setPositiveButton(R.string.general_go, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (tag.equals(Constants.COMPANY_WEB)) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(args));
                    getActivity().startActivity(browserIntent);
                } else if (tag.equals(Constants.COMPANY_EMAIL)) {
                    Intent emailIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + args));
                    getActivity().startActivity(emailIntent);
                }
            }
        });
        builder.setNegativeButton(R.string.general_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog select_general_dialog;
        select_general_dialog = builder.create();
        select_general_dialog.show();
    }

    private void createCompanyNotSponsoredAlertDialog(int string) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(getResources().getString(string));
        builder.setPositiveButton(R.string.alertToCompany, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.GA_STATS_CATEGORY)
                        .setAction(Constants.GA_STATS_ACTION_REPORT)
                        .setLabel(id_company)
                        .build());

                Toast.makeText(getActivity(), getResources().
                        getString(R.string.reportWasSendToCompany), Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton(R.string.alertToCompanyLater, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog select_general_dialog;
        select_general_dialog = builder.create();
        select_general_dialog.show();
    }
}
