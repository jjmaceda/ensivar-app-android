package net.clevermobileapps.ensivar.fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.models.Favorite;
import net.clevermobileapps.ensivar.models.Profile;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import info.hoang8f.android.segmented.SegmentedGroup;
import jp.wasabeef.picasso.transformations.BlurTransformation;

public class ProfileFragment extends Fragment {

    private ProgressDialog progressDialog;
    private SegmentedGroup changeFragmentSegmentGroup;

    public static ProfileFragment newInstance(Bundle args) {
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ProfileFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) return null;

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        changeFragmentSegmentGroup = (SegmentedGroup) view.findViewById(R.id.changeFragmentSegmentGroup);
        RadioButton favoriteOption = (RadioButton) view.findViewById(R.id.favoriteOption);
        RadioButton passbookOption = (RadioButton) view.findViewById(R.id.passbookOption);

        if(getArguments().getInt("viewPagerPosition", -1) == 1){
            passbookOption.setChecked(true);
            insertPassbookFragment();
        }else{
            favoriteOption.setChecked(true);
            insertFavoriteFragment();
        }

        if(favoriteOption!= null){
            favoriteOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    insertFavoriteFragment();
                }
            });
        }
        if(passbookOption!= null){
            passbookOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    insertPassbookFragment();
                    if(!Common.isUserLoggedIn()){
                        Toast.makeText(getActivity(),
                                getResources().getString(R.string.loggedInFacebookPassbook),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        if (Common.isUserLoggedIn()) {
            showAvatar(view);
        } else {
            setFacebookLogin(view);
        }


        return view;
    }

    private void insertFavoriteFragment(){
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        Fragment fragment = FavoriteFragment.newInstance();

        ft.replace(R.id.main_fragment_container, fragment);
        ft.commit();
    }
    private void insertPassbookFragment(){
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        Fragment fragment = PassbookFragment.newInstance();

        ft.replace(R.id.main_fragment_container, fragment);
        ft.commit();
    }

    private void createProfileIfNotExist() {
        Common.getInstance().getUserProfile(new Callback<Profile>() {
            @Override
            public void success(Profile profile) {
                //PROFILE EXIST FOR ENDED DON'T CREATE A NEW PROFILE
            }

            @Override
            public void error(String error) {
                //PROFILE NOT EXIST FOR ENDED CREATE A NEW PROFILE
                ParseObject userObject = new Profile();
                userObject.put("user", ParseUser.getCurrentUser());
                userObject.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {

                    }
                });
            }
        });

    }

    private void showAvatar(final View view) {
        final RelativeLayout avatar_wrapper = (RelativeLayout) view.findViewById(R.id.avatar_wrapper);
        avatar_wrapper.setVisibility(View.VISIBLE);

        createProfileIfNotExist();

        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                //TODO check why sometimes get NULL the object
                try {
                    String fbId = object.getString("id");
                    String name = object.getString("name");

                    String facebookProfilePicUrl = "https://graph.facebook.com/" + fbId + "/picture?type=large";

                    final CircleImageView avatar = (CircleImageView) view.findViewById(R.id.user_avatar);
                    final ImageView blur_img = (ImageView) view.findViewById(R.id.blur_img);

                    Picasso.with(getActivity())
                            .load(facebookProfilePicUrl)
                            .placeholder(R.drawable.avatar_placeholder)
                            .into(avatar);
                    Picasso.with(getActivity())
                            .load(facebookProfilePicUrl)
                            .transform(new BlurTransformation(getActivity()))
                            .into(blur_img);

                    TextView user_name = (TextView) view.findViewById(R.id.user_name);
                    user_name.setText(name);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    avatar_wrapper.setVisibility(View.GONE);
                } catch (JSONException e) {
                    avatar_wrapper.setVisibility(View.GONE);
                }

            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private ArrayList<Favorite> createFavoritesList() {
        SharedPreferences preferences = getActivity().
                getSharedPreferences(Constants.FAVORITES_PREFERENCE,
                        AppCompatActivity.MODE_PRIVATE);

        Map<String, ?> map = preferences.getAll();
        ArrayList<Favorite> list = new ArrayList<>();

        for (Map.Entry<String, ?> entry : map.entrySet()) {
            Favorite favorite = new Favorite();
            favorite.setIdLocation(entry.getKey());
            favorite.setName((String) entry.getValue());

            if (Common.isUserLoggedIn()) {
                favorite.setUser(ParseUser.getCurrentUser());
            }
            list.add(favorite);
        }

        return list;
    }

    private void setFacebookLogin(final View view) {
        CardView login_wrapper = (CardView) view.findViewById(R.id.login_card_view);
        login_wrapper.setVisibility(View.VISIBLE);

        Button fb_start_btn = (Button) view.findViewById(R.id.fb_start_btn);
        fb_start_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ArrayList<String> permissions = new ArrayList<>();
                permissions.add("email");

                progressDialog = Common.getInstance().getProgressDialog(getActivity());
                progressDialog.show();

                ParseFacebookUtils.logInWithReadPermissionsInBackground(getActivity(), permissions, new LogInCallback() {
                    @Override
                    public void done(ParseUser user, ParseException err) {
                        progressDialog.dismiss();

                        if (user == null) {
                            //Log.d(TAG, "Uh oh. The user cancelled the Facebook login.");
                        } else if (user.isNew()) {
                            //Log.d(TAG, "User signed up and logged in through Facebook!");
                            transferFavorites(view, user);
                        } else {
                            //Log.d(TAG, "User logged in through Facebook!");
                            loadUserInfoAndFavorites(view, user);
                        }
                    }
                });
            }
        });
    }

    private void saveFavoriteList(final View view){
        ArrayList<Favorite> list = createFavoritesList();
        ParseObject.saveAllInBackground(list, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    if(changeFragmentSegmentGroup != null){
                       int checkId =  changeFragmentSegmentGroup.getCheckedRadioButtonId();
                        switch (checkId){
                            case R.id.favoriteOption:
                                insertFavoriteFragment();
                                break;
                            case R.id.passbookOption:
                                insertPassbookFragment();
                                break;
                        }

                    }
                    showAvatar(view);
                }
            }
        });
    }
    private void loadUserInfoAndFavorites(final View view, final ParseUser user) {
        CardView login_wrapper = (CardView) view.findViewById(R.id.login_card_view);
        if (user != null && ParseFacebookUtils.isLinked(user)) {
            login_wrapper.setVisibility(View.GONE);

            saveFavoriteList(view);
        }
    }

    private void transferFavorites(final View view, final ParseUser user) {
        CardView login_wrapper = (CardView) view.findViewById(R.id.login_card_view);
        if (Common.isUserLoggedIn()) {
            login_wrapper.setVisibility(View.GONE);
        }

        //set email from fb
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    String email = object.getString("email");
                    if (!email.isEmpty()) {
                        if(user != null) {
                            user.setEmail(email);
                            user.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    saveFavoriteList(view);
                                }
                            });
                        }
                    }
                } catch (JSONException e) {}

            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "email");
        request.setParameters(parameters);
        request.executeAsync();
    }

}
