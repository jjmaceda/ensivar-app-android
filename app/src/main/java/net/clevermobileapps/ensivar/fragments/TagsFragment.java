package net.clevermobileapps.ensivar.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;

import net.clevermobileapps.ensivar.AppSearchActivity;
import net.clevermobileapps.ensivar.EnsivarApplication;
import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.models.Tag;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;

import org.apmem.tools.layouts.FlowLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class TagsFragment extends Fragment {
    private static final String TAG = "TagsFragment";
    private ProgressBar pb;
    private static boolean sponsorsLoaded = false;
    private static boolean tagsLoaded = false;
    ParseQuery<Company> companyParseQuery;
    ParseQuery<Tag> tagsParseQuery;

    private Tracker mTracker;

    public static TagsFragment newInstance(Bundle args) {
        TagsFragment fragment = new TagsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public TagsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        if (container == null) return null;

        final View view = inflater.inflate(R.layout.fragment_tag, container, false);

        EnsivarApplication application = (EnsivarApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        pb = (ProgressBar) view.findViewById(R.id.pb);
        pb.setVisibility(View.VISIBLE);

        loadSponsors(view);


        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

        if (companyParseQuery != null)
            companyParseQuery.cancel();

        if (tagsParseQuery != null)
            tagsParseQuery.cancel();
    }

    private void createSponsors(View view, ArrayList<Company> companyArrayList) {
        int companyTotals = companyArrayList.size();

        LinearLayout sponsorCompanies = (LinearLayout) view.findViewById(R.id.sponsor_companies);

        int counter = 0;
        LinearLayout row = null;

        LinearLayout.LayoutParams rowLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
        int margin = Common.getInstance().convertPixels(getActivity(), 8);
        imageParams.setMargins(margin, margin, margin, margin);

        for (final Company company : companyArrayList) {
            if (counter % 4 == 0) {
                row = new LinearLayout(getActivity());
                row.setOrientation(LinearLayout.HORIZONTAL);
                row.setLayoutParams(rowLayoutParams);
                sponsorCompanies.addView(row);
            }

            ImageView imageView = new ImageView(getActivity());
            imageView.setLayoutParams(imageParams);
            imageView.setAdjustViewBounds(true);
            imageView.setClickable(true);
            imageView.setFocusable(true);

            if (company.getLogo()) {
                String logoUrl = Common.getInstance().getAssetUrl(getActivity(), company
                        .getObjectId() + "/logo/__120-180-240-360__/logo.png", 120);
                Picasso.with(getActivity()).load(logoUrl).placeholder(R.drawable.empty_sponsor)
                        .into(imageView);
            }

            if (row != null) {
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.GA_STATS_CATEGORY)
                                .setAction(Constants.GA_STATS_ACTION_SPONSOR_COMPANY)
                                .setLabel(company.getObjectId() + "|" + company.getName())
                                .build());

                        startActivity(Common.getInstance().createIntentForLocationByCompany
                                (getActivity(), company));
                    }
                });
                row.addView(imageView);
            }

            counter++;

            if (counter == companyTotals) {
                int how_many_last = companyTotals % 4;
                int how_many_more = how_many_last > 0 ? 4 - how_many_last : 0;
//                String url = Common.getInstance().getAssetUrl(getActivity(), "empty_360.png",
// 120);
                for (int i = 0; i < how_many_more; i++) {
                    ImageView imageView_extra = new ImageView(getActivity());
                    imageView_extra.setLayoutParams(imageParams);
                    imageView_extra.setAdjustViewBounds(true);
                    imageView_extra.setImageResource(R.drawable.empty_sponsor);
//                    Picasso.with(getActivity()).load(url).into(imageView_extra);

                    if (row != null) {
                        row.addView(imageView_extra);
                    }
                }
            }
        }
    }

    private void loadSponsors(final View view) {
        if (companyParseQuery == null) companyParseQuery = ParseQuery.getQuery("Company");

       String configCompanyId = EnsivarApplication.getConfigHelper().getCompanyId();

        companyParseQuery.cancel();
        companyParseQuery.addAscendingOrder("name");

        if (configCompanyId != null) {
            companyParseQuery.whereEqualTo("objectId", configCompanyId);
        } else {
            companyParseQuery.whereEqualTo("sponsor", true);
            companyParseQuery.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
            companyParseQuery.setMaxCacheAge(Constants.MAX_CACHE_TIME);
        }

        companyParseQuery.findInBackground(new FindCallback<Company>() {
            @Override
            public void done(List<Company> objects, ParseException e) {
                ArrayList<Company> companyArrayList;
                if (e == null) {
                    companyArrayList = (ArrayList<Company>) objects;

                    int companyTotals = companyArrayList.size();
                    if (getActivity() != null && companyTotals > 0) {
                        createSponsors(view, companyArrayList);
                    }
                }

                //check if loaded
                sponsorsLoaded = true;
                checkIfLoaded();
                loadTags(view);
            }
        });
    }

    private void loadTags(View view) {
        final FlowLayout wrapper = (FlowLayout) view.findViewById(R.id.tags_wrapper);

        if (tagsParseQuery == null) tagsParseQuery = ParseQuery.getQuery("Tag");

        tagsParseQuery.cancel();
        tagsParseQuery.addAscendingOrder("name");
        tagsParseQuery.setLimit(1000);
        tagsParseQuery.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
        tagsParseQuery.setMaxCacheAge(Constants.MAX_CACHE_TIME);

        tagsParseQuery.findInBackground(new FindCallback<Tag>() {
            @Override
            public void done(final List<Tag> objects, ParseException e) {
                ArrayList<Tag> tagArrayList;

                if (e == null) {
                    tagArrayList = (ArrayList<Tag>) objects;

                    if (getActivity() != null) {
                        //TODO can we do this different?
                        int spacing = Common.getInstance().convertPixels(getActivity(), 12);
                        int minSpacing = Common.getInstance().convertPixels(getActivity(), 1);

                        for (Tag tag : tagArrayList) {
                            Button b = new Button(getActivity());

                            FlowLayout.LayoutParams layoutParams = new FlowLayout.LayoutParams(
                                    ViewGroup.LayoutParams.WRAP_CONTENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT);
                            layoutParams.setMargins(minSpacing, spacing, spacing, minSpacing);

                            b.setBackgroundResource(R.drawable.toggle_tag_selector);
                            b.setMinHeight(0);
                            b.setMinimumHeight(0);
                            b.setMinimumWidth(0);
                            b.setMinWidth(0);

                            //TODO can we had some animations?
                            b.setTextColor(ContextCompat.getColorStateList(getActivity(), R.color
                                    .tag_btn_color_selector));
                            b.setText(tag.getName());
                            b.setTag(tag.getObjectId());
                            b.setText(tag.getName());
                            b.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final Button b = (Button) v;
                                    final String tag_id = (String) v.getTag();
                                    final ArrayList<String> tags = new ArrayList<>();
                                    tags.add(tag_id);

                                    Intent intent = new Intent(getActivity(), AppSearchActivity
                                            .class);
                                    intent.putStringArrayListExtra("tags", tags);
                                    intent.putExtra("tagName", b.getText().toString());
                                    intent.putExtra("type", AppSearchActivity.TAG_SEARCH);

                                    startActivity(intent);
                                }
                            });

                            wrapper.addView(b);
                            b.setLayoutParams(layoutParams);
                        }
                    }
                }

                //check if loaded
                tagsLoaded = true;
                checkIfLoaded();
            }
        });
    }

    private void checkIfLoaded() {
        if (sponsorsLoaded && tagsLoaded) {
            pb.setVisibility(View.GONE);
        }
    }
}
