package net.clevermobileapps.ensivar.interfaces;

/**
 * Created by jjmaceda on 6/14/15.
 */
public interface Callback<T> {
    void success(T t);
    void error(String error);
}
