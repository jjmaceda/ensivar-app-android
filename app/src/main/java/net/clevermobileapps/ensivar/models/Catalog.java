package net.clevermobileapps.ensivar.models;

public class Catalog {
    public String id_company;
    public String id_product;
    public String description;
    public String image;
    public Integer points;
    public Integer companyPoints;

    public Catalog(String id_company, String id_product, String image,String description, Integer points, Integer companyPoints) {
        this.id_company = id_company;
        this.id_product = id_product;
        this.image = image;
        this.description = description;
        this.points = points;
        this.companyPoints = companyPoints;
    }


}
