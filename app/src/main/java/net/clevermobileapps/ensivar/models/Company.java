package net.clevermobileapps.ensivar.models;

import android.support.annotation.Nullable;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import net.clevermobileapps.ensivar.interfaces.RecycleViewRowVariant;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

@ParseClassName("Company")
public class Company extends ParseObject implements RecycleViewRowVariant{

    public Company() {}

    public String getName() {
        return getString("name");
    }

    public void setName(String name) {
        put("name", name);
    }

    public String getWeb() {
        return getString("web");
    }

    public void setWeb(String web) {
        put("web", web);
    }

    public String getSocialFb() {
        return getString("social_fb");
}

    public void setSocialFb(String social) {
        put("social_fb", social);
    }

    public String getSocialTw() {
        return getString("social_twitter");
    }

    public void setSocialTw(String social) {
        put("social_twitter", social);
    }

    public String getSocialIn() {
        return getString("social_instagram");
    }

    public void setSocialIn(String social) {
        put("social_instagram", social);
    }

    public String getCover() {
        return getString("cover");
    }

    public void setCover(String cover) {
        put("cover", cover);
    }

    public String getEmail() {
        return getString("email");
    }

    public void setEmail(String email) {
        put("email", email);
    }

    public String getPhone() {
        return getString("phone");
    }

    @Nullable
    public ArrayList<String> getPredominantColor() {
        ArrayList<String> listdata = new ArrayList<>();
        JSONArray jArray = getJSONArray("predominant_color");
        if (jArray != null) {
            for (int i=0;i<jArray.length();i++){
                try {
                    listdata.add(jArray.get(i).toString());
                } catch (JSONException e) {
                    return null;
                }
            }
        }
        return listdata;
    }

    public void setPredominantColor(List<String> predominantColor){
        put("predominant_color", predominantColor);
    }

    public boolean getSponsor(){
        return getBoolean("sponsor");
    }

    public void setSponsor(boolean sponsor){
        put("sponsor", sponsor);
    }

    public boolean getFeatured(){
        return getBoolean("featured");
    }

    public void setFeatured(boolean featured){
        put("featured", featured);
    }

    public boolean getLogo(){
        return getBoolean("logo");
    }

    public void setLogo(boolean logo){
        put("logo", logo);
    }

    public String getExplanationRedeem(){
        return getString("explanation_redeem");
    }

    public String getExplanationAccumulate(){
        return getString("explanation_accumulate");
    }

    public JSONArray getLoyaltyCatalog(){
        return getJSONArray("loyalty_catalog");
    }
    public ParseObject getCategory() {
        return (ParseObject) get("category");
    }

}
