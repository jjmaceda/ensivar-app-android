package net.clevermobileapps.ensivar.models;

import android.os.Bundle;

public class CompanyInfo {
    public String name;
    public Bundle args;

    public CompanyInfo(String name, Bundle args) {
        this.name = name;
        this.args = args;
    }

}
