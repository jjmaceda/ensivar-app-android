package net.clevermobileapps.ensivar.models;

public class CompanyOverview {
    public String name;
    public String type;

    public CompanyOverview(String name, String type) {
        this.name = name;
        this.type = type;
    }
}
