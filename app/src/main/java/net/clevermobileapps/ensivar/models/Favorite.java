package net.clevermobileapps.ensivar.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

@ParseClassName("Favorite")
public class Favorite extends ParseObject {

    public Favorite() {
    }

    public String getIdLocation() {
        return getString("id_location");
    }

    public String getName() {
        return getString("name");
    }

    public void setIdLocation(String id_location) {
        put("id_location", id_location);
    }

    public void setName(String name) {
        put("name", name);
    }

    public void setUser(ParseUser user) {
        put("user", user);
    }

}
