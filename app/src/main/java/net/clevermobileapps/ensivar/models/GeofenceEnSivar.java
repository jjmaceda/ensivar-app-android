package net.clevermobileapps.ensivar.models;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

@ParseClassName("Geofence")
public class GeofenceEnSivar extends ParseObject {
    public GeofenceEnSivar() {}

    public ParseGeoPoint getGeo() {
        return (ParseGeoPoint) get("geo");
    }

    public int getRadius() {
        return (int) get("radius");
    }
}
