package net.clevermobileapps.ensivar.models;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

@ParseClassName("GeofenceZone")
public class GeofenceZone extends ParseObject {
    public GeofenceZone() {}

    public ParseGeoPoint getGeo() {
        return (ParseGeoPoint) get("geo");
    }

    public int getRadius() {
        return (int) get("radius");
    }
}
