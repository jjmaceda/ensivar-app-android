package net.clevermobileapps.ensivar.models;

import com.google.android.gms.maps.model.LatLng;
import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.Date;


@ParseClassName("Location")
public class Location extends ParseObject implements net.clevermobileapps.ensivar.interfaces.LocationType {

    public Location() {}

    public String getName() {
        return getString("name");
    }

    public void setName(String name) {
        put("name", name);
    }

    public LatLng getGeo() {
        final ParseGeoPoint geoPoint = getParseGeoPoint("geo");

        return new LatLng(geoPoint.getLatitude(), geoPoint.getLongitude());
    }

    public void setGeo(ParseGeoPoint geo) {
        put("geo", geo);
    }

    public String getPhone() {
        return getString("phone");
    }

    public void setPhone(String phone) {
        put("phone", phone);
    }

    public String getAddress() {
        return getString("address");
    }

    public void setAddress(String address) {
        put("address", address);
    }

    public Integer getVerified() {
        return getInt("verified");
    }

    public void setVerified(Integer verified) {
        put("verified", verified);
    }

    public Date getDateVerified() {
        return getDate("date_verify");
    }

    public void setDateVerified(Date date_verify) {
        put("date_verify", date_verify);
    }

    public Company getCompany() {
        return (Company) get("company");
    }

    public String getOptions() {
        return getString("options");
    }




    public static class Info implements net.clevermobileapps.ensivar.interfaces.LocationType {
        public ArrayList<String> predominantColor;
        public boolean isBank;

        public Info (ArrayList<String> predominantColor, boolean isBank) {
            this.predominantColor = predominantColor;
            this.isBank = isBank;
        }
    }
}
