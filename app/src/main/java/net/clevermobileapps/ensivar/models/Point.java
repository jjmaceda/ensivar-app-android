package net.clevermobileapps.ensivar.models;

import net.clevermobileapps.ensivar.interfaces.RecycleViewRowVariant;

import java.util.ArrayList;

public class Point implements RecycleViewRowVariant {
    public String id_company;
    public String name;
    public boolean logo;
    public int points;
    public ArrayList<String> predominantColor;
    public Company company;

    public Point(String id_company,String name, boolean logo, int points, ArrayList<String> predominantColor, Company company) {
        this.id_company = id_company;
        this.name = name;
        this.logo = logo;
        this.points = points;
        this.predominantColor = predominantColor;
        this.company = company;
    }


}
