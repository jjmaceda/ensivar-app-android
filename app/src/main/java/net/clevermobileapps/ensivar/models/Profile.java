package net.clevermobileapps.ensivar.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import org.json.JSONArray;

import java.util.List;

/**
 * Created by developer on 7/4/16.
 */
@ParseClassName("Profile")
public class Profile extends ParseObject{
    public Profile(){

    }
    public JSONArray getCoupons(){
       return getJSONArray("coupons");
    }

    public void setCoupons(JSONArray coupons){
        put("coupons", coupons);
    }

    public JSONArray getPoints(){
        return getJSONArray("points");
    }

    public void setPoints(JSONArray points){
        put("points", points);
    }

    public List<String> getBlackList(){
        return getList("black_list");
    }

    public void setBlackList(JSONArray blackList){
        put("black_list", blackList);
    }

    public ParseObject getProfileUser(){
        return (ParseObject) get("user");
    }

}
