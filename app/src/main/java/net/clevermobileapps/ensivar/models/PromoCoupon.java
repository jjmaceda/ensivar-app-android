package net.clevermobileapps.ensivar.models;

import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;

import net.clevermobileapps.ensivar.interfaces.RecycleViewRowVariant;

import java.util.Date;

@ParseClassName("PromoCoupon")
public class PromoCoupon extends ParseObject implements RecycleViewRowVariant {
    public PromoCoupon(){

    }
    public String getTitle(){
        return getString("title");
    }
    public void setTitle(String title){
        put("title", title);
    }
    public String getDescription(){
        return getString("description");
    }
    public void setDescription(String description){
        put("description", description);
    }
    public String getImage(){
        return getString("image");
    }
    public void setImage(String image){
        put("image", image);
    }
    public String getType(){
        return getString("type");
    }
    public void setType(String type){
        put("type", type);
    }
    public boolean getFeatured(){
        return getBoolean("featured");
    }
    public void setFeatured(boolean featured){
        put("featured", featured);
    }
    public Date getValidUntil(){
        return getDate("valid_until");
    }
    public void setValidUntil(Date validUntil){
        put("valid_until", validUntil);
    }
    public boolean getIsRedeem(){ return getBoolean("isRedeem");}
    public void setIsRedeem(boolean isRedeem){put("isRedeem", isRedeem);}
    public int getQuantity(){
       int quantity = 0;
        try{
            quantity =  fetchIfNeeded().getInt("quantity");
        }catch (ParseException e){
            e.printStackTrace();
        }
        return quantity;
    }
    public void setQuantity(int quantity){put("quantity", quantity);}
    public ParseObject getCompany(){
        return (ParseObject) get("company");
    }


}
