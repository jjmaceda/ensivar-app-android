package net.clevermobileapps.ensivar.models;

import android.os.Parcel;
import android.os.Parcelable;


public class Social implements Parcelable {

    public String name;
    public String url;

    public Social() {}

    public Social(String name, String url) {
        this.name = name;
        this.url = url;
    }

    @SuppressWarnings("unused")
    public Social(Parcel in) {
        this();
        readFromParcel(in);
    }

    private void readFromParcel(Parcel in) {
        name = in.readString();
        url = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(url);
    }

    public static final Creator<Social> CREATOR = new Creator<Social>() {
        public Social createFromParcel(Parcel in) {
            return new Social(in);
        }

        public Social[] newArray(int size) {
            return new Social[size];
        }
    };

    @Override
    public String toString() {
        return this.name;
    }
}
