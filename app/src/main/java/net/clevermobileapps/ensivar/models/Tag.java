package net.clevermobileapps.ensivar.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by jjmaceda on 11/30/15.
 */
@ParseClassName("Tag")
public class Tag extends ParseObject {

    public Tag() {}

    public String getName() {
        return getString("name");
    }

    public void setName(String name) {
        put("name", name);
    }

}
