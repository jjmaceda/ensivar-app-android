package net.clevermobileapps.ensivar.services;

import android.app.Activity;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;

import net.clevermobileapps.ensivar.LocationsByCompanyActivity;
import net.clevermobileapps.ensivar.GeofenceNotificationActivity;
import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.models.GeofenceEnSivar;
import net.clevermobileapps.ensivar.models.Social;
import net.clevermobileapps.ensivar.utils.Common;
import net.clevermobileapps.ensivar.utils.Constants;
import net.clevermobileapps.ensivar.utils.GeofenceErrorMessages;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class GeofenceTransitionsIntentService extends IntentService {

    protected static final String TAG = "geofence-trans-service";
    protected ArrayList<Geofence> mGeofenceList;
    protected GoogleApiClient mGoogleApiClient;

    public GeofenceTransitionsIntentService() {
        // Use the TAG to name the worker thread.
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (Common.getInstance().isInternetAvailable(this)) {

            GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

            if (geofencingEvent.hasError()) {
                String errorMessage = GeofenceErrorMessages.getErrorString(this,
                        geofencingEvent.getErrorCode());
                //TODO send error to GA
                Log.e(TAG, errorMessage);
                return;
            }

            int geofenceTransition = geofencingEvent.getGeofenceTransition();
            if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
                List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

                if (intent.getAction().equals("net.clevermobileapps.ensivar.zones")) {
                    handleZones(triggeringGeofences);
                } else {
                    handleGeofence(triggeringGeofences);
                }
            } else {
                // Log the error.
                Log.e(TAG, getString(R.string.geofence_transition_invalid_type,
                        geofenceTransition));
            }
        }
    }

    private void handleZones(List<Geofence> triggeringGeofences) {
        ParseQuery<GeofenceEnSivar> query =
                ParseQuery.getQuery("Geofence");
        ParseObject zone = ParseObject.createWithoutData(
                "GeofenceZone", triggeringGeofences.get(0).getRequestId());
        query.whereEqualTo("geofencezone", zone);

        try {
            List<GeofenceEnSivar> geofences = query.find();
            JSONArray listOfGeofence = new JSONArray();

            for (GeofenceEnSivar geo : geofences) {
                JSONObject obj = new JSONObject();

                obj.put("objectId", geo.getObjectId());
                obj.put("lat", geo.getGeo().getLatitude());
                obj.put("lng", geo.getGeo().getLongitude());
                obj.put("radius", geo.getRadius());

                listOfGeofence.put(obj);
            }

            //save geofence for this zone
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("geofence", listOfGeofence.toString());
            editor.apply();

            populateGeofenceList(listOfGeofence);
            buildGoogleApiClient();
        } catch (ParseException | JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleGeofence(List<Geofence> triggeringGeofences) {

        //check geofence in blacklist
        final SharedPreferences blacklist = this.getSharedPreferences(
                Constants.BLACKLIST_PREFERENCE, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = blacklist.edit();

        SharedPreferences pref =
                getSharedPreferences(Constants.HANDLE_GEOFENCE_LOGS_PREFERENCE, MODE_PRIVATE);
        SharedPreferences.Editor editor2 = pref.edit();

        for (Geofence geofence : triggeringGeofences) {
            String key = geofence.getRequestId();
            long storedDate = blacklist.getLong(key, 0);
            long currentDateWithoutTime = Common.getInstance().dateWithoutTime();
            String stringKey = key + ".random." + Common.random(10);

            if (storedDate == 0) {
                editor.putLong(key, currentDateWithoutTime).apply();

                getNotificationFromServer(key);
            } else {
                if (currentDateWithoutTime != storedDate) {
                    editor.putLong(key, currentDateWithoutTime).apply();

                    stringKey += ".YES";

                    getNotificationFromServer(key);
                }
            }

            editor2.putString(stringKey, new Date().toString());
            editor2.apply();

            //getNotificationFromServer(key);
        }
    }

    //METHODS FOR ZONES HANDLING
    private void populateGeofenceList(JSONArray listOfGeofence) {
        try {
            mGeofenceList = new ArrayList<>();
            for (int i = 0; i < listOfGeofence.length(); i++) {
                JSONObject obj = listOfGeofence.getJSONObject(i);
                double lat = obj.getDouble("lat");
                double lng = obj.getDouble("lng");
                int distance = obj.getInt("radius");

                Geofence geo =
                        new Geofence.Builder()
                                .setRequestId(obj.getString("objectId"))
                                .setCircularRegion(lat, lng, distance)
                                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                                        Geofence.GEOFENCE_TRANSITION_EXIT)
                                .build();

                mGeofenceList.add(geo);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private PendingIntent getGeofencePendingIntent() {
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        intent.setAction("net.clevermobileapps.ensivar.geofence");
        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    @NonNull
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER |
                Geofence.GEOFENCE_TRANSITION_EXIT);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private void addNewGeofence() {
        try {
            LocationServices.GeofencingApi.addGeofences(
                    mGoogleApiClient,
                    getGeofencingRequest(),
                    getGeofencePendingIntent()
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    Log.d(TAG, "Add onResult: " + status.toString());
                    mGoogleApiClient.disconnect();
                }
            });
        } catch (SecurityException securityException) {
            mGoogleApiClient.disconnect();
        }
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        addNewGeofence();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();

    }

    //METHODS FOR GEOFENCE HANDLING
    private void getNotificationFromServer(String key) {
        //get notification from server
        //send id geofence
        //send user object to parse

        if (!key.isEmpty()) {
            HashMap<String, String> params = new HashMap<>();
            params.put("geofence", key);

            try {
                HashMap<String, Object> response = ParseCloud.callFunction("notification", params);
                JSONObject json = new JSONObject(response);
                sendNotification(json);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendNotification(JSONObject objInfo) {
        try {
            JSONObject companyInfo = objInfo.getJSONObject("company");
            JSONObject notificationInfo = objInfo.getJSONObject("notification");
            int companyInfoLen = companyInfo.length();
            String logoUrl = notificationInfo.getString("logo");
            String attachmentUrl = notificationInfo.getString("attachment");

            Intent notificationIntent;
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());

            //for more than one company
            if (companyInfoLen == 0) {
                notificationIntent = getNotificationForMultipleIntent(notificationInfo);
                stackBuilder.addParentStack(GeofenceNotificationActivity.class);
            } else {
                //only one company
                notificationIntent = getNotificationIntent(companyInfo);
                stackBuilder.addParentStack(LocationsByCompanyActivity.class);
            }
            stackBuilder.addNextIntent(notificationIntent);

            PendingIntent notificationPendingIntent =
                    stackBuilder.getPendingIntent(1, PendingIntent.FLAG_UPDATE_CURRENT);

            final NotificationCompat.Builder builder = new NotificationCompat.Builder
                    (getApplicationContext());
            builder.setSmallIcon(R.drawable.ic_stat_onesignal_default)
                    .setColor(Color.parseColor(notificationInfo.getString("color")))
                    .setContentTitle(notificationInfo.getString("title"))
                    .setContentText(notificationInfo.getString("description"))
                    .setContentIntent(notificationPendingIntent);

            if (!logoUrl.isEmpty()) {
                try {
                    Bitmap largeIcon = Picasso.with(this).load(logoUrl).get();
                    builder.setLargeIcon(largeIcon);
                } catch (IOException e) {
                    Log.d(TAG, "cannot load icon");
                }
            }

            if (!attachmentUrl.isEmpty()) {
                try {
                    Bitmap attachment = Picasso.with(this).load(attachmentUrl)
                            .get();

                    builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture
                            (attachment));
                } catch (IOException e) {
                    Log.d(TAG, "cannot load icon");
                }
            }

            builder.setAutoCancel(true);

            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            mNotificationManager.notify(0, builder.build());
        } catch (JSONException e) {
            Log.d(TAG, "cannot send notification");
        }

    }

    private Intent getNotificationForMultipleIntent(JSONObject notificationInfo) throws
            JSONException {
        JSONArray promoCouponList = notificationInfo.getJSONArray("data");
        int len = promoCouponList.length();
        ArrayList<String> items = new ArrayList<>();

        for (int i = 0; i < len; i++) {
            String promoCouponId = promoCouponList.getString(i);
            items.add(promoCouponId);
        }

        Intent notificationIntent = new Intent(getApplicationContext(),
                GeofenceNotificationActivity.class);
        notificationIntent.putStringArrayListExtra("promoCouponKeys", items);

        return notificationIntent;
    }

    private Intent getNotificationIntent(JSONObject companyInfo) throws JSONException {

        ArrayList<String> colors = new ArrayList<>();
        colors.add(companyInfo.getString("color1"));
        colors.add(companyInfo.getString("color2"));

        String social_fb = companyInfo.getString("social_fb");
        String social_tw = companyInfo.getString("social_twitter");
        String social_ins = companyInfo.getString("social_instagram");

        ArrayList<Social> socials = new ArrayList<>();
        if (social_fb != null && !social_fb.equals("")) {
            socials.add(new Social(Constants.FACEBOOK_LABEL, social_fb));
        }

        if (social_tw != null && !social_tw.equals("")) {
            socials.add(new Social(Constants.TWITTER_LABEL, social_tw));
        }

        if (social_ins != null && !social_ins.equals("")) {
            socials.add(new Social(Constants.INSTAGRAM_LABEL, social_ins));
        }

        Intent notificationIntent = new Intent(getApplicationContext(),
                LocationsByCompanyActivity.class);
        notificationIntent.putExtra("company_name", companyInfo.getString("company_name"));
        notificationIntent.putExtra("id_company", companyInfo.getString("id_company"));
        notificationIntent.putExtra("web", companyInfo.getString("web"));
        notificationIntent.putExtra("email", companyInfo.getString("email"));
        notificationIntent.putExtra("cover", companyInfo.getString("cover"));
        notificationIntent.putParcelableArrayListExtra("social", socials);
        notificationIntent.putExtra("main_phone", companyInfo.getString("main_phone"));
        notificationIntent.putStringArrayListExtra("predominant_color", colors);
        notificationIntent.putExtra("categoryId", companyInfo.getString("categoryId"));
        notificationIntent.putExtra("sponsor", companyInfo.getBoolean("sponsor"));

        return notificationIntent;
    }

}
