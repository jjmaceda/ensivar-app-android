package net.clevermobileapps.ensivar.services;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.parse.ParseException;
import com.parse.ParseQuery;

import net.clevermobileapps.ensivar.models.GeofenceZone;
import net.clevermobileapps.ensivar.utils.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GeofenceZonesLoadedService extends IntentService {

    private static final String TAG = "GeofenceZonesService";
    protected ArrayList<Geofence> mGeofenceList;
    protected GoogleApiClient mGoogleApiClient;

    public GeofenceZonesLoadedService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (Common.getInstance().isInternetAvailable(this)) {
            ParseQuery<GeofenceZone> query =
                    ParseQuery.getQuery("GeofenceZone");
            try {
                List<GeofenceZone> geofences = query.find();
                JSONArray listOfZones = new JSONArray();
                for (GeofenceZone geo : geofences) {
                    JSONObject obj = new JSONObject();

                    obj.put("objectId", geo.getObjectId());
                    obj.put("lat", geo.getGeo().getLatitude());
                    obj.put("lng", geo.getGeo().getLongitude());
                    obj.put("radius", geo.getRadius());

                    listOfZones.put(obj);
                }

                populateGeofenceList(listOfZones);
                buildGoogleApiClient();

                //save zones
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("geoZones", listOfZones.toString());
                editor.apply();

            } catch (ParseException | JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void populateGeofenceList(JSONArray listOfZones) {
        try {
            mGeofenceList = new ArrayList<>();
            for (int i = 0 ; i < listOfZones.length(); i++) {
                JSONObject obj = listOfZones.getJSONObject(i);
//                JSONObject obj = Mocks.getInstance().getGeofence();
                double lat = obj.getDouble("lat");
                double lng = obj.getDouble("lng");
                int distance = obj.getInt("radius");

                Geofence geo =
                        new Geofence.Builder()
                        .setRequestId(obj.getString("objectId"))
                        .setCircularRegion(lat, lng, distance)
                        .setExpirationDuration(Geofence.NEVER_EXPIRE)
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                        .build();

                mGeofenceList.add(geo);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private PendingIntent getGeofencePendingIntent() {
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        intent.setAction("net.clevermobileapps.ensivar.zones");
        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @NonNull
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private void removeGeofence() {
        LocationServices.GeofencingApi.removeGeofences(
                mGoogleApiClient,
                getGeofencePendingIntent()
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                Log.d(TAG, "Remove onResult: " + status.toString());
            }
        });
    }

    private void addNewGeofence() {
        try {
            LocationServices.GeofencingApi.addGeofences(
                    mGoogleApiClient,
                    getGeofencingRequest(),
                    getGeofencePendingIntent()
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    Log.d(TAG, "Add onResult: " + status.toString());
                    mGoogleApiClient.disconnect();
                }
            });
        } catch (SecurityException securityException) {
            mGoogleApiClient.disconnect();
        }
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        addNewGeofence();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();

    }

}
