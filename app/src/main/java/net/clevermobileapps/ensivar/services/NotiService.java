package net.clevermobileapps.ensivar.services;

import android.app.IntentService;
import android.content.Intent;

import net.clevermobileapps.ensivar.utils.Mocks;

/**
 * Created by jjmaceda on 8/20/16.
 */
public class NotiService extends IntentService {

    public NotiService() {
        super("");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Mocks.getInstance().sendNotification("Test local", this);
    }
}
