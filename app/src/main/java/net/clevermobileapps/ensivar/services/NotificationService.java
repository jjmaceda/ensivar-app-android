package net.clevermobileapps.ensivar.services;


import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationPayload;

public class NotificationService extends NotificationExtenderService {

    private static final String TAG = "NotificationService";

    @Override
    protected boolean onNotificationProcessing(OSNotificationPayload notification) {
        return false;
    }
}
