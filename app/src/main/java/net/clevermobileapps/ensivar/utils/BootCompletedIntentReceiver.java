package net.clevermobileapps.ensivar.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import net.clevermobileapps.ensivar.services.GeofenceZonesLoadedService;


public class BootCompletedIntentReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            Intent pushIntent = new Intent(context, GeofenceZonesLoadedService.class);
            context.startService(pushIntent);
        }
    }

}
