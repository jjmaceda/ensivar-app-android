package net.clevermobileapps.ensivar.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseFacebookUtils;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import net.clevermobileapps.ensivar.LocationsByCompanyActivity;
import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.interfaces.Callback;
import net.clevermobileapps.ensivar.models.Company;
import net.clevermobileapps.ensivar.models.Location;
import net.clevermobileapps.ensivar.models.Profile;
import net.clevermobileapps.ensivar.models.PromoCoupon;
import net.clevermobileapps.ensivar.models.Social;
import net.clevermobileapps.ensivar.services.GeofenceZonesLoadedService;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Common {
    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();
    private static Common _instance;
    private Callback<Boolean> cb;
    private Callback<Profile> cbProfile;
    private Callback<Boolean> cbAuthenticityOfLocation;
    private Callback<List<PromoCoupon>> cbPromotionList;
    private Callback<List<PromoCoupon>> cbCouponList;
    private Callback<Company> cbCompany;

    private static final String TAG = "Common";

    private Common() {}

    public static Common getInstance() {
        if (_instance == null) {
            _instance = new Common();
        }
        return _instance;
    }

    public boolean isInternetAvailable(Context ctx) {
        ConnectivityManager cm =
                (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public Point getDimensions(AppCompatActivity activity) {
        int measuredWidth = 0;
        int measuredHeight = 0;
        int actionBarHeight = 0;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (activity.getSupportActionBar() != null) {
                actionBarHeight = activity.getSupportActionBar().getHeight();
            }
        } else {
            if (activity.getSupportActionBar() != null) {
                actionBarHeight = activity.getSupportActionBar().getHeight();
            }
        }

        WindowManager w = activity.getWindowManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            Point size = new Point();
            w.getDefaultDisplay().getSize(size);
            measuredWidth = size.x;
            measuredHeight = size.y - actionBarHeight;
        }

        return new Point(measuredWidth, measuredHeight);
    }

    public int getSpacingForDensity(Context context, int space) {
        return convertToDensity(context, space);
    }

    private int convertToDensity(Context context, int value) {
        int densityDpi = context.getResources().getDisplayMetrics().densityDpi;
        final double final_value;
        switch (densityDpi)
        {
            case DisplayMetrics.DENSITY_MEDIUM:
                final_value = value;
                break;
            case DisplayMetrics.DENSITY_TV:
            case DisplayMetrics.DENSITY_HIGH:
                final_value = value * 1.5;
                break;
            case DisplayMetrics.DENSITY_XHIGH:
            case DisplayMetrics.DENSITY_280:
                final_value = value * 2;
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
            case DisplayMetrics.DENSITY_360:
            case DisplayMetrics.DENSITY_400:
            case DisplayMetrics.DENSITY_420:
                final_value = value * 3;
                break;
            case DisplayMetrics.DENSITY_XXXHIGH:
            case DisplayMetrics.DENSITY_560:
                final_value = value * 3;
                break;
            default: final_value = value;
        }

        Log.d(TAG, "density: " + final_value);
        Log.d(TAG, "density pdi: " + densityDpi);

        return (int) final_value;
    }

    public int convertPixels(Activity context, int dp) {
        int px = 0;
        if (context != null) {
            DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            float logicalDensity = metrics.density;
            px = (int) Math.ceil(dp * logicalDensity);
        }
        return px;
    }

    public ProgressDialog getProgressDialog(Context context) {
        ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getString(R.string.loading_message));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);

        return progressDialog;
    }

    public boolean checkIfNeedFromRemote(String check, Context context) {
        Boolean isRemote = false;
        final Long tsLong = System.currentTimeMillis()/1000;
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Long lastTimeModified = preferences.getLong(check, 0);

        if (lastTimeModified == 0) {
            //first time
            //check internet
            if (!Common.getInstance().isInternetAvailable(context)) {
                //show snack bar with no connection
                CharSequence text = context.getString(R.string.no_connection);
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            } else {
                isRemote = true;
            }
        } else {
            //check 24 hours lapsed
            if ((tsLong - lastTimeModified) > 86400) {
                //check internet
                if (Common.getInstance().isInternetAvailable(context)) {
                    isRemote = true;
                }
            }
        }

        return isRemote;
    }

    public void updateLastTimeDowloaded(String check, Context context) {
        if (context != null) {
            final Long tsLong = System.currentTimeMillis()/1000;
            final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            preferences.edit().putLong(check, tsLong).apply();
        }
    }

    public void setButtonTint(Button button, ColorStateList tint) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP && button instanceof AppCompatButton) {
            ((AppCompatButton) button).setSupportBackgroundTintList(tint);
        } else {
            ViewCompat.setBackgroundTintList(button, tint);
        }
    }

    public void setImageButtonTint(ImageButton button, ColorStateList tint) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP && button instanceof AppCompatImageButton) {
            ((AppCompatImageButton) button).setSupportBackgroundTintList(tint);
        } else {
            ViewCompat.setBackgroundTintList(button, tint);
        }
    }

    public void showSweepstakeIfNeeded(Context context, Callback<Boolean> cb) {
        final String url = "http://directory.clevermobileapps.net/sweepstakeping";
        this.cb = cb;
        if (Common.getInstance().isInternetAvailable(context)) {
            new PingForSweepstakeStateTask().execute(url);
        } else {
            cb.error("no connection");
        }
    }

    private int pingUrl(String myurl) throws IOException {
        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            return conn.getResponseCode();
        }catch (IOException e) {
            return 0;
        }
    }


    public class PingForSweepstakeStateTask extends AsyncTask<String, Void, Integer> {
        @Override
        protected Integer doInBackground(String... urls) {
            // params comes from the execute() call: params[0] is the url.
            try {
                return pingUrl(urls[0]);
            } catch (IOException e) {
                return 0;
            }
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(Integer result) {
            if (result == 200) {
                if(cb != null) {
                    cb.success(true);
                }
            } else {
                if(cb != null) {
                    cb.success(false);
                }
            }
        }
    }

    public String getAssetUrl(String model) {
        return getConvertedAssetUrl(model, -1);
    }

    public String getAssetUrl(Context context, String model, int width) {
        return getConvertedAssetUrl(model, convertToDensity(context, width));
    }

    private String getConvertedAssetUrl(String model, int width) {
        Pattern PATTERN = Pattern.compile("__((?:-?\\d+)+)__");

        //base/company_id/type/size_folder/name
        //http://assets.ensivarapp.com/uploads/KysmCKxWHq/logo/android/__w-60-120-180__/logo.jpg
        //http://assets.ensivarapp.com/uploads/KysmCKxWHq/logo/android/w180/logo.jpg
        String base_url = "http://assets.ensivarapp.com/uploads/";
        model = base_url + model;

        Matcher m = PATTERN.matcher(model);
        int bestBucket = 0;
        if (m.find()) {
            String[] found = m.group(1).split("-");
            if (width < 0) {
                width = Integer.parseInt(found[0]);
            }
            for (String bucketStr : found) {
                bestBucket = Integer.parseInt(bucketStr);
                if (bestBucket >= width) {
                    // the best bucket is the first immediately
                    // bigger than the requested width
                    break;
                }
            }
            if (bestBucket > 0) {
                model = m.replaceFirst("w" + bestBucket);
            }
        }

        return model;
    }

    public Intent createIntentForLocationByCompany(Context context, Company company){
        final ArrayList<Social> socialList = new ArrayList<>();
        if (company.getSocialFb() != null && !company.getSocialFb().equals("")) {
            socialList.add(new Social(Constants.FACEBOOK_LABEL, company.getSocialFb()));
        }

        if (company.getSocialTw() != null && !company.getSocialTw().equals("")) {
            socialList.add(new Social(Constants.TWITTER_LABEL, company.getSocialTw()));
        }

        if (company.getSocialIn() != null && !company.getSocialIn().equals("")) {
            socialList.add(new Social(Constants.INSTAGRAM_LABEL, company.getSocialIn()));
        }

        Intent intent = new Intent(context, LocationsByCompanyActivity.class);
        intent.putExtra("company_name", company.getName());
        intent.putExtra("id_company", company.getObjectId());
        intent.putExtra("web", company.getWeb());
        intent.putExtra("email", company.getEmail());
        intent.putExtra("cover", company.getCover());
        intent.putParcelableArrayListExtra("social", socialList);
        intent.putExtra("main_phone", company.getPhone());
        intent.putStringArrayListExtra("predominant_color", company.getPredominantColor());
        intent.putExtra("categoryId", company.getCategory().getObjectId());
        intent.putExtra("sponsor", company.getSponsor());

        return intent;
    }


    public int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    public long dateWithoutTime() {
        // today
        Calendar date = new GregorianCalendar();
        // reset hour, minutes, seconds and millis
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        return date.getTime().getTime();
    }

    public void geofenceInit(Context context) {
        final Long tsLong = System.currentTimeMillis()/1000;
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Long lastTimeModified = preferences.getLong(Constants.GEOFENCE_LAST_CHECK, 0);
        if ((tsLong - lastTimeModified) > 86400) {
            Intent intent = new Intent(context, GeofenceZonesLoadedService.class);
            context.startService(intent);
        }
    }

    public static String random(int len) {
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }


    public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

    public static String formatDate(Date date){
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy",  Locale.getDefault());
        return df.format(date);
    }

    public static Date convertStringDateToDateObject(String dateString){
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.getDefault());
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
      return convertedDate;
    }

    public static boolean isUserLoggedIn(){
        ParseUser user = ParseUser.getCurrentUser();
        if(user!=null && ParseFacebookUtils.isLinked(user)){
            return true;
        }
        return false;
    }

    public static Drawable tintIcon(int icon, int color, Context mContext) {
        final Drawable drawable = ResourcesCompat.getDrawable(mContext.getResources(), icon, null);
        if(drawable != null){
            final Drawable wrapped = DrawableCompat.wrap(drawable);
            try {
                drawable.mutate();
                DrawableCompat.setTint(wrapped, ContextCompat.getColor(mContext, color));
                return wrapped;
            } catch (NullPointerException e) {
                throw new NullPointerException();
            }
        }else{
            return null;
        }
    }

    public void getUserProfile(final Callback<Profile> cbProfile){
        this.cbProfile = cbProfile;

        ParseQuery<Profile> profileQuery = ParseQuery.getQuery("Profile");
        profileQuery.whereEqualTo("user", ParseUser.getCurrentUser());

        profileQuery.getFirstInBackground(new GetCallback<Profile>() {
            @Override
            public void done(Profile parseProfile, com.parse.ParseException e) {
                if (e == null) {
                    cbProfile.success(parseProfile);
                }else{
                    cbProfile.error(e.toString());
                }
            }
        });
    }


    public void verifyAuthenticityOfLocation(String id_company, String id_location, final Callback<Boolean> cbAuthenticityOfLocation) {
        this.cbAuthenticityOfLocation = cbAuthenticityOfLocation;

        ParseQuery<Company> innerQuery = ParseQuery.getQuery("Company");
        innerQuery.whereEqualTo("objectId", id_company);

        ParseQuery<Location> locationQuery = ParseQuery.getQuery("Location");
        locationQuery.whereEqualTo("objectId", id_location);
        locationQuery.whereMatchesQuery("company", innerQuery);
        locationQuery.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
        locationQuery.setMaxCacheAge(Constants.MAX_CACHE_TIME);
        locationQuery.getFirstInBackground(new GetCallback<Location>() {
            @Override
            public void done(Location location, com.parse.ParseException e) {
                if (e == null) {
                    cbAuthenticityOfLocation.success(true);
                }else{
                    cbAuthenticityOfLocation.error(e.toString());
                }
            }
        });
    }

    public void getPromotions(String id_company, final Callback<List<PromoCoupon>> cbPromotionList){
        this.cbPromotionList = cbPromotionList;

        ParseQuery innerQuery = new ParseQuery("Company");
        innerQuery.whereEqualTo("objectId", id_company);

        ParseQuery<PromoCoupon> promotionsQuery = ParseQuery.getQuery("PromoCoupon");
        promotionsQuery.whereEqualTo("active", true);
        promotionsQuery.whereEqualTo("type", "P");
        promotionsQuery.whereMatchesQuery("company", innerQuery);
        promotionsQuery.include("company");
        promotionsQuery.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
        promotionsQuery.setMaxCacheAge(Constants.MAX_CACHE_TIME);
        promotionsQuery.findInBackground(new FindCallback<PromoCoupon>() {
            @Override
            public void done(List<PromoCoupon> promotions, com.parse.ParseException e) {
                if(e==null){
                    cbPromotionList.success(promotions);
                }else{
                    cbPromotionList.error(e.toString());
                }
            }
        });
    }

    public void getCoupons(String id_company, final Callback<List<PromoCoupon>> cbCouponList){
        this.cbCouponList = cbCouponList;

        ParseQuery innerQuery = new ParseQuery("Company");
        innerQuery.whereEqualTo("objectId", id_company);

        ParseQuery<PromoCoupon> couponsQuery = ParseQuery.getQuery("PromoCoupon");
        couponsQuery.whereEqualTo("type", "C");
        couponsQuery.whereEqualTo("active", true);
        couponsQuery.whereNotEqualTo("featured", true);
        couponsQuery.whereMatchesQuery("company", innerQuery);
        couponsQuery.include("company");
        couponsQuery.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
        couponsQuery.setMaxCacheAge(Constants.MAX_CACHE_TIME);
        couponsQuery.findInBackground(new FindCallback<PromoCoupon>() {
            @Override
            public void done(List<PromoCoupon> coupons, com.parse.ParseException e) {
                if(e==null){
                    cbCouponList.success(coupons);
                }else{
                    cbCouponList.error(e.toString());
                }
            }
        });
    }

    public void getCompany(String id_company, final Callback<Company> cbCompany){
        this.cbCompany = cbCompany;

        ParseQuery<Company> companyQuery = ParseQuery.getQuery("Company");
        companyQuery.whereEqualTo("objectId", id_company);
        companyQuery.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
        companyQuery.setMaxCacheAge(Constants.MAX_CACHE_TIME);
        companyQuery.getFirstInBackground(new GetCallback<Company>() {
            @Override
            public void done(Company company, com.parse.ParseException e) {
                if(e==null){
                    cbCompany.success(company);
                }else{
                    cbCompany.error(e.toString());
                }
            }
        });
    }

}
