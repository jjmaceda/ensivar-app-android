package net.clevermobileapps.ensivar.utils;

import android.support.annotation.Nullable;

import com.parse.ConfigCallback;
import com.parse.ParseConfig;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.HashMap;
import java.util.Map;

public class ConfigHelper {
    private ParseConfig config;
    private long configLastFetchedTime;

    public void fetchConfigIfNeeded() {
        fetchConfigIfNeeded(false);
    }

    public void fetchConfigIfNeeded(boolean reload) {
        final long configRefreshInterval = 60 * 60; // 1 hour

        if (config == null ||
                System.currentTimeMillis() - configLastFetchedTime > configRefreshInterval || reload) {
            // Set the config to current, just to load the cache
            config = ParseConfig.getCurrentConfig();

            // Set the current time, to flag that the operation started and prevent double fetch
            ParseConfig.getInBackground(new ConfigCallback() {
                @Override
                public void done(ParseConfig parseConfig, ParseException e) {
                    if (e == null) {
                        // Yay, retrieved successfully
                        config = parseConfig;
                        configLastFetchedTime = System.currentTimeMillis();
                    } else {
                        // Fetch failed, reset the time
                        configLastFetchedTime = 0;
                    }
                }
            });
        }
    }

    private Map<String, String> getAppEvaluationOptionsPerUser() {
        final Map<String, String> defaultOptions = new HashMap<>();

        Map<String, String> options = config.getMap("evaluations");
        if (options == null) {
            return defaultOptions;
        }

        return options;
    }

    @Nullable
    public String getUser() {
        Map<String, String> evaluations = getAppEvaluationOptionsPerUser();
        String user = evaluations.get("user");
        if (user != null) {
            ParseUser parseUser = ParseUser.getCurrentUser();
            if (parseUser != null) {
                if (user.equals(parseUser.getObjectId())) {
                    return user;
                }
            }
        }

        return null;
    }

    @Nullable
    public String getCompanyId() {
        String user = getUser();
        if (user != null) {
            Map<String, String> evaluations = getAppEvaluationOptionsPerUser();
            return evaluations.get("companyId");
        }

        return null;
    }


}