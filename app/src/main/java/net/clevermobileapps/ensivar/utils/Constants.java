package net.clevermobileapps.ensivar.utils;

import java.util.Date;


public class Constants {
    public static final String FAVORITES_PREFERENCE = "favoritesPreference";
    public static final String BLACKLIST_PREFERENCE = "blacklist_preference";
    public static final String ENSIVAR_PREFERENCE = "ensivar_preference";
    public static final String COMPANY_STATUS_DOWNLOAD_CHECK = "companyStatusDownload";
    public static final String HANDLE_GEOFENCE_LOGS_PREFERENCE = "geofence_logs";
    public static final String GEOFENCE_LAST_CHECK = "geofence_last_check";
    public static final String LAST_APP_VERSION = "lastAppVersion";

    public static final String LOCATION_FAIL = "locationFail";
    public static final String CONNECTION_FAIL = "connectionFail";

    public static final String LAUNCH_SCREEN = "Launch Screen";
    public static final String DIRECTORY_SCREEN = "Directory Screen";
    public static final String LOCATION_DETAIL_SCREEN = "Location Detail Screen";
    public static final String LOCATION_BY_COMPANY_SCREEN = "Location By Company Screen";
    public static final String WEBVIEW_SCREEN = "Webview Screen";
    public static final String LOCATION_MAP_SCREEN = "Location Map Screen";
    public static final String SEARCH_SCREEN = "Search Screen";
    public static final String TOUR_SCREEN = "Tour Screen";
    public static final String DIRECTORY_LIST_FRAGMENT = "Directory List Fragment";
    public static final String FAVORITE_FRAGMENT = "Favorite Fragment";
    public static final String TAGS_FRAGMENT = "Tags Fragment";

    public static final String FACEBOOK_LABEL = "Facebook";
    public static final String TWITTER_LABEL = "Twitter";
    public static final String INSTAGRAM_LABEL = "Instagram";

    public static final String GEO_ZONES = "geoZones";
    public static final Date today = new Date();
    public static final int RC_BARCODE_CAPTURE = 9001;
    public static final int RES_IMAGE_CAPTURE = 9002;
    public static final int MAX_CACHE_TIME = 24 * 60 * 60 * 1000;

    public static final String GA_STATS_CATEGORY = "Stats";
    public static final String GA_STATS_ACTION_REPORT = "Report";
    public static final String GA_STATS_ACTION_SPONSOR_COMPANY = "Sponsor Company";

    public static final String PROMOTIONS_SCREEN = "Promotions By Company Screen";
    public static final String COUPONS_SCREEN = "Coupons By Company Screen";
    public static final String CATALOG_SCREEN = "Catalog By Company Screen";

    public static final String COMPANY_PROMOTION = "promotion";
    public static final String COMPANY_COUPON = "coupon";
    public static final String COMPANY_POINT = "point";
    public static final String COMPANY_EMAIL = "email";
    public static final String COMPANY_WEB = "web";
    public static final String COMPANY_SOCIAL = "social";



}
