package net.clevermobileapps.ensivar.utils;

import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import net.clevermobileapps.ensivar.interfaces.Callback;

/**
 * Created by jjmaceda on 6/15/15.
 */
public class LocationManager implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ResultCallback<LocationSettingsResult> {

    private static final String TAG = "LocationManager";
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private Callback<Location> cb;
    private Context context;

    private static LocationManager _instance;

    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    public static final int REQUEST_CHECK_SETTINGS = 0x1;


    public GoogleApiClient getClient() {
        return mGoogleApiClient;
    }

    private LocationManager(Context context) {
        this.context = context;

        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );

        result.setResultCallback(this);
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();

        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                //Log.i(TAG, "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to" + "upgrade location settings ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult((AppCompatActivity) context, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    //Log.i(TAG, "PendingIntent unable to execute request.");
                    if (cb != null) {
                        cb.error(Constants.LOCATION_FAIL);
                    }
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                //Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " + "not created.");
                if(cb != null) {
                    cb.error(Constants.LOCATION_FAIL);
                }
                break;
        }
    }

    private void createLocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)        // 10 seconds, in milliseconds
                .setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS); // 1 second, in milliseconds
    }

    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    public static LocationManager getInstance(Context context) {
        if (_instance == null) {
            _instance = new LocationManager(context);
        }
        return _instance;
    }

    public void connect() {
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    public void disconnect() {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            this.context = null;
            this.cb = null;
        }
    }

    public void addLocationCallback(Callback<Location> cb) {
        this.cb = cb;
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        //Log.d(TAG, "Google client connected");
    }

    public void getLastLocation() {
        if (mGoogleApiClient.isConnected()) {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (location != null) {
                handleNewLocation(location);
            } else {
                checkLocationSettings();
            }
        } else {
            if(cb != null) {
                cb.error(Constants.CONNECTION_FAIL);
            }
        }

    }

    public void getSimpleLastLocation() {
        if (mGoogleApiClient.isConnected()) {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (location != null) {
                handleNewLocation(location);
            } else {
                if(cb != null) {
                    cb.error(Constants.LOCATION_FAIL);
                }
            }
        } else {
            if(cb != null) {
                cb.error(Constants.CONNECTION_FAIL);
            }
        }
    }

    public void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
        );
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Log.d(TAG, "onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //Log.d(TAG, "onConnectionFailed");

        if(cb != null) {
            cb.error(Constants.CONNECTION_FAIL);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        handleNewLocation(location);
    }

    private void handleNewLocation(Location location) {
        //Log.d(TAG, location.toString());

        if(cb != null) {
            cb.success(location);
        }
    }

}
