package net.clevermobileapps.ensivar.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.squareup.picasso.Picasso;

import net.clevermobileapps.ensivar.LocationsByCompanyActivity;
import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.models.Social;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class Mocks {

    private static final String TAG = "Mocks";

    private String desc1 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pellentesque nisi sed\n" +
            "Curabitur maximus mattis sapien\n" +
            "Suspendisse vel 2298-4911";
    private String desc2 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pellentesque...";
    private String desc3 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pellentesque... nisi sed Curabitur maximus mattis sapien\n" +
            "Suspendisse vel ...";
    private int date1 = 1467327984;
    private int date2 = 1469487984;
    private int date3 = 1470179184;
    private String pizzaHut = "Pizza Hut";
    private String starbucks = "Starbucks";
    private String cinepolis = "Cinépolis";
    private String grupoq = "Grupo Q";
    private String product1 = "Producto 1";
    private String product2 = "Producto 2";
    private String product3 = "Producto 3";
    private int accumulatedPoints1 = 22000;
    private int accumulatedPoints2 = 36560;
    private int accumulatedPoints3 = 8960;
    private ArrayList<String> predominantColor1 = new ArrayList<String>(){{
        add("#ff0000");
        add("#b30000");
    }};
    private ArrayList<String> predominantColor2 = new ArrayList<String>(){{
        add("#006f45");
        add("#004d2f");
    }};
    private ArrayList<String> predominantColor3 = new ArrayList<String>(){{
        add("#004f87");
        add("#003c66");
    }};
    private ArrayList<String> predominantColor4 = new ArrayList<String>(){{
        add("#005e9b");
        add("#003d66");
    }};

    private static Mocks ourInstance = new Mocks();

    public static Mocks getInstance() {
        return ourInstance;
    }

    private Mocks() {
    }


    public JSONObject getGeofence() throws JSONException {
        JSONObject geo = new JSONObject();
        geo.put("objectId", "qHHbVvEXcL");
        geo.put("lat", 13.7221102467);
        geo.put("lng", -89.21715438360502);
        geo.put("radius", 150);

        return geo;
    }

    @Nullable
    public JSONObject getNotificationInfo() {
        JSONObject notification = new JSONObject();
        JSONObject company = new JSONObject();
        JSONObject obj = new JSONObject();

        try {
            company.put("color1", "#ff0000");
            company.put("color2", "#b30000");
            company.put("company_name", "#Pizza Hut");
            company.put("id_company", "KysmCKxWHq");
            company.put("web", "");
            company.put("email", "");
            company.put("cover", "cover.png");
            company.put("social", "");
            company.put("main_phone", "2257-7777");
            company.put("categoryId", "FmWBLss8TR");
            company.put("sponsor", true);

            notification.put("title", "Pizza Hut");
            notification.put("description", "Aprovecha lo que tenemos para ti solo hoy!");
            notification.put("color", "#ff0000");
            notification.put("logo", "");
            notification.put("data", "");
            notification.put("goTo", "");
            notification.put("attachment", "");

            obj.put("company", company);
            obj.put("notification", notification);

            return obj;
        } catch (JSONException e) {
            return null;
        }
    }

    public void sendGroupedNotifications(Context context) {
        String GROUP_KEY_PROMOS = "promos_group";
        Notification notif = new NotificationCompat.Builder(context)
                .setContentTitle("Descuentos cerca de aqui")
                .setContentText("Pizza Hut, SIMAN, Go Green y mas")
                .setSmallIcon(R.drawable.ic_stat_onesignal_default)
                .build();

        // Issue the notification
        Calendar c = Calendar.getInstance();
        int mseconds = c.get(Calendar.MILLISECOND);
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(context);
        notificationManager.notify(mseconds, notif);
    }

    public void sendNotification(String notificationDetails, Context context) {
        // Create an explicit content Intent that starts the main Activity.
        ArrayList<String> colors = new ArrayList<>();
        colors.add("#ff0000");
        colors.add("#b30000");

        Social social = new Social();
        ArrayList<Social> socials = new ArrayList<>();
        socials.add(social);

        Intent notificationIntent = new Intent(context, LocationsByCompanyActivity.class);
        notificationIntent.putExtra("company_name", "Pizza Hut");
        notificationIntent.putExtra("id_company", "KysmCKxWHq");
        notificationIntent.putExtra("web", "");
        notificationIntent.putExtra("email", "");
        notificationIntent.putExtra("cover", "cover.png");
        notificationIntent.putParcelableArrayListExtra("social", socials);
        notificationIntent.putExtra("main_phone", "2257-7777");
        notificationIntent.putStringArrayListExtra("predominant_color", colors);
        notificationIntent.putExtra("categoryId", "FmWBLss8TR");
        notificationIntent.putExtra("sponsor", true);

        // Construct a task stack.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        // Add the main Activity to the task stack as the parent.
        stackBuilder.addParentStack(LocationsByCompanyActivity.class);

        stackBuilder.addNextIntent(notificationIntent);

        // Get a PendingIntent containing the entire back stack.
        PendingIntent notificationPendingIntent =
                stackBuilder.getPendingIntent(1, PendingIntent.FLAG_UPDATE_CURRENT);

        // Get a notification builder that's compatible with platform versions >= 4
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        // Define the notification settings.
        builder.setSmallIcon(R.drawable.ic_stat_onesignal_default)
                // In a real app, you may want to use a library like Volley
                // to decode the Bitmap.
                .setColor(context.getResources().getColor(R.color.app_blue))
                .setContentTitle(notificationDetails)
                .setContentText(context.getString(R.string.geofence_transition_notification_text))
                .setContentIntent(notificationPendingIntent);

            try {
                Bitmap largeIcon = Picasso.with(context).load("http://assets.ensivarapp.com/uploads/KysmCKxWHq/logo/w120/logo.png").get();
                builder.setLargeIcon(largeIcon);
            } catch (IOException e) {
                Log.d(TAG, "cannot load icon");
            }

            try {
                Bitmap attachment = Picasso.with(context).load(
                        "http://assets.ensivarapp.com/uploads/KysmCKxWHq/promos/w480/promo_pizza_hut_2.jpg")
                        .get();

                builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(attachment));
            } catch (IOException e) {
                Log.d(TAG, "cannot load icon");
            }

        // Dismiss notification once the user touches it.
        builder.setAutoCancel(true);

        // Get an instance of the Notification manager
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Issue the notification
        Calendar c = Calendar.getInstance();
        int mseconds = c.get(Calendar.MILLISECOND);
        mNotificationManager.notify(mseconds, builder.build());
    }

    public void compareTimes(Context context) {
        final SharedPreferences blacklist = context.getSharedPreferences(
                Constants.BLACKLIST_PREFERENCE, Context.MODE_PRIVATE);
        long currentDateWithoutTime = Common.getInstance().dateWithoutTime();

        Log.d(TAG, Long.toString(blacklist.getLong("qHHbVvEXcL", 0)));
        Log.d(TAG, Long.toString(currentDateWithoutTime));
    }


//    public ArrayList<Point> getPoints(Context context) {
//        ArrayList<Point> cards = new ArrayList<>();
//
//        cards.add(new Point(
//                pizzaHut,
//                Common.getInstance().getAssetUrl(context, "KysmCKxWHq/logo/__120-180-240-360__/logo.png", 120),
//                accumulatedPoints1,
//                predominantColor1)
//        );
//        cards.add(new Point(
//                starbucks,
//                Common.getInstance().getAssetUrl(context, "ih2qLjYz6O/logo/__120-180-240-360__/logo.png", 120),
//                accumulatedPoints2,
//                predominantColor2)
//        );
//
//        cards.add(new Point(
//                grupoq,
//                Common.getInstance().getAssetUrl(context, "LC0GnUfLYy/logo/__120-180-240-360__/logo.png", 120),
//                accumulatedPoints3,
//                predominantColor3)
//        );
//
//        cards.add(new Point(
//                cinepolis,
//                Common.getInstance().getAssetUrl(context, "8wCS6ooWGs/logo/__120-180-240-360__/logo.png", 120),
//                accumulatedPoints1,
//                predominantColor4)
//        );
//
//        return cards;
//    }

//    public ArrayList<Point> getPointsGifts(Context context) {
//        ArrayList<Point> cards = new ArrayList<>();
//
//        cards.add(new Point(
//                grupoq,
//                Common.getInstance().getAssetUrl(context, "LC0GnUfLYy/logo/__120-180-240-360__/logo.png", 120),
//                accumulatedPoints1,
//                predominantColor1)
//        );
//        cards.add(new Point(
//                cinepolis,
//                Common.getInstance().getAssetUrl(context, "8wCS6ooWGs/logo/__120-180-240-360__/logo.png", 120),
//                accumulatedPoints2,
//                predominantColor2)
//        );
//        return cards;
//    }

//    public ArrayList<Catalog> getCatalog(Context context){
//        ArrayList<Catalog> items = new ArrayList<>();
//
//        items.add(new Catalog(product1, Common.getInstance().getAssetUrl(context, "KysmCKxWHq/puntos/__160-240-320-480__/points_1.jpg", 160)));
//        items.add(new Catalog(product2, Common.getInstance().getAssetUrl(context, "KysmCKxWHq/puntos/__160-240-320-480__/point_2.jpg", 160)));
//        items.add(new Catalog(product3, Common.getInstance().getAssetUrl(context, "KysmCKxWHq/puntos/__160-240-320-480__/points_3.jpg", 160)));
//        items.add(new Catalog(product1, Common.getInstance().getAssetUrl(context, "KysmCKxWHq/puntos/__160-240-320-480__/points_1.jpg", 160)));
//        items.add(new Catalog(product2, Common.getInstance().getAssetUrl(context, "KysmCKxWHq/puntos/__160-240-320-480__/point_2.jpg", 160)));
//        items.add(new Catalog(product3, Common.getInstance().getAssetUrl(context, "KysmCKxWHq/puntos/__160-240-320-480__/points_3.jpg", 160)));
//        items.add(new Catalog(product1, Common.getInstance().getAssetUrl(context, "KysmCKxWHq/puntos/__160-240-320-480__/points_1.jpg", 160)));
//        items.add(new Catalog(product2, Common.getInstance().getAssetUrl(context, "KysmCKxWHq/puntos/__160-240-320-480__/point_2.jpg", 160)));
//        items.add(new Catalog(product3, Common.getInstance().getAssetUrl(context, "KysmCKxWHq/puntos/__160-240-320-480__/points_3.jpg", 160)));
//        items.add(new Catalog(product1, Common.getInstance().getAssetUrl(context, "KysmCKxWHq/puntos/__160-240-320-480__/points_1.jpg", 160)));
//
//        return items;
//    }
}
