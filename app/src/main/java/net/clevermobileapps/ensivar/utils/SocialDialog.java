package net.clevermobileapps.ensivar.utils;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import net.clevermobileapps.ensivar.R;
import net.clevermobileapps.ensivar.models.Social;

import java.util.ArrayList;

/**
 * Created by jjmaceda on 12/14/15.
 */
public class SocialDialog extends DialogFragment {
    private final String TAG = "SocialDialog";
    private ArrayList<Social> arrayList;

    public static SocialDialog newInstance(Bundle args) {
        SocialDialog fragment = new SocialDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public SocialDialog() {

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

//        builder.setMessage(R.string.social_alert_message);
        builder.setNegativeButton(R.string.social_btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        final ArrayList<Social> list = getArguments().getParcelableArrayList("social");
        final CharSequence[] items;
        try {
            final ArrayList<String> socialList = new ArrayList<>();
            for (Social s : list) {
                if (!s.url.equals("")) {
                    socialList.add(s.name);
                }
            }

            items = socialList.toArray(new CharSequence[socialList.size()]);

            builder.setTitle(R.string.social_dialog_title)
                    .setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get(which).url));
                                startActivity(browserIntent);
                            } catch (NullPointerException e) {
                                //Log.e(TAG, e.getMessage());
                            }

                        }
                    });
        } catch (NullPointerException e) {
            //Log.e(TAG, e.getMessage());
        }

        return builder.create();
    }

}
