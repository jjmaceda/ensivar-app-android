package net.clevermobileapps.ensivar.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

public class LogoImageView extends ImageView {

    public LogoImageView(Context context) {
        super(context);
    }

    public LogoImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LogoImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//
//        int height = getMeasuredHeight();
//        setMeasuredDimension(height, height);
//    }
}