Parse.Cloud.define('notification', function(request, response) {
	var companyArray = null;
	var counter = 0;
	var	promoCouponsArray = [];
	var titleForMultiple = [];

	// Juan Maceda sxGd8YhYQO
	var user = request.user; // request.user replaces Parse.User.current()
	if(!user) {
		getCompanyList([]);
	}else{
	  	var token = user.getSessionToken(); // get session token from request.user

	  	var parseProfile = new Parse.Query('Profile');
	  	parseProfile.equalTo('user', user);
		parseProfile.find({ sessionToken: token }) // pass the session token to find()
		.then(function(results) {
				console.log(results);
				if(results.length > 0){
					var black_list = results[0].get("black_list");
					getCompanyList(black_list);
				}else{
					response.error("The parse user does not exist.");
				}
			});
	}

	//FAxLiWUfKG , qHHbVvEXcL
	function getCompanyList(black_list){
		var parseCompanyQuery = new Parse.Query('Company');

		if(!request.params.geofence){
			response.error("The geofence object id is empty");
		}

		parseCompanyQuery.equalTo("geofences", request.params.geofence);
		if(black_list.length > 0){
			parseCompanyQuery.notContainedIn("objectId", black_list);
		}
		parseCompanyQuery.find()
		.then(function(companyList){
			if(companyList.length > 0){
				companyArray = companyList;
				//Only one company
				if(companyArray.length == 1){
					createSingleNotificationObject();
				}else{
				//more than one company
				createMultipleNotificationObject();
				//response.error("done.");
			}
		}else{
			response.error("There are not companies associated with the geofence received or are in the blacklist.");
		}
	});
	}


	function createSingleNotificationObject(){
		var obj = {},
		companyObject = {},
		notificationObject = {},
		company = companyArray[0];

		var innerQuery = new Parse.Query("Company");
		innerQuery.equalTo("objectId", company.id);

		var parsePromoCoupon =  new Parse.Query("PromoCoupon");
		parsePromoCoupon.equalTo('type', 'C');
		parsePromoCoupon.equalTo('active', true);
		parsePromoCoupon.equalTo('featured', true);
		parsePromoCoupon.matchesQuery("company", innerQuery);
		parsePromoCoupon.find().then(function(promoCouponList){
			if(promoCouponList.length > 0){
				var randomPromoCoupon = getRandomItem(promoCouponList);

				companyObject.id_company = company.id;
				companyObject.company_name = company.get("name");
				companyObject.main_phone = company.get("phone");
				companyObject.sponsor = company.get("sponsor");
				companyObject.web = company.get("web");
				companyObject.email = company.get("email");
				companyObject.cover = company.get("cover");
				companyObject.social_instagram = company.get("social_instagram");
				companyObject.social_twitter = company.get("social_twitter");
				companyObject.social_fb = company.get("social_fb");
				companyObject.categoryId = company.get("category").id;

				var predominant_color = company.get("predominant_color");
				if(predominant_color.length == 1){
					companyObject.color1 = predominant_color[0];
					companyObject.color2 = "";
				}else if(predominant_color.length == 2){
					companyObject.color1 = predominant_color[0];
					companyObject.color2 = predominant_color[1];
				}else{
					companyObject.color1 = "";
					companyObject.color2 = "";
				}

				if(randomPromoCoupon){
					notificationObject.objectId = randomPromoCoupon.id;
					notificationObject.title = randomPromoCoupon.get("title");
					notificationObject.description = randomPromoCoupon.get("description");
					notificationObject.color = "#ff0000";
					notificationObject.logo = "http://assets.ensivarapp.com/uploads/"+company.id+"/logo/w120/logo.png";
					notificationObject.data = [];
					notificationObject.goTo = "";

					var attachmentUrl = "",
					 	assetsUrl = "http://assets.ensivarapp.com/uploads/",
					 	image = randomPromoCoupon.get("image"),
					 	companyId = company.id;

					if(randomPromoCoupon.get("type") == "C"){
						attachmentUrl = assetsUrl+ companyId + "/cupones/w480/" + image;
					}
					if(randomPromoCoupon.get("type") == "P"){
						attachmentUrl = assetsUrl+ companyId + "/promos/w480/" + image;
					}

					notificationObject.attachment = attachmentUrl;
				}

				obj.company = companyObject;
				obj.notification = notificationObject;

				response.success(obj);
			}else{
				response.error("There are not coupons or promotions");
			}
		});
	}

	function createMultipleNotificationObject(){
		var obj = {},
		companyObject = {},
		notificationObject = {};

		for (var i = counter; i < companyArray.length; i++) {
			var company = companyArray[i];

			// if (counter < 3) { 
			// 	titleForMultiple.push(company.get("name"));
			// }

			var innerQuery = new Parse.Query("Company");
			innerQuery.equalTo("objectId", company.id);

			var parsePromoCoupon =  new Parse.Query("PromoCoupon");
			parsePromoCoupon.equalTo('type', 'C');
			parsePromoCoupon.equalTo('active', true);
			parsePromoCoupon.equalTo('featured', true);
			parsePromoCoupon.matchesQuery("company", innerQuery);
			parsePromoCoupon.find()
			.then(function(promoCouponList){
				if(promoCouponList.length > 0){
					var randomPromoCoupon = getRandomItem(promoCouponList);
					promoCouponsArray.push(randomPromoCoupon.id);
					counter++;
					createMultipleNotificationObject();
				}
			});
		}

		if(companyArray.length == counter){
			//notificationObject.description = titleForMultiple.join(", ") + "y más";

			notificationObject.title = "Descuentos cerca de aqui";
			notificationObject.description = "Pizza Hut, SIMAN, Go Green y mas";
			notificationObject.color = "#ff0000";
			notificationObject.logo = "";
			notificationObject.data = promoCouponsArray;
			notificationObject.goTo = "";
			notificationObject.attachment = "";

			obj.company = companyObject;
			obj.notification = notificationObject;

			response.success(obj);
		}

	}

	function getRandomItem(promoCouponList){
		var random = Math.floor(Math.random()*promoCouponList.length);
		return promoCouponList[random];
	}
});
