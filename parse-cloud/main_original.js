Parse.Cloud.define('notification', function(request, response) {

	// Juan Maceda sxGd8YhYQO
	var user = request.user; // request.user replaces Parse.User.current()
	if(!user) {
		getCompanyList([]);
	}else{	
	  	var token = user.getSessionToken(); // get session token from request.user

	  	var parseProfile = new Parse.Query('Profile');
	  	parseProfile.equalTo('user', user);
		parseProfile.find({ sessionToken: token }) // pass the session token to find()
			.then(function(results) {
				//console.log(results);
				if(results.length > 0){
					var black_list = results[0].get("black_list");
					getCompanyList(black_list);
				}else{
			  		response.error("The parse user does not exist.");
				}
		});
	}


	//FAxLiWUfKG , qHHbVvEXcL
	function getCompanyList(black_list){
		var parseCompanyQuery = new Parse.Query('Company');

		if(!request.params.geofence){
			response.error("The geofence object id is empty");
		}

		parseCompanyQuery.equalTo("geofences", request.params.geofence);
		if(black_list.length > 0){
			parseCompanyQuery.notContainedIn("objectId", black_list);
		}
		parseCompanyQuery.find()
		.then(function(companyList){
			// console.log("Black list: "+black_list);
			if(companyList.length > 0){
				getPromoCoupon(companyList);
			}else{
				response.error("There are companies associated with the geofence received or are in the blacklist.");
			}
		});
	}

	function getPromoCoupon(results){
		var company = results[0];
		var objectId = company.id;

		var innerQuery = new Parse.Query("Company");
			innerQuery.equalTo("objectId", objectId);

		var parsePromoCoupon =  new Parse.Query("PromoCoupon");
			parsePromoCoupon.matchesQuery("company", innerQuery);
			parsePromoCoupon.find()
			.then(function(promoCouponList){
				// console.log("Total promo coupon: "+promoCouponList.length);
				if(promoCouponList.length > 0){
					response.success(getNotificationObject(promoCouponList, company));
				}else{
					response.error("There are not promotions or coupons.");
				}
			});
	}

	function getNotificationObject(results, company){
		var promoCoupon = getRandomItem(results);
			// console.log("random promo coupon: "+promoCoupon);

		var obj = {},
			companyObject = {},
			notificationObject = {};

			companyObject.id_company = company.id;
			companyObject.company_name = company.get("name");
			companyObject.main_phone = company.get("phone");
			companyObject.sponsor = company.get("sponsor");
			companyObject.web = company.get("web");
			companyObject.email = company.get("email");
			companyObject.cover = company.get("cover");
			companyObject.social_instagram = company.get("social_instagram");
			companyObject.social_twitter = company.get("social_twitter");
			companyObject.social_fb = company.get("social_fb");
			companyObject.categoryId = company.get("category").id;

			var predominant_color = company.get("predominant_color");
			if(predominant_color.length == 1){
				companyObject.color1 = predominant_color[0];
				companyObject.color2 = "";
			}else if(predominant_color.length == 2){
				companyObject.color1 = predominant_color[0];
				companyObject.color2 = predominant_color[1];
			}else{
				companyObject.color1 = "";
				companyObject.color2 = "";
			}

			if(promoCoupon){
				notificationObject.objectId = promoCoupon.id;
				notificationObject.title = promoCoupon.get("title");
				notificationObject.description = promoCoupon.get("description");
				notificationObject.color = "#ff0000";
				notificationObject.logo = "";
				notificationObject.data = "";
				notificationObject.goTo = "";
				notificationObject.attachment = "";
			}

			obj.company = companyObject;
			obj.notification = notificationObject;

		return obj; 
	}

	function getRandomItem(results){
		var random = Math.floor(Math.random()*results.length);
		// console.log("Random index: "+random);
		return results[random]; 
	}
});
